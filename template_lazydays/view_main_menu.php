	<!-- MAIN MENU: Top horizontal menu of the site.	Use class="here" to turn the current page tab on -->
	<div id="mainMenu">
		<ul class="floatRight">
			<?php
				$page = $this->uri->segment(1);
				$here = "class='here'"; 
			?>
			<li><a <?php echo $page==""?$here:""; ?>						href="<?php echo base_url(''); ?>" title="Home" ></a></li>
			<li><a <?php echo $page=="pipeline"?$here:""; ?>		href="<?php echo base_url('pipeline'); ?>" title="All product of pipeline"></a></li>
			<li><a <?php echo $page=="basespace"?$here:""; ?>		href="<?php echo base_url('basespace'); ?>" title="Basespace">Basespace</a></li>
			<li><a <?php echo $page=="aws"?$here:""; ?>					href="aws" title="AWS"></a></li>
			<li><?php echo mailto('me@my-site.com', 'Connect Us'); ?></li>
			<!-- <li><a href="http://fullahead.org/contact.html" title="Get in touch" class="last">Mail</a></li> -->
		</ul>
	</div>