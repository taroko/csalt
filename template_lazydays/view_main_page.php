		<div class="width75 floatRight">


      <!-- Gives the gradient block -->
      <div class="gradient">

        <a name="fluidity"></a>

        <h1>Dynamic Template</h1>
        <h2>Taken from the book of <a href="http://openwebdesign.org/viewdesign.phtml?id=2514" title="View template">fluidity</a>, this template is exceptionally easy to customize using re-useable CSS classes.</h2>

        <p>
          This template quickly and easily allows for one, two, three or even four column layouts.  This is done with simple <acronym title="Cascading Style Sheet">CSS</acronym> classes that can be used over and over again.  To learn more about it, follow <a href="help.html" title="View help">this link</a>.  The template itself is valid <acronym title="eXtensible Hypertext Markup Language">XTHML</acronym> 1.0 strict and <i>almost</i> valid CSS.  The almost is added in there because CSS expressions were used.  You can read more about that <a href="#expressions" title="Read about CSS expressions">here</a>.
        </p>

        <blockquote class="go">
          <p>
            This template doesn't use fixed columns.  With a few simple CSS classes you can quickly change the appearance.  <a href="help.html" title="View help">Click here</a> to learn how.
          </p>
        </blockquote>

        <p>
          Since this template is fluid width and the font sizes are defined relatively, you can <b>change font size without breaking the layout</b>.  The fluid width also means that people with higher resolutions aren't constrained.
        </p>

        <p>
          Total size of the images used is just under 25 <acronym title="kilobytes">kb</acronym>, so that's good news to all the dial-up users.  The code behind the template is semantically written which means that it is widely supported.  As for the browsers, it displays consistently in the usual suspects:
        </p>


        <ul>
          <li><b>Firefox</b> of course</li>
          <li><b>Opera</b> sure, why not</li>
          <li><b>Netscape</b> like firefox's less attractive cousin at the dance</li>
          <li><b>Internet Explorer</b> ...</li>
          <li><b>Safari</b> cause macs are computers too</li>
        </ul>

      </div>





      <div class="gradient">

        <a name="coding"></a>

        <h1>Coding</h1>
        <h2>Image Alignment and The Header</h2>

        <p>
          <a href="http://www.mozilla.com/firefox/" title="Get Firefox"><img src="<?= $this->config->item('template'); ?>images/firefox.jpg" alt="pic" class="floatLeft"></a>
          Images are fully supported using the <i>floatLeft</i> and <i>floatRight</i> classes.  Just apply one of them to the image you want to position.  Images that have link tags around them will also have a nifty rollover effect, as long as you're using a standards complients browser.  If you're not, well then no rollover effect for you.
        </p>

        <p>
          The site header images are <a href="<?= $this->config->item('template'); ?>images/bg/header_left.jpg" title="View slice one">two</a> <a href="images/bg/header_right.jpg" title="View slice two">slices</a> and a repeated background.  Using the same approach, and a little bit of photoshop, you should be able to easily replace them.  However, since they were created by me, you're welcome to use them in any projects you might like.
        </p>

        <h2>The Footer</h2>
        <p>
          A variation on the <a href="http://www.themaninblue.com/experiment/footerStickAlt/" title="View footerStickAlt code">footerStickAlt</a> technique is used to force the site footer to the bottom of the page if there isn't enough content to push it down.  This means your template will always look tidy and you won't have to worry about a lonely footer, hanging out halfway up your page.
        </p>

        <a name="expressions"></a>

        <h2>Readability using Maximum Width</h2>
        <p>
          To improve readability of this template (since it is fluid), the width is capped at 1000 pixels.  This seems like a pretty reasonable thing to do and that's why CSS has thoughtfully included the max-width property.  Unfortunately, IE decided it didn't need no stinkin' max-width, so a CSS expression was needed.
        </p>

        <blockquote class="exclamation">
          <p>
            A CSS expression is like a snippet of javascript code that runs in your style sheet.  It's invalid CSS, but only IE pays any attention to it.
          </p>
        </blockquote>

        <p>
          If you're concerned about having valid CSS, take out the expression.  It won't ruin the template in IE, it just means that the content will expand to 100% of the screen width.  All other browsers will behave.
        </p>

      </div>

    </div>