		<!-- 25 percent width column, aligned to the left -->
		<div class="width25 floatLeft leftColumn">

			<h1>Intro</h1>

			<ul class="sideMenu">
				<li class="here">
					Dynamic Template
					<ul>
						<li><a href="#fluidity" title="Jump to section">Book of Fluidity</a></li>
						<li><a href="#coding" title="Jump to section">Coding</a></li>
					</ul>
				</li>
				<li><a href="http://fullahead.org" title="Goto Fullahead">Fullahead</a></li>
				<li><a href="http://threetree.net" title="Goto Threetree">ThreeTree</a></li>
			</ul>

			<p>
				This sidebar can be used to jump to sections within the page, or other sub pages of the active tab.
			</p>

			<p>
				Using this approach, you shouldn't need breadcrumbs since you've provided your visitor with a <b>visual cue</b> to where they are in the site.
			</p>

			<p>
				To <b>learn more</b> about how to use this template, follow the <a href="help.html" title="View help">help</a> link.	You can also see the styled <a href="tags.html" title="View tags">tags</a> and the <a href="print.html" title="View print layout">print</a> layout version.
			</p>

		</div>