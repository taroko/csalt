<!--
 ____________________________________________________________
|																														|
|		DESIGN + Pat Heard { http://fullahead.org }						 |
|			DATE + 2006.03.19																		 |
| COPYRIGHT + Free use if this notice is left in place			 |
|____________________________________________________________|

-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en-AU">

<?php echo $head; ?>


<body>

<!-- CONTENT: Holds all site content except for the footer.	This is what causes the footer to stick to the bottom -->
<div id="content">	

	<?php echo $header; ?>
	<?php echo $main_menu; ?>
	
	<!-- PAGE CONTENT BEGINS: This is where you would define the columns (number, width and alignment) -->
	<div id="page">

		<!-- 25 percent width column, aligned to the left -->
		<?php echo $sub_menu; ?>
		
		<!-- 75 percent width column, aligned to the right -->
		<!-- <div class="width75 floatRight"></div> -->
		<?php echo $main_page; ?>

	</div>

</div>

<?php echo $footer; ?>


</body>

</html>
