	<!-- HEADER: Holds title, subtitle and header images -->
	<div id="header">

		<div id="title">
			<h1>C-Salt</h1>
			<h2></h2>
		</div>
		<img src="<?= $this->config->item('template'); ?>images/bg/balloons.gif" alt="balloons" class="balloons" />
		<img src="<?= $this->config->item('template'); ?>images/bg/header_left.jpg" alt="left slice" class="left" />
		<img src="<?= $this->config->item('template'); ?>images/bg/header_right.jpg" alt="right slice" class="right" />
	</div>