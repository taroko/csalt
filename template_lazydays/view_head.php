<head>


	<title>C-satl</title>

	<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
	<meta name="author" content="fullahead.org" />
	<meta name="keywords" content="Open Web Design, OWD, Free Web Template, Lazy Days, Fullahead" />
	<meta name="description" content="A free web template designed by Fullahead.org and hosted on OpenWebDesign.org" />
	<meta name="robots" content="index, follow, noarchive" />
	<meta name="googlebot" content="noarchive" />

	<link rel="stylesheet" type="text/css" href="<?= $this->config->item('template'); ?>css/html.css" media="screen, projection, tv " />
	<link rel="stylesheet" type="text/css" href="<?= $this->config->item('template'); ?>css/layout.css" media="screen, projection, tv" />
	<link rel="stylesheet" type="text/css" href="<?= $this->config->item('template'); ?>css/print.css" media="print" />

</head>