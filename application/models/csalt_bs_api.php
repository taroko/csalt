<?php

class Csalt_bs_api extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	var $base_url = array(
		"basic" => "https://www.cloudxdna.com",
		"hoth" => "https://www.cloudxdna.com"
	);
	var $debug = false;
	var $base_api_version					= array(
		 "basic"	=> "https://api.basespace.illumina.com"
		,"hoth"		=> "https://api.cloud-hoth.illumina.com"
	);
	var $store_api_url = array(
		"basic" => ""
		,"hoth" => "https://hoth-store.basespace.illumina.com"
	);
	
	var $app_clients = array(
		 "peat_hoth" => array("a9e72275c48c47168259764fc9ef33d4", "a0df3436ea0947ecb3bb74144ae0e7f1", "https://hoth.muggle.tw/basespace/peat_hoth")
		,"peat" => array("f812fcadd55c4aef9e443545d3c08a00", "a863abe0403048efbf695cde246940d0", "https://peat.jhhlab.tw/basespace/peat")
		,"peat_free" => array("7ff6b5ab4a5245c58b011360cf19bc0e", "d1b979dcd6fe41e48545ba079196ad7f", "https://peat.jhhlab.tw/basespace/peat_free")
		,"srnap_hoth" => array("0120e6218aa942b8866f041de3a18c47", "bc34df614de24f7dbb0a90f32456bfc1", "http://localhost/~andy/csalt/index.php/basespace/srnap_hoth")
		,"spviewer_hoth" => array("cf80228e14e54b5bbfc51049f96685b0", "378fe9fdbde14a8a9cdaebce955b1647", "https://www.cloudxdna.com/bs/spviewer_hoth")
		,"spviewer" => array("8fdf3f39574d4b948bbd4a40e690ebae", "84f936827880485b82b221aa212363e9", "https://www.cloudxdna.com/bs/spviewer")
		

	);
	
	var $config = array();
	var $user = array();
	
	public function init($app_name = NULL, $bs_type = NULL, $BS_appsessionuri = NULL, $BS_authorization_code = NULL, $BS_access_token = NULL)
	{
		$is_parameter_one_null = ($app_name == NULL|| $bs_type == NULL || $BS_appsessionuri == NULL);
		
		// $_SESSION['config'] 沒有設定，並且$BS_appsessionuri, $BS_authorization_code 為 NULL，表示有可能為非法進入，或是過期
		if( !isset($_SESSION['config']) && $is_parameter_one_null )
		{
			die("No any appsessionuri and authorization_code");
		}
		// session 已經記錄過了，不再記錄。trigger頁面會自動清除session
		else if ( isset($_SESSION['config']) )
		{
			// 相同的授權，使用者應該是重新整理
			// 或是 但此頁面不是 trigger ，parameter 為 NULL
			if( ($_SESSION['config']['authorization_code'] == $BS_authorization_code) || $is_parameter_one_null )
			{
				$this->config = $_SESSION['config'];
				return true;
			}
		}
		
		// 新的 trigger 清除舊的記錄
		session_unset();
		
		$this->config["app_name"] = $app_name;
		$this->config["bs_type"] = $bs_type;
		
		$this->config["authorization_code"] = $BS_authorization_code;
		$this->config["appsessionuri"] = $BS_appsessionuri;
		
		$this->config["base_api_url"] = $this->base_api_version[$bs_type];
		
		$this->config["app_client_id"] = $this->app_clients[$app_name][0];
		$this->config["app_client_seceret"] = $this->app_clients[$app_name][1];
		$this->config["app_redirect_uri"] = $this->app_clients[$app_name][2];
		
		$this->config["access_token"] = $BS_access_token ? $BS_access_token : $this->get_access_token();
		$this->config["purchase_info"] = ""; // 未來使用
		
		$_SESSION["config"] = $this->config;
		return true;
	}
	public function init_bg($access_token)
	{
		$this->load->database();
		
		$query = $this->db->get_where( 'runs', array("access_token"=>$access_token ) );
		if($query->num_rows() == 0)
			die("No access token error.");
		
		$run = $query->row_array();
		
		$this->config["app_name"] = $run['app_name'];
		
		$this->config["bs_type"] = strstr($this->config["app_name"], "hoth") ? "hoth" : "basic";
		
		$this->config["authorization_code"] = "";
		$this->config["appsessionuri"] = $run['AppSession'];
		
		$this->config["base_api_url"] = $this->base_api_version[$this->config["bs_type"]];
		
		$this->config["app_client_id"] = $this->app_clients[$this->config["app_name"]][0];
		$this->config["app_client_seceret"] = $this->app_clients[$this->config["app_name"]][1];
		$this->config["app_redirect_uri"] = $this->app_clients[$this->config["app_name"]][2];
		$this->config["access_token"] = $access_token;
		$this->config["purchase_info"] = ""; // 未來使用
		return $run;
	}
	
	public function init_user()
	{
		$this->get_user_info();
		$this->save_user_info();
		
		$_SESSION['user'] = array(
			"Id" => $this->user['Id'],
			"Access_key" => $this->user['Access_key'],
			"Href" => $this->user['Href'],
			"Email" => $this->user['Email'],
			"Name" => $this->user['Name']
		); 
		return true;
	}
	function get_user_info()
	{	
		$results = $this->get_simple_url("{$this->config['base_api_url']}/v1pre3/users/current");	
		$this->user = $results['Response'];
		$this->user['Access_key'] = md5( $this->user['Id'] . $this->user['Name'] . $this->user['Email'] );
		return true;
	}
	function save_user_info()
	{
		if(count($this->user) == 0)
			$this->get_user_info();
			
		$this->load->database();
		
		$data = Array(
			"Email" 			=>	$this->user['Email'],
			"Href" 				=>	$this->user['Href'],
			"Name" 				=>	$this->user['Name'],
			"Access_key"		=>	$this->user['Access_key']
		);
		
		//########## save user info
		$sql_result = $this->db->get_where('peat_user', array("Id" => $this->user['Id']));
		
		if($sql_result->num_rows() > 0)
		{
			//update user info
			$row = $sql_result->row_array();
			$data["times"] = $row['times']+1;	
			$where = "Id = {$this->user['Id']}"; 
			$sql = $this->db->update_string('peat_user', $data, $where); //取得sql語法
		}
		else
		{
			//insert user info
			$data["Id"] = $this->user['Id'];
			$sql = $this->db->insert_string('peat_user', $data); //取得sql語法
		}
		$this->db->query($sql); //執行 sql
		
	}
	
	public function get_access_token()
	{
		$post_data = Array(
			"code"			=> $this->config['authorization_code'],
			"redirect_uri"	=> $this->config['app_redirect_uri'],
			"grant_type"	=> "authorization_code"
		);
		$options = Array(
			CURLOPT_URL => "{$this->config['base_api_url']}/v1pre3/oauthv2/token",
			CURLOPT_VERBOSE => true,
			CURLOPT_POST => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_USERPWD => "{$this->config['app_client_id']}:{$this->config['app_client_seceret']}",
			CURLOPT_POSTFIELDS => $post_data
		);
		
		// require access_tocken
		$response = $this->query_curl($options);
		

		$results = json_decode($response,true);
		$this->error_handle($results);
		return $results['access_token'];
	}
	
	function get_appsessions()
	{
		$results = $this->get_simple_url("{$this->config['base_api_url']}/{$this->config['appsessionuri']}");
		return $results['Response'];
	}
	function set_appsession_status($run, $status, $summary)
	{	
		$post_data = Array(
			"status"						=>	$status,
			"statussummary"					=>	$summary
		);
		$options = Array(
			CURLOPT_URL => "{$this->config['base_api_url']}/{$run['AppSession']}",
			CURLOPT_HTTPHEADER => Array("x-access-token: {$run['access_token']}"),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_VERBOSE => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $post_data
		);
		// require access_tocken
		$response = $this->query_curl($options);
		$results = json_decode($response, true);
		$this->error_handle($results);
		return $this;
	}
	
	function request_purchase($product_id)
	{
		$app_session_id = explode("/", $this->config['appsessionuri']);
		$app_session_id = $app_session_id[2];
		
		$post_data = Array(
			"Products" => Array(
				Array("Id" => $product_id, "Quantity" => 1, "Tags" => $this->config["app_name"])
			),
			"AppSessionId" => $app_session_id
		);
		
		$url = $this->store_api_url[$this->config['bs_type']] . "/v1pre3/purchases";
		$options = Array(
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => Array("x-access-token: {$this->config['access_token']}", "Content-Type: application/json"),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_VERBOSE => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => json_encode($post_data)
		);
		
		// require access_tocken
		$response = $this->query_curl($options);
		$results = json_decode($response,true);
		$this->error_handle($results);
		$results = $results['Response'];
		
		// insert to purchase
		$this->purchase_response = $data = Array(
			"Id"							=> $results["Id"]
			,"Status"						=> $results["Status"]
			,"RefundStatus"					=> $results["RefundStatus"]
			,"InvoiceNumber"				=> $results["InvoiceNumber"]
			,"Amount"						=> $results["Amount"]
			,"AmountOfTax"					=> $results["AmountOfTax"]
			,"AmountTotal"					=> $results["AmountTotal"]
			,"RefundSecret"					=> $results["RefundSecret"]
			,"HrefPurchaseDialog"			=> $results["HrefPurchaseDialog"]
		);
		$sql = $this->db->insert_string('purchase', $data);
		$this->db->query($sql);
		$purchase_id = $this->db->insert_id();
		
		// insert to purchase_product
		foreach($results["Products"] as $product)
		{
			$data = Array(
				"purchase_id"						=> $purchase_id
				,"task"								=> $this->config["app_name"]
				,"Id"								=> $product["Id"]
				,"Name"								=> $product["Name"]
				,"Quantity"							=> $product["Quantity"]
				,"PersistenceStatus"				=> $product["PersistenceStatus"]
				,"Price"							=> $product["Price"]
				,"Tags"								=> json_encode($product["Tags"])
			);
			$sql = $this->db->insert_string('purchase_product', $data);
			$this->db->query($sql);
		}
		
		return array("db_purchase_id" => $purchase_id, "bs_purchase_id" => $results["Id"]);
	}
	
	function request_purchase_update($id)
	{
		$url = $this->store_api_url[$this->config['bs_type']] . "/v1pre3/purchases";
		$results = $this->get_simple_url("$url/$id");
		$results = $results["Response"];
		$data = Array(
			"Status"						=> $results["Status"]
			,"RefundStatus"					=> $results["RefundStatus"]
		);
		$where = "`id` = '$id'";
		$sql = $this->db->update_string('purchase', $data, $where);
		$this->db->query($sql);
		
		return $data;
	}
	function refund($purchase_id, $RefundSecret)
	{
		$post_data = Array(
			"RefundSecret"		=> $RefundSecret,
			"Comment"			=> "App failed"
		);
		$url = $this->store_api_url[$this->config['bs_type']] . "/v1pre3/purchases/$purchase_id/refund";
		$options = Array(
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => Array("x-access-token: {$this->config['access_token']}"),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_VERBOSE => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $post_data
		);
		// require access_tocken
		$response = $this->query_curl($options);
		$results = json_decode($response,true);
		//var_dump($results);
		//error
		if(isset($results['ResponseStatus']['ErrorCode']))
		{
			return $results['ResponseStatus'];
		}
		else
		{
			$results = $results['Response'];
			if($results['Status'] == "COMPLETED")
				return true;
		}
		return false;
	}
	
	function purchase_check($purchaseid)
	{
		$check = $this->BS->request_purchase_update($purchaseid);
			
		if($check['Status'] == "COMPLETED")
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function save_run($instance_id)
	{
		$this->load->database();
		// 儲存 Runs 到 DB
		$data = Array(
			"InstanceId"					=>	$instance_id,
			"app_name"						=>	$this->config['app_name'],
			"access_token"					=>	$this->config['access_token'],
			"purchase_id"					=>	$this->config['purchase_info']['db_purchase_id'],
			"AppSession" 					=>	$this->config['appsessionuri'],
			"status"						=>	10,
			"HrefUser"						=>	$_SESSION['user']['Href'],
			"DateCreate"					=>	time(),
			"DateFinish"					=>	0
		);
		$sql = $this->db->insert_string('runs', $data);
		$this->db->query($sql);
		$run_id = $this->db->insert_id();
		
		//********** Saving Logs *********//
		//$this->csalt_log->add(2, "BS", "run_peat_save_run", "run_peat_save_run success");
		//********************************//
		
		return $run_id;
	}
	
	function is_run_exist($href_user, $appsessionuri)
	{
		$this->load->database();
		
		$query = $this->db->get_where('runs', array("HrefUser" => $href_user, "AppSession" => $appsessionuri ) );
		
		if($query->num_rows() > 0)
			return true;
		return false;
	}
	
	function status_table($app_name, $status)
	{	
		$table = array();
											// bs-state,  bs-msg,   instance-state  
		$table["spviewer"][0]		=	array('Aborted', 'Unknown Error!', 'terminated');
		$table["spviewer"][1]		=	array('Complete', 'Launched', 'running');
		$table["spviewer"][2]		=	array('Complete', 'Launched and User Refund', 'terminated');
		$table["spviewer"][3]		=	array('Aborted', 'User Refund', 'terminated');
		$table["spviewer"][4]		=	array('Complete', 'Time Up', 'terminated');
		$table["spviewer"][5]		=	array('Running', '', 'running');
		$table["spviewer"][6]		=	array('Pending', 'Configuring Visualizer', 'running');
		$table["spviewer"][7]		=	array('Pending', 'Downloading BaseSpace App result', 'running');
		$table["spviewer"][8]		=	array('Pending', 'Downloading Visualizer', 'running');
		$table["spviewer"][9]		=	array('Pending', 'Pending images', 'running');
		$table["spviewer"][10]		=	array('Pending', 'Preparing instance', 'pending');
		
		$table["spviewer_hoth"] = $table["spviewer"];
		
		if($status > 10)
			return array('Error!','Error status nember.');
		return $table[$app_name][$status];
	}
	function browse_note(&$run, $app_name)
	{
		$model_name = $this->get_model_name_from_app_name($app_name);
		$this->load->model($model_name, 'APP');
		return $this->APP->browse_note($run);
	}
	
	function get_simple_url($url)
	{
		$options = Array(
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => Array("x-access-token: {$this->config['access_token']}"),
			CURLOPT_VERBOSE => true,
			CURLOPT_RETURNTRANSFER => true
		);
		$response = $this->query_curl($options);
		$results = json_decode($response, true);
		
		if(!$results)
		{
			sleep(3);
			$response = $this->query_curl($options);
			$results = json_decode($response, true);
		}
		if(!$results)
			$this->utility->log("die", __CLASS__.".".__FUNCTION__, "CURL", "Curl Error! <br />", "Retry 1. Url Not Response <br /> URL: $url");
		
		//#############DeBug#########################
		if( $this->debug )
		{
			//var_dump($options);
			var_dump($results);
		}
		//###########################################
		
		$this->error_handle($results);
		
		return $results;
	}
	function get_model_name_from_app_name($app_name)
	{
		switch($app_name)
		{
			case "spviewer":
			case "spviewer_hoth":
				$model_name = "Csalt_bs_spviewer";
				break;
			default:
				$this->utility->log("die", __CLASS__.".".__FUNCTION__, $run['app_name'], "No such app", "Access token: $access_token");
				break;
		}
		return $model_name;
	}
	private function error_handle($results)
	{
		if( isset($results['error_code']) || isset($results['error_description']) )
		{	
			echo "ERROR! \n";
			var_dump($results);
			exit;	
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private function query_curl($opts)
	{
		$try_times = 0;
		
		$ch = curl_init();
		curl_setopt_array($ch, $opts);
		
		while( ($response = curl_exec($ch) ) === FALSE && ++$try_times < 2)
			sleep(3);
		
		if($response === FALSE)
			$this->utility->log("die", __CLASS__.".".__FUNCTION__, "CURL", "Curl Error! <br />", "Retry 1. ".curl_error($ch)." <br /> URL: ".$opts[CURLOPT_URL]);
		
		curl_close($ch);
		return $response;
	}
	
}


?>
