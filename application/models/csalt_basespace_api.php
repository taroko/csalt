<?php

class Csalt_baseSpace_api extends CI_Model
{
	function __construct()
  {
      parent::__construct();
  }

	var $debug = false;
	var $base_api_version					= array(
		 "basic"	=> "https://api.basespace.illumina.com"
		,"hoth"		=> "https://api.cloud-hoth.illumina.com"
	);
	var $base_api_url							=	"https://api.basespace.illumina.com";
	
	var $app_clients							= array(
		 "peat_hoth"				=> array("a9e72275c48c47168259764fc9ef33d4", "a0df3436ea0947ecb3bb74144ae0e7f1", "https://hoth.muggle.tw/basespace/peat_hoth")
		,"peat"						=> array("f812fcadd55c4aef9e443545d3c08a00", "a863abe0403048efbf695cde246940d0", "https://peat.jhhlab.tw/basespace/peat")
		,"peat_free"				=> array("7ff6b5ab4a5245c58b011360cf19bc0e", "d1b979dcd6fe41e48545ba079196ad7f", "https://peat.jhhlab.tw/basespace/peat_free")
		,"srnap_hoth"				=> array("0120e6218aa942b8866f041de3a18c47", "bc34df614de24f7dbb0a90f32456bfc1", "http://localhost/~andy/csalt/index.php/basespace/srnap_hoth")
		//,"srnap_hoth"				=> array("0120e6218aa942b8866f041de3a18c47", "bc34df614de24f7dbb0a90f32456bfc1", "https://hoth.muggle.tw/basespace/srnap_hoth")
	);
	var $app_client_id					= "";
	var $app_client_seceret				= "";
	var $app_redirect_uri				=	"https://peat.jhhlab.tw/basespace/peat";
	var $app_authorization_code 		= NULL;
	var $app_appsessionuri				= NULL;
	var $app_appsessionid				= NULL;
	var $app_access_tocken				= NULL;
	var $app_appsessionuri_data			= NULL;
	
	var $user_data						= NULL;
	var $user_projects					= NULL;
	var $user_runs						= NULL;
	
	var $project_data					= NULL;
	var $project_samples				= NULL;
	var $project_create					= NULL;
	
	var $appresult_create				= NULL;

	var $run_data						= NULL;
	var $run_samples					= NULL;

	var $sample_data					= NULL;
	var $sample_files					= NULL;
	
	var $files							= NULL;
	
	var $purchase_response				= NULL;
	
	var $retry_number					= 0;
	
	//set parameter
	public function init($app_name, $authorization_code, $appsessionuri, $bs_type = "basic")
	{
		if($appsessionuri != NULL)
		{
			$ids = explode("/", $appsessionuri);
			$this->app_appsessionid					=	$ids[2];
		}
		$this->base_api_url							= $this->base_api_version[$bs_type];
		$this->app_authorization_code				= $authorization_code;
		$this->app_appsessionuri					= $appsessionuri;
		
		$this->app_client_id						= $this->app_clients[$app_name][0];
		$this->app_client_seceret					= $this->app_clients[$app_name][1];
		$this->app_redirect_uri						= $this->app_clients[$app_name][2];
	}
	
	//! Access_token
	
	//#################### GET access_token #####################
	public function get_access_token()
	{
		if($this->app_access_tocken != NULL)
		{
			return $this->app_access_tocken;
		}
		
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "get_access_token", "curl get access_token");
		//********************************//
		
		$post_data = Array(
			"code"					=>	$this->app_authorization_code,
			"redirect_uri"	=>	$this->app_redirect_uri,
			"grant_type"		=>	"authorization_code"
		);
		$options = Array(
			CURLOPT_URL => "{$this->base_api_url}/v1pre3/oauthv2/token",
			CURLOPT_VERBOSE => true,
			CURLOPT_POST => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_USERPWD => "$this->app_client_id:{$this->app_client_seceret}",
			CURLOPT_POSTFIELDS => $post_data
		);
		
		// require access_tocken
		$response = $this->query_curl($options);
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "get_access_token", "curl get access_token success");
		//********************************//
		
		$results = json_decode($response,true);
		
		//############# DeBug #########################
		if( $this->debug )
		{
			//var_dump($post_data);
			//var_dump($options);
			//var_dump($results);
		}
		//#############################################
		
		$this->error_handle($results);

		
		$this->app_access_tocken = $results['access_token'];
		return $this->app_access_tocken;
	}
	//#################### SET access_token #####################
	public function set_access_token($access_tocken)
	{
		$this->app_access_tocken = $access_tocken;
	}
	
	public function get_appsessions()
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "get_appsessions", "curl get info");
		//********************************//
		$results = $this->get_simple_url("{$this->base_api_url}/{$this->app_appsessionuri}?access_token={$this->app_access_tocken}");
		$this->app_appsessionuri_data = $results['Response'];
		return $this;
	}
	
	// !Initial
	public function session_check_init($app_name, $BS_appsessionuri = NULL, $BS_authorization_code = NULL, $bs_type = "basic")
	{
		//########## app code, app session, init BS api
		//如果 session 已經記錄
		if( isset($_SESSION['BS_authorization_code']) && isset($_SESSION['BS_appsessionuri']) )
		{
			if($BS_appsessionuri == NULL && $BS_authorization_code == NULL)
			{
				// nothing to do
			}
			else if( $BS_appsessionuri != $_SESSION['BS_appsessionuri'] || $BS_authorization_code != $_SESSION['BS_authorization_code'])
			{
				// session is different, so reset all
				session_unset();
			}
			else
			{
				// session is the same
			}
		}
		// session not record, but $BS_appsessionuri or $BS_authorization_code is NULL
		else if($BS_appsessionuri == NULL && $BS_authorization_code == NULL)
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "peat", "session_check_init", "No any appsessionuri and authorization_code");
			//********************************//
			die("No any appsessionuri and authorization_code");	
		}
		// session not record, and $BS_appsessionuri and $BS_authorization_code having value, Go init
		else
		{
			// OK! Go init
		}
		if($BS_appsessionuri != NULL && $BS_authorization_code != NULL)
		{
			$_SESSION['BS_authorization_code'] = $BS_authorization_code;
			$_SESSION['BS_appsessionuri'] = $BS_appsessionuri;
		}
		
		// set parameter
		$this->init($app_name, $_SESSION['BS_authorization_code'], $_SESSION['BS_appsessionuri'], $bs_type);
		
		//########## app access tocken
		if( ! isset($_SESSION['BS_access_token']) )
		{
			$_SESSION['BS_access_token'] = $this->get_access_token();
		}
		else
		{
			$this->set_access_token($_SESSION['BS_access_token']);
		}
		
		// set app_name
		$_SESSION['app_name'] = $app_name;
		$_SESSION['bs_type'] = $bs_type;
	}

	// !User
	//#################### GET user_info #####################
	function get_user_info()
	{
		if( is_array($this->user_data) )
		{
			return $this;
		}
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "get_user_info", "curl get info");
		//********************************//
		
		$results = $this->get_simple_url("{$this->base_api_url}/v1pre3/users/current");
		
		$this->user_data = $results['Response'];
		return $this;
	}
	
	////#################### GET user_projects #####################
	function get_projects_from_user()
	{
		//if($this->user_data == NULL)
		$this->get_user_info();
		
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "get_projects_from_user", "curl get info");
		//********************************//
		
		$results = $this->get_simple_url("{$this->base_api_url}/{$this->user_data['HrefProjects']}");
		$this->user_projects = $results['Response'];
		return $this;
	}
	
	////#################### GET user_runs #####################
	function get_runs_from_user()
	{
		//if($this->user_data == NULL)
		$this->get_user_info();
		
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "get_runs_from_user", "curl get info");
		//********************************//
		
		$results = $this->get_simple_url("{$this->base_api_url}/{$this->user_data['HrefRuns']}");
		$this->user_runs = $results['Response'];
		return $this;
	}
	
	//#################### SAVE user_info #####################
	function save_user_info($BS_Id, $BS_Access_key)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "save_user_info", "save user info");
		//********************************//
		
		$this->load->database();
		
		//########## save user info
		$sql_result = $this->db->get_where('peat_user', array("Id"=>$BS_Id));
		
		if($sql_result->num_rows() > 0)
		{
			$row = $sql_result->row_array();
			//update user info
			$data = Array(
				"Email" 			=>	$this->user_data['Email'],
				"Href" 				=>	$this->user_data['Href'],
				"Name" 				=>	$this->user_data['Name'],
				"access_key"	=>	$BS_Access_key,
				"times"				=>	$row['times']+1
			);
				
			$where = "Id = {$this->BS->user_data['Id']}"; 
			
			//取得sql語法
			$sql = $this->db->update_string('peat_user', $data, $where);
			
			//執行 sql，更新
			$this->db->query($sql);
			
			//********** Saving Logs *********//
			$this->csalt_log->add(3, "BS", "save_user_info", "update user info success");
			//********************************//
		}
		else
		{
			//insert user info
			$data = Array(
				"Id"					=>	$this->user_data['Id'],
				"Email" 			=>	$this->user_data['Email'],
				"Href" 				=>	$this->user_data['Href'],
				"Name" 				=>	$this->user_data['Name'],
				"access_key"	=>	$BS_Access_key
			);
			//取得sql語法
			$sql = $this->db->insert_string('peat_user', $data);
			
			//執行 sql，新增
			$this->db->query($sql);
			
			//********** Saving Logs *********//
			$this->csalt_log->add(3, "BS", "save_user_info", "insert user info success");
			//********************************//
		}
	}
	
	
	// !Project
	//#################### GET project_info #####################
	function get_project_info($href)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "get_project_info", "curl get info");
		//********************************//
		$results = $this->get_simple_url("{$this->base_api_url}/{$href}?Limit=1000");
		$this->project_data = $results["Response"];
		return $this;
	}
	
	//#################### GET project_samples #####################
	
	function get_samples_from_project($href=NULL)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "get_samples_from_project", "curl get info");
		//********************************//
		if($href != NULL)
		{
			$results = $this->get_simple_url("{$this->base_api_url}/{$href}?&Limit=1000");
		}
		else if($this->project_data == NULL)
		{
			die("please select project.");
		}
		else
		{
			$results = $this->get_simple_url("{$this->base_api_url}/{$this->project_data['HrefSamples']}&Limit=1000");
		}
		$this->project_samples = $results["Response"];
		return $this;
	}
	
	//#################### POST create_project #####################
	function create_project ($name)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "create_project", "create_project init, $name");
		//********************************//
		
		$post_data = Array(
			"name"					=>	$name
		);
		$options = Array(
			CURLOPT_URL => "{$this->base_api_url}/v1pre3/projects",
			CURLOPT_HTTPHEADER => Array("x-access-token: {$this->app_access_tocken}"),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_VERBOSE => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $post_data
		);
		
		// require access_tocken
		
		//********** Saving Logs *********//
		$this->csalt_log->add(2, "BS", "create_project", "get curl , {$this->base_api_url}/v1pre3/projects");
		//********************************//
		
		$response = $this->query_curl($options);
		
		$results = json_decode($response,true);
		
		$this->error_handle($results);
		
		if( !isset($results['Response']) )
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(1, "BS", "create_project", "Error! No result Response, app project create failure.");
			//********************************//
			die("Error! No result Response, app project create failure.");
		}
		
		$this->project_create = $results['Response'];
		
		return $this;
	}
	
	//#################### POST create_appresult #####################
	function create_appresult($name, $description, $ref_sample_href)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "create_appresult", "create_appresult init, $name, $description, $ref_sample_href");
		//********************************//
	
		//var_dump($this->project_create);
		if($this->project_create == NULL)
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "BS", "create_appresult", "No project created.");
			//********************************//
			die("Error! No project created.");
		}
		if($this->app_appsessionuri == NULL)
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "BS", "create_appresult", "Error! No app_appsessionuri.");
			//********************************//
			die("Error! No app_appsessionuri.");
		}
		//檢查 $ref_sample_href 是否為 sample，如果不是則會出錯，如果是 appresults 則 trace回 sample href
		if(strstr($ref_sample_href, "appresults"))
		{
			$this->get_sample_info($ref_sample_href);
			$ref_sample_href = $this->sample_data["References"][0]["HrefContent"];
		}
		$post_data = Array(
			"Name"						=>	$name,
			"Description"			=>	$description,
			"HrefAppSession"	=>	$this->app_appsessionuri,
			"References"			=>	Array( Array("Rel" => "using", "HrefContent" => $ref_sample_href) )
		);
		$options = Array(
			CURLOPT_URL => "{$this->base_api_url}/{$this->project_create['HrefAppResults']}",
			CURLOPT_HTTPHEADER => Array("x-access-token: {$this->app_access_tocken}", "Content-Type: application/json"),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_VERBOSE => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => json_encode($post_data)
		);
		
		//********** Saving Logs *********//
		$this->csalt_log->add(2, "BS", "create_appresult", "get curl, {$this->base_api_url}/{$this->project_create['HrefAppResults']}");
		//********************************//
		// require access_tocken
		$response = $this->query_curl($options);
		$results = json_decode($response,true);
		$this->error_handle($results);
		//echo "$name $description <br>";
		//var_dump($results);
		
		if( !isset($results['Response']) )
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "BS", "create_appresult", "Error! No result Response, app result create failure.");
			//********************************//
			die("Error! No result Response, app result create failure.");
			
			$this->retry_number ++;
			if($this->retry_number < 100)
			{
				sleep(2);
				$this->create_appresult($name, $description, $ref_sample_href);
				echo "failure. retrying".$this->retry_number;
			}
			else
			{
				die("retry finish");
			}
		}
		
		$this->appresult_create = $results['Response'];
		//var_dump($this->appresult_create);
		return $this;
	}
	
	
	
	// !Run
	//#################### GET run_info #####################
	function get_run_info($href)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "get_run_info", "curl get info");
		//********************************//
	
		$results = $this->get_simple_url("{$this->base_api_url}/{$href}?Limit=1000");
		$this->run_data = $results["Response"];
		return $this;
	}
	
	//#################### GET run_samples #####################
	
	function get_samples_from_run($href=NULL)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "get_samples_from_run", "curl get info");
		//********************************//
		if($href != NULL)
		{
			$results = $this->get_simple_url("{$this->base_api_url}/{$href}?Limit=1000");
		}
		else if($this->run_data == NULL)
		{
			die("please select run.");
		}
		else
		{
			$results = $this->get_simple_url("{$this->base_api_url}/{$this->run_data['HrefSamples']}?Limit=1000");
		}
		
		$this->project_samples = $results["Response"];
		return $this;
	}
	
	
	// !Samples
	
	function get_sample_info($href)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "get_sample_info", "curl get info");
		//********************************//
		$results = $this->get_simple_url("{$this->base_api_url}/{$href}?Limit=1000");
		$this->sample_data = $results["Response"];
		return $this;
	}
	
	function get_files_from_sample()
	{
		if($this->sample_data == NULL)
			die("please select sample.");
		
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "get_files_from_sample", "curl get info");
		//********************************//
		
		$results = $this->get_simple_url("{$this->base_api_url}/{$this->sample_data['HrefFiles']}?Limit=1000");
		$this->sample_files = $results["Response"];
		return $this;
	}
	
	////########## app session
	//#################### POST set_appsession_status #####################
	function set_appsession_status($href, $status, $summary)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "set_appsession_status", "set_appsession_status init");
		//********************************//
		
		$post_data = Array(
			"status"						=>	$status,
			"statussummary"					=>	$summary
		);
		$options = Array(
			CURLOPT_URL => "{$this->base_api_url}/$href",
			CURLOPT_HTTPHEADER => Array("x-access-token: {$this->app_access_tocken}"),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_VERBOSE => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $post_data
		);
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "set_appsession_status", "get curl, {$this->base_api_url}/$href");
		//********************************//
		
		// require access_tocken
		$response = $this->query_curl($options);
		$results = json_decode($response,true);
		$this->error_handle($results);
		
		//$this->appresult_create = $results['Response'];
		//var_dump($results);
		return $this;
	}
	
	// !Files
	//#################### GET file list #####################
	function get_files($href=NULL)
	{
		if($href == NULL)
			die("No Href");
			
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "get_files", "curl get info");
		//********************************//

		$results = $this->get_simple_url("{$this->base_api_url}/$href?Limit=1000");
		$this->files = $results["Response"];
		return $this;
	}
	
	function get_paired_file($items)
	{
		$files = Array();
		foreach($items as $pItem)
		{
			if( strstr($pItem['Name'],'fastq') && (strstr($pItem['Name'],'R1') || strstr($pItem['Name'],'R2')) )
			{
				$files[$pItem['Name']] = $pItem;
			}
		}
		$paired_files = Array();
		foreach($files as $filename=>$pItem)
		{
			if(!strstr($pItem['Name'],'R1'))
				continue;
			$other_filename = str_replace( "R1", "R2", $filename );
			if( isset( $files [ $other_filename ] ) )
			{
				$paired_files[] = Array(
					'R1' => $pItem,
					'R2' => $files [ $other_filename ]
				);
			}
			else
			{
				return "Error, Not paired";
			}
		}
		return $paired_files;
	}
	
	function get_single_file($items)
	{
		$files = Array();
		foreach($items as $pItem)
		{
			if( strstr($pItem['Name'],'fastq') )
			{
				$files[$pItem['Name']] = $pItem;
			}
		}
		$single_files = Array();
		foreach($files as $filename=>$pItem)
		{
			$single_files[] = Array(
				'R1' => $pItem
			);
		}
		return $single_files;
	}
	
	
	
	
	
	// !Buy
	function request_buy_update($id)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "request_buy_update", "curl get info");
		//********************************//
		$results = $this->get_simple_url("https://hoth-store.basespace.illumina.com/v1pre3/purchases/$id");

		$results = $results["Response"];
		
		$data = Array(
			"Status"								=> $results["Status"]
			,"RefundStatus"					=> $results["RefundStatus"]
		);
		
		
		$where = "`id` = '$id'";
		$sql = $this->db->update_string('purchase', $data, $where);
		$this->db->query($sql);
		
		//********** Saving Logs *********//
		$this->csalt_log->add(2, "BS", "request_buy_update", "success, $id, {$results['Status']}, {$results['Status']}");
		//********************************//
		
		return $data;
	}
	
	
	function request_buy($product_id, $app_name)
	{
		$post_data = Array(
			"Products" => Array(
				Array("Id" => $product_id, "Quantity" => 1, "Tags" => "$app_name")
			),
			"AppSessionId" => $this->app_appsessionid
		);
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "request_buy", "request_buy, $product_id, $app_name, {$this->app_appsessionid}");
		//********************************//
		
		$options = Array(
			//CURLOPT_URL => "{$this->base_api_url}/{$this->project_create['HrefAppResults']}",
			CURLOPT_URL => "https://hoth-store.basespace.illumina.com/v1pre3/purchases",
			CURLOPT_HTTPHEADER => Array("x-access-token: {$this->app_access_tocken}", "Content-Type: application/json"),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_VERBOSE => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => json_encode($post_data)
		);
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "request_buy", "get curl, https://hoth-store.basespace.illumina.com/v1pre3/purchases");
		//********************************//
		
		// require access_tocken
		$response = $this->query_curl($options);
		$results = json_decode($response,true);
		$this->error_handle($results);
		$results = $results['Response'];
		
		// insert to purchase
		$this->purchase_response = $data = Array(
			"Id"										=> $results["Id"]
			,"Status"								=> $results["Status"]
			,"RefundStatus"					=> $results["RefundStatus"]
			,"InvoiceNumber"				=> $results["InvoiceNumber"]
			,"Amount"								=> $results["Amount"]
			,"AmountOfTax"					=> $results["AmountOfTax"]
			,"AmountTotal"					=> $results["AmountTotal"]
			,"RefundSecret"					=> $results["RefundSecret"]
			,"HrefPurchaseDialog"		=> $results["HrefPurchaseDialog"]
		);
		$sql = $this->db->insert_string('purchase', $data);
		$this->db->query($sql);
		$purchase_id = $this->db->insert_id();
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "request_buy", "insert to purchase success");
		//********************************//
		
		// insert to purchase_product
		foreach($results["Products"] as $product)
		{
			$data = Array(
				"purchase_id"								=> $purchase_id
				,"task"											=> $app_name
				,"Id"												=> $product["Id"]
				,"Name"											=> $product["Name"]
				,"Quantity"									=> $product["Quantity"]
				,"PersistenceStatus"				=> $product["PersistenceStatus"]
				,"Price"										=> $product["Price"]
				,"Tags"											=> json_encode($product["Tags"])
			);
			$sql = $this->db->insert_string('purchase_product', $data);
			$this->db->query($sql);
		}
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "request_buy", "insert to purchase_product success");
		//********************************//
		
		return $purchase_id;
	}
	function refund($purchase_id, $RefundSecret)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "refund", "refund, $purchase_id, $RefundSecret");
		//********************************//
		$post_data = Array(
			"RefundSecret"		=> $RefundSecret,
			"Comment"					=> "App failed during RNA-Seq analysis"
		);
		$options = Array(
			//CURLOPT_URL => "{$this->base_api_url}/{$this->project_create['HrefAppResults']}",
			CURLOPT_URL => "https://hoth-store.basespace.illumina.com/v1pre3/purchases/$purchase_id/refund",
			CURLOPT_HTTPHEADER => Array("x-access-token: {$this->app_access_tocken}"),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_VERBOSE => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $post_data
		);
		// require access_tocken
		$response = $this->query_curl($options);
		$results = json_decode($response,true);
		$this->error_handle($results);
		// var_dump($results);
		$results = $results['Response'];
		
		if($results['Status'] == "COMPLETED")
			return true;
	}
	
	
	// !Run PEAT app
	function run_peat_check_init()
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "run_peat_check_init", "run peat check init");
		//********************************//
		
		$this->load->model('Error_handle', 'EH');
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
	
		//if( !isset($_SESSION['BS_authorization_code']) || !isset($_SESSION['BS_appsessionuri']) )
		//	$this->EH->error_handle($pages,'No session file! or No authorization code!');
		
		if( !isset($_SESSION['BS_access_token']) )
		{
			$this->EH->error_handle($pages,'No access_token!');
		}
			
		
		if(!isset($_POST['sample_select']))
		{
			$this->EH->error_handle($pages,'No sample selection!');
		}
		else
		{
			$href = $_POST['sample_select'];
		}
			
		
		if(!isset($_POST['files_select']))
		{
			$this->EH->error_handle($pages,'No files selected!');
		}
			
		//else
		//	$selected_files = json_decode($_POST['files_select']);
		$this->init($_SESSION['app_name'], NULL, $_SESSION['BS_appsessionuri'], $_SESSION['bs_type']);
		$this->set_access_token($_SESSION['BS_access_token']);
		$this->get_user_info();
		$this->get_sample_info($href);
		$this->get_files_from_sample();
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "run_peat_check_init", "run peat check success");
		//********************************//
	}
	
	function run_peat_check_files()
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "run_peat_check_files", "run_peat_check_files init");
		//********************************//
		
		$this->load->model('Error_handle', 'EH');
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		
		$href = $_POST['sample_select'];
		$selected_files = json_decode($_POST['files_select']);
		
		
		//檢查是否為 pair end
		if($this->sample_data['IsPairedEnd'] == false)
		{
			$this->HR->error_handle($pages,' This sample is not paired!');
		}
		
		//處理選擇的 Files
		foreach($this->sample_files['Items'] as $fItem)
		{
			if(in_array($fItem['Href'], $selected_files))
			{
				$tmp_array[ $fItem['Name'] ] = $fItem;
			}
		}
		
		//檢查數目
		if(count($tmp_array)%2 != 0)
		{
			$this->HR->error_handle($pages,'Fastq file not paired.');
		}
		
		//檢查是否配對，以及整理配對檔案
		$paired_files = $this->get_paired_file($tmp_array);
		if(is_string($paired_files))
		{
			$this->HR->error_handle($pages,$paired_files);
		}
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "run_peat_check_files", "run_peat_check_files success");
		//********************************//
		
		return $paired_files;
	}
	
	function run_peat_check_db()
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "run_peat_check_db", "run_peat_check_db init");
		//********************************//
		
		$this->load->database();
		$this->load->model('Error_handle', 'EH');
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		
		$query = $this->db->get_where('peat_user', array("Href"=>$this->user_data['Href']) );
		if($query->num_rows() == 0)
		{
			$this->EH->error_handle($pages,'No user info in db.');
		}
		else
		{
			$DB_user = $query->row_array();
		}
		
		$query = $this->db->get_where('peat_runs', array("HrefUser"=>$this->user_data['Href'], "AppSession" => $_SESSION['BS_appsessionuri'] ) );
		
		if($query->num_rows() > 0)
		{
			$this->EH->error_handle($pages,'You have run this app session.');
		}
		
		//$query = $this->db->get_where('peat_runs', array("HrefUser"=>$this->BS->user_data['Href'], "HrefSamples" => $this->BS->sample_data['Href'] ) );
		//if($query->num_rows() > 0)
		//{
			//die("You have run this sample, Please select other sample.");
		//	$this->error_handle($pages,'You have run this sample, Please select other sample.');
		//}
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "run_peat_check_db", "run_peat_check_db success");
		//********************************//
		
		return $DB_user;
	}
	
	function run_peat_save_run($instance_id, $project_name, $sample_name, $appresult_name, $AllFileSize, $reRun=1)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "run_peat_save_run", "run_peat_save_run init");
		//********************************//
		$this->load->database();
		// 儲存 Runs 到 DB
		$data = Array(
			"InstanceId"						=>	$instance_id,
			"task"									=>	$_SESSION['app_name'],
			"access_token"					=>	$_SESSION['BS_access_token'],
			"HrefProjects" 					=>	$this->project_create['Href'],
			"HrefSamples" 					=>	$this->sample_data['Href'],
			"HrefAppresult"					=>	$this->appresult_create['Href'],
			"AppSession" 						=>	$_SESSION['BS_appsessionuri'],
			"status"								=>	10,
			"HrefUser"							=>	$this->user_data['Href'],
			"ProjectName"						=>	$project_name,
			"SampleName"						=>	$sample_name,
			"AppresultName"					=>	$appresult_name,
			"DateCreate"						=>	time(),
			"DateFinish"						=>	0,
			"AllFilesSize"					=>	$AllFileSize,
			"reRun"									=>	$reRun
		);
		$sql = $this->db->insert_string('peat_runs', $data);
		$this->db->query($sql);
		$run_id = $this->db->insert_id();
		
		//********** Saving Logs *********//
		$this->csalt_log->add(2, "BS", "run_peat_save_run", "run_peat_save_run success");
		//********************************//
		
		return $run_id;
	}
	function run_peat_update_run($run_id, $instance_id, $project_name, $sample_name, $appresult_name, $AllFileSize, $reRun=0)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "run_peat_update_run", "run_peat_update_run init");
		//********************************//
		
		$this->load->database();
		
		// 儲存 Runs 到 DB
		$data = Array(
			"InstanceId"						=>	$instance_id,
			//"task"									=>	$_SESSION['app_name'],
			//"access_token"					=>	$_SESSION['BS_access_token'],
			"HrefProjects" 					=>	$this->project_create['Href'],
			"HrefSamples" 					=>	$this->sample_data['Href'],
			"HrefAppresult"					=>	$this->appresult_create['Href'],
			//"AppSession" 						=>	$_SESSION['BS_appsessionuri'],
			"status"								=>	10,
			"HrefUser"							=>	$this->user_data['Href'],
			"ProjectName"						=>	$project_name,
			"SampleName"						=>	$sample_name,
			"AppresultName"					=>	$appresult_name,
			"DateCreate"						=>	time(),
			"DateFinish"						=>	0,
			"AllFilesSize"					=>	$AllFileSize,
			"reRun"									=>	$reRun
		);
		
		$where = "uid = $run_id"; 

		$sql = $this->db->update_string('peat_runs', $data, $where);
		$this->db->query($sql);
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "run_peat_update_run", "run_peat_update_run success");
		//********************************//
		
		return $run_id;
	}
	
	function run_peat_save_files($run_id, $paired_files)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "run_peat_save_files", "run_peat_save_files init");
		//********************************//
		//計算解壓縮後檔案大小
		/*
		$total_file_count = 0;
		foreach($this->BS->sample_files['Items'] as $fItem)
		{
			if(strstr($fItem['Name'],'R1') || strstr($fItem['Name'],'R2'))
			{
				++$total_file_count;
			}
		}
		$R1_avg_size = (($this->BS->sample_data['Read1']*2+35)*$this->BS->sample_data['NumReadsRaw'] )/$total_file_count;
		$R2_avg_size = (($this->BS->sample_data['Read2']*2+35)*$this->BS->sample_data['NumReadsRaw'] )/$total_file_count;
		*/
		foreach($paired_files as &$file_2)
		{
			//$file_2['R1']['OSize'] = (int)$R1_avg_size;
			//$file_2['R2']['OSize'] = (int)$R2_avg_size;
			$file_2['R1']['OSize'] = $file_2['R1']['Size']*2.5;
			$file_2['R2']['OSize'] = $file_2['R2']['Size']*2.5;
		}
		
		// 儲存 Files 到 DB
		foreach($paired_files as $file)
		{
			$data = Array(
				"run_id"								=>	$run_id,
				"is_paired"							=>	1,
				"type"									=>	1,
				"f1_Name"								=>	$file['R1']['Name'],
				"f1_Href"								=>	$file['R1']['Href'],
				"f1_Size"								=>	$file['R1']['Size'],
				"f1_OSize"							=>	$file['R1']['OSize'],
				"f2_Name"								=>	$file['R2']['Name'],
				"f2_Href"								=>	$file['R2']['Href'],
				"f2_Size"								=>	$file['R2']['Size'],
				"f2_OSize"							=>	$file['R2']['OSize']
			);
			$sql = $this->db->insert_string('peat_files', $data);
			$this->db->query($sql);
		}
		
		//********** Saving Logs *********//
		$this->csalt_log->add(2, "BS", "run_peat_save_files", "run_peat_save_files success");
		//********************************//
		
	}
	
	
	
	// !Other
	//#################### OTHER ###################
	
	function get_simple_url($url)
	{
		
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "get_simple_url", "get curl init, $url");
		//********************************//
		
		$options = Array(
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => Array("x-access-token: {$this->app_access_tocken}"),
			CURLOPT_VERBOSE => true,
			CURLOPT_RETURNTRANSFER => true
		);
		$response = $this->query_curl($options);
		$results = json_decode($response,true);
		
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "get_simple_url", "get curl result success, $url");
		//********************************//
		
		//#############DeBug#########################
		if( $this->debug )
		{
			//var_dump($options);
			var_dump($results);
		}
		//###########################################
		
		$this->error_handle($results);
		
		return $results;
	}
	
	function get_require_from_js($href)
	{
		$url = "{$this->base_api_url}/{$href}?Limit=1000";
		$options = Array(
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => Array("x-access-token: {$this->app_access_tocken}"),
			CURLOPT_VERBOSE => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true
		);
		$response = $this->query_curl($options);
		return json_decode($response,true);
	}
	
	
	function error_handle($results)
	{
		if( isset($results['error_code']) || isset($results['error_description']) )
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "BS", "error_handle", "BS response error_code, " . json_encode($results));
			//********************************//
			
			echo "ERROR! \n";
			var_dump($results);
			exit;
			
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function query_curl($opts)
	{
		$ch = curl_init();
		curl_setopt_array($ch, $opts);
		$response = curl_exec($ch); 
		curl_close($ch);
		return $response;
	}
	
	
	// !New version for small rna pipeline
	
	
	function run_check_init()
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "run_check_init", "run check init");
		//********************************//
		
		$this->load->model('Error_handle', 'EH');
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
	
		//if( !isset($_SESSION['BS_authorization_code']) || !isset($_SESSION['BS_appsessionuri']) )
		//	$this->EH->error_handle($pages,'No session file! or No authorization code!');
		
		if( !isset($_SESSION['BS_access_token']) )
		{
			$this->EH->error_handle($pages,'No access_token!');
		}
			
		
		if(!isset($_POST['sample_select']))
		{
			$this->EH->error_handle($pages,'No sample selection!');
		}
		else
		{
			$href = $_POST['sample_select'];
		}
			
		
		if(!isset($_POST['files_select']))
		{
			$this->EH->error_handle($pages,'No files selected!');
		}
			
		//else
		//	$selected_files = json_decode($_POST['files_select']);
		$this->init($_SESSION['app_name'], NULL, $_SESSION['BS_appsessionuri'], $_SESSION['bs_type']);
		$this->set_access_token($_SESSION['BS_access_token']);
		$this->get_user_info();
		$this->get_sample_info($href);
		$this->get_files_from_sample();
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "run_check_init", "run check success");
		//********************************//
	}
	
	
	//file_type, single=1 or paired=2
	function run_check_selected_files($file_type=2)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "run_peat_check_files", "run_peat_check_files init");
		//********************************//
		
		$this->load->model('Error_handle', 'EH');
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		
		$href = $_POST['sample_select'];
		$selected_files = json_decode($_POST['files_select']);
		
		//處理選擇的 Files
		foreach($this->sample_files['Items'] as $fItem)
		{
			if(in_array($fItem['Href'], $selected_files))
			{
				$tmp_array[ $fItem['Name'] ] = $fItem;
			}
		}
		// 檢查 IsPairedEnd， appresults 的 file 結果，可能沒有此info
		if(!isset($this->sample_data['IsPairedEnd']))
		{
			$this->sample_data['IsPairedEnd'] = ($file_type==1?false:true);
		}
		
		//檢查是否為 file_type
		if($this->sample_data['IsPairedEnd'] == true && $file_type == 2)
		{
			// ok paired end
			//檢查數目
			if(count($tmp_array)%2 != 0)
			{
				$this->EH->error_handle($pages,'Selected Fastq files are not paired.');
			}
			//檢查是否配對，以及整理配對檔案
			$files = $this->get_paired_file($tmp_array);
			if(is_string($files))
			{
				$this->EH->error_handle($pages, $files);
			}
			
		}
		else if($file_type == 1)//&& $this->sample_data['IsPairedEnd'] == false ) //有可能sample為paired但使用者要跑 single
		{
			// ok single end
			//檢查是否配對，以及整理配對檔案
			$files = $this->get_single_file($tmp_array);
			if(is_string($files))
			{
				$this->EH->error_handle($pages, $files);
			}
		}
		else
		{
			$this->EH->error_handle($pages,' This sample is not paired!');
		}	
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "run_check_selected_files", "run_check_selected_files success");
		//********************************//
		
		return $files;
	}
	function get_all_file_size($files, $file_type=2)
	{
		$AllFileSize = 0;
		if($file_type == 2)
		{
			foreach($files as $file)
			{
				$AllFileSize += $file['R1']['Size'];
				$AllFileSize += $file['R2']['Size'];
			}
		}
		else
		{
			foreach($files as $file)
			{
				$AllFileSize += $file['R1']['Size'];
			}
		}
		return $AllFileSize;
	}
	
	//檢查資料庫，使用者資訊以及是否已經app session跑過
	//1. is user in db
	//2. is app session already run
	function run_check_user()
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "run_peat_check_db", "run_peat_check_db init");
		//********************************//
		
		$this->load->database();
		$this->load->model('Error_handle', 'EH');
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		
		$query = $this->db->get_where('peat_user', array("Href"=>$this->user_data['Href']) );
		if($query->num_rows() == 0)
		{
			$this->EH->error_handle($pages,'No user info in db.');
		}
		else
		{
			$DB_user = $query->row_array();
		}
		
		$query = $this->db->get_where('peat_runs', array("HrefUser"=>$this->user_data['Href'], "AppSession" => $_SESSION['BS_appsessionuri'] ) );
		
		if($query->num_rows() > 0)
		{
			$this->EH->error_handle($pages,'You have run this app session.');
		}
		
		//$query = $this->db->get_where('peat_runs', array("HrefUser"=>$this->BS->user_data['Href'], "HrefSamples" => $this->BS->sample_data['Href'] ) );
		//if($query->num_rows() > 0)
		//{
			//die("You have run this sample, Please select other sample.");
		//	$this->error_handle($pages,'You have run this sample, Please select other sample.');
		//}
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "BS", "run_peat_check_db", "run_peat_check_db success");
		//********************************//
		
		return $DB_user;
	}
	
	function run_purchase_check($BS_purchaseid)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "run", "run_purchase_check", "check purchase");
		//********************************//
		
		if( !isset($_SESSION['request_run_info']) )
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "run", "run_purchase_check", "Error! No request_run, or timeout. <br> Or you may have run this session, or have not trigger this session.");
			//********************************//
			die("Error! No request_run, or timeout. <br> Or you may have run this session, or have not trigger this session.");
		}
		if($BS_purchaseid == 'Free')
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(3, "run Free", "run_purchase_check", "Free version no need check");
			//********************************//
			
			//No Free version
			return false;
			//return true;
		}
		else
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(3, "run", "run_purchase_check", "Check non-free purchase");
			//********************************//
			
			$this->load->model('Csalt_baseSpace_api', 'BS');
			$this->BS->session_check_init($_SESSION['app_name'], NULL, NULL, $_SESSION['bs_type']);
			$check = $this->BS->request_buy_update($BS_purchaseid);
			
			if($check['Status'] == "COMPLETED")
			{
				return true;
			}
			else
			{
				die("Sorry! this product have not purchased");
			}
			//not free	
		}
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "run", "run_purchase_check", "check purchase success");
		//********************************//
		return false;
	}
	
	function run_save($instance_id, $project_name, $sample_name, $appresult_name, $AllFileSize, $reRun=1)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "run_save", "run_save init");
		//********************************//
		$this->load->database();
		// 儲存 Runs 到 DB
		$data = Array(
			"InstanceId"					=>	$instance_id,
			"task"							=>	$_SESSION['app_name'],
			"access_token"					=>	$_SESSION['BS_access_token'],
			"HrefProjects" 					=>	$this->project_create['Href'],
			"HrefSamples" 					=>	$this->sample_data['Href'],
			"HrefAppresult"					=>	$this->appresult_create['Href'],
			"AppSession" 					=>	$_SESSION['BS_appsessionuri'],
			"status"						=>	10,
			"HrefUser"						=>	$this->user_data['Href'],
			"ProjectName"					=>	$project_name,
			"SampleName"					=>	$sample_name,
			"AppresultName"					=>	$appresult_name,
			"DateCreate"					=>	time(),
			"DateFinish"					=>	0,
			"AllFilesSize"					=>	$AllFileSize,
			"reRun"							=>	$reRun
		);
		$sql = $this->db->insert_string('peat_runs', $data);
		$this->db->query($sql);
		$run_id = $this->db->insert_id();
		
		//********** Saving Logs *********//
		$this->csalt_log->add(2, "BS", "run_save", "run_save success");
		//********************************//
		return $run_id;
	}
	function run_save_files($run_id, $files, $file_type=2)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "BS", "run_save_files", "run_save_files init");
		//********************************//
		
		//paired
		if($file_type == 2)
		{
			
			foreach($files as &$file_2)
			{
				$file_2['R1']['OSize'] = $file_2['R1']['Size']*2.5;
				$file_2['R2']['OSize'] = $file_2['R2']['Size']*2.5;
			}
			// 儲存 Files 到 DB
			foreach($files as $file)
			{
				$data = Array(
					"run_id"							=>	$run_id,
					"is_paired"							=>	1,
					"type"								=>	1,
					"f1_Name"							=>	$file['R1']['Name'],
					"f1_Href"							=>	$file['R1']['Href'],
					"f1_Size"							=>	$file['R1']['Size'],
					"f1_OSize"							=>	$file['R1']['OSize'],
					"f2_Name"							=>	$file['R2']['Name'],
					"f2_Href"							=>	$file['R2']['Href'],
					"f2_Size"							=>	$file['R2']['Size'],
					"f2_OSize"							=>	$file['R2']['OSize']
				);
				$sql = $this->db->insert_string('peat_files', $data);
				$this->db->query($sql);
			}
		}
		//single
		else
		{
			foreach($files as &$file_2)
			{
				$file_2['R1']['OSize'] = $file_2['R1']['Size']*2.5;
			}
			// 儲存 Files 到 DB
			foreach($files as $file)
			{
				$data = Array(
					"run_id"							=>	$run_id,
					"is_paired"							=>	0,
					"type"								=>	1,
					"f1_Name"							=>	$file['R1']['Name'],
					"f1_Href"							=>	$file['R1']['Href'],
					"f1_Size"							=>	$file['R1']['Size'],
					"f1_OSize"							=>	$file['R1']['OSize']
				);
				$sql = $this->db->insert_string('peat_files', $data);
				$this->db->query($sql);
			}
		}
		
		
		
		//********** Saving Logs *********//
		$this->csalt_log->add(2, "BS", "run_save_files", "run_save_files success");
		//********************************//
	}
	function run_save_parameter($run_id, $para)
	{
		$data = array(
			 "run_id"		=> $run_id
			,"json"			=> json_encode($para)
		);
		$sql = $this->db->insert_string('run_parameter', $data);
		$this->db->query($sql);
		//********** Saving Logs *********//
		$this->csalt_log->add(2, "BS", "run_save_parameter", "run_save_parameter success");
		//********************************//
	}
}






















?>
