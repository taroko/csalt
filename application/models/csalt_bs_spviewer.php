<?php

class Csalt_bs_spviewer extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	var $default_BS_model = "Csalt_bs_api";
	
	public $products		= array(
		//					 name     GB  day $$   key
		"1days" 	=> array("1days", 28, 1, 0, "aaaaaaaaaaaa"),
		"7days" 	=> array("7days", 28, 7, 490, "2c92c0f948bfe18a0148c01687fa31e0"),
		"30days"	=> array("30days", 28, 30, 990, "2c92c0f848bfcf0c0148c016d62e4826"),
		"90days"	=> array("90days", 28, 90, 2490, "2c92c0f948bfe18b0148c0174df72b42")
	);
	function get_product_by_key(&$key)
	{
		foreach($this->products as &$product)
		{
			if($product[4] == $key)
				return $product;
		}
		return false;
	}
	function get_appsession_properties(&$appsession, $name)
	{
		foreach($appsession["Properties"]["Items"] as &$properties)
		{
			if($properties["Name"] == $name)
				return $properties;
		}
		return false;
	}
	function get_user_pass($hrefuser, $passwd_prefix)
	{
		// get user info
		//$passwd_prefix is instance
		$username = "user";
		if(!strstr($hrefuser, "@"))
		{
			$sql_result = $this->db->get_where('peat_user', array("Href" => $hrefuser));
			$row = $sql_result->row_array();
			$username = explode("@", $row['Email'])[0];
		}
		else
		{
			$username = explode("@", $hrefuser)[0];
		}
		$password = substr(md5($passwd_prefix."5566"),0,8);
		return array($username, $password);
	}
	
	function check_appsession(&$appsession)
	{
		$inputs = $this->get_appsession_properties($appsession, "Input.app-result-id");
		$outputs = $this->get_appsession_properties($appsession, "Input.project-id");
		$billing_type = $this->get_appsession_properties($appsession, "Input.billing-type");
		$session_name = $this->get_appsession_properties($appsession, "Input.app-session-name");
		
		$product_name = $billing_type["Content"];
		$product = $this->products[$product_name];
		
		$total_size = 0;
		// check inputs
		foreach($inputs["Items"] as $input)
		{
			if($input["Name"] != "small_rna_pipeline")
				$this->log("error", __CLASS__.".".__FUNCTION__, "", "Appresult Name", "{$input['Name']} != small_rna_pipeline.");
			if($input["Status"] != "Complete")
				$this->log("error", __CLASS__.".".__FUNCTION__, "", "Appresult Status Not Complete", "Appresult Status not complete.");
			$total_size += $input["TotalSize"];
		}
		if($total_size > $product[1]*1024*1024*1024)
			$this->log("error", __CLASS__.".".__FUNCTION__, "", "Size Too Large", "Total size of appresults is more than {$product[0]}G");
		
		$view = array(
			"inputs" => $inputs,
			"outputs" => $outputs,
			"session_name" => $session_name,
			"product" => $product
		);
		return $view;
	}
	
	function run_command(&$run)
	{
		sleep(30);
		$access_token = $run['access_token'];
		$appsessionid = explode("/", $run['AppSession']);
		$appsessionid = $appsessionid[2];
		
		$query = $this->db->get_where( 'instances', array("InstanceId"=>$run['InstanceId'] ) );	
		$instance = $query->row_array();
		
		$this->utility->log("info", __CLASS__.".".__FUNCTION__, $run['app_name'], "Running Command Start", "Running Command Start");
		
		$this->load->model('Csalt_ssh_model', 'SSH');
		$is_success = $this->SSH->ssh_connect_by_key($instance['PublicDnsName'], "ubuntu", "/var/www/csalt/application/models/basespace.pem");
		
		if(!$is_success)
			return false;
		
		if(strstr($run['app_name'], 'hoth'))
			$platform = "hoth.cloudxdna.com";
		else
			$platform = "www.cloudxdna.com";
		
		$cmd = "mkdir /home/ubuntu/genomes";
		echo $this->SSH->ssh_command($cmd);
		
		$cmd = "sudo mkfs.ext3 /dev/xvdb && sudo mount /dev/xvdb /home/ubuntu/genomes";
		echo $this->SSH->ssh_command($cmd);

		$cmd = "sudo apt-get update"; // for update docker 2015/05/24
		echo $this->SSH->ssh_command($cmd);
		
		$cmd = 'sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install docker.io';
		//$cmd = "sudo apt-get install -qq --force-yes docker.io"; // for update docker 2015/05/24
		echo $this->SSH->ssh_command($cmd);
	
		$userpass = $this->get_user_pass($run['HrefUser'], $run['InstanceId']);
		
		$cmd = "nohup sudo docker run --privileged=true -p 5566:22 -p 80:80 -e access_token_url=http://$platform/bs/get_appsession/$access_token -e AppSessionId=$appsessionid -e AccessToken=$access_token -e user={$userpass[0]} -e pass={$userpass[1]} -v /home/ubuntu/genomes:/genomes -t poi5305/viewer > log_docker 2>&1 &";
		echo $this->SSH->ssh_command($cmd);
		
		$this->utility->log("info", __CLASS__.".".__FUNCTION__, $run['app_name'], "Running Command Finish", "Running Command Success. $access_token");
		return true;
	}
	
	function browse_note(&$run)
	{
		// check is refund
		$is_refund = $this->check_is_refund($run);
		
		$this->load->database();
		// check viewer instance status and get url to viewer
		$instance = NULL;
		$query = $this->db->get_where('instances', array("InstanceId"=>$run['InstanceId'] ) );
		if($query->num_rows() != 0)
			$instance = $query->row_array();
		
		$user_pass = $this->get_user_pass($run['HrefUser'], $instance['InstanceId']);
		
		return $this->load->view('basespace/view_bs_spviewer_item', array("run"=>$run, "is_refund"=>$is_refund, "instance" => $instance, "user"=>$user_pass[0]), true );
	}
	
	// 每個 app refund 機制不同
	function check_is_refund(&$RUN)
	{
		$is_refund = false;
		if(time() - $RUN['DateCreate'] < 24*60*60)
			$is_refund = true;
		else if($RUN['status'] != 1)
			$is_refund = true;
		
		if($RUN['RefundStatus'] != "NOTREFUNDED")
		{
			$is_refund = false;
		}
		return $is_refund;
	}
	
	function get_email_message($status, $user_name="", $browse_url="", $p1="", $p2="", $p3="", $p4="")
	{
		$msg = array();
		$msg['RunStart']['title'] = "Thank you for running sRNA Viewer !!";
		$msg['RunStart']['content'] = "Hello {$user_name} ~! \nThank you for running sRNA Viewer, it is pending.\nYou can browse your jobs from this link: $browse_url\nViewer login information:\nUsername: $p1\nPassword: $p2\nIf you have any questions, please send a reply to obigbando@gmail.com.\nCloudxDNA";
		
		$msg['RunFinish']['title'] = "sRNA Viewer Run !!";
		$msg['RunFinish']['content'] = "Hello {$user_name} ~! \nThank you for running sRNA Viewer, it has been Launched.\nYou can use your Viewer from this link: $browse_url\nViewer login information:\nUsername: $p1\nPassword: $p2\n\nIf you have any questions, please send a reply to obigbando@gmail.com.\nCloudxDNA";
		
		$msg['TimeUp']['title'] = "sRNA Viewer Service Termination !!";
		$msg['TimeUp']['content'] = "Hello {$user_name} ~! \nThank you for running sRNA Viewer, your service is terminated. We hope you enjoy our service, and see you next time.\nCloudxDNA";
		
		$msg['TimeOut']['title'] = "sRNA Viewer Service Pending|Running Error !";
		$msg['TimeOut']['title'] = "Hello {$user_name} ~! \nWe are Sorry about instance Pending(Running) error. You can refund first from this link: $browse_url.\n And try reRUN your app result please.\n And Thank you for running sRNA Viewer \nCloudxDNA";
		
		return $msg[$status];
	}
	
	
}






















?>
