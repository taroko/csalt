<?php
/**
 *  @file csalt_basespace_peat.php
 *  @brief csalt_basespace_peat.php
 *  @author JHH Corp.
 */
 
/**
 * @struct Csalt_baseSpace_peat
 * 
 * @brief Csalt_baseSpace_peat Model
 *  
 * @tparam
 *  
 */
class Csalt_baseSpace_srnap extends CI_Model
{
	function __construct()
	{
      parent::__construct();
  }

	/* file size, price, id */
	public $peat_product			= array(
		"SmallRNApipeline"			=> array(100, 10, "2c92c0f84495ff400144b01891021a03")
	);
	
	function exam_submited_parameter()
	{
		$this->load->model('Error_handle', 'EH');
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		
		if( !isset($_SESSION['BS_access_token']) )
		{
			$this->EH->error_handle($pages,'No access_token!');
		}
		
		if(!isset($_POST["files_select"]) 
			|| !isset($_POST["project_select"])
			|| !isset($_POST["sample_select"])
			|| !isset($_POST["sample_type"])
			|| !isset($_POST["genome"])
			|| !isset($_POST["barcode"])
			|| !isset($_POST["barcode_seq"])
			|| !isset($_POST["adapter"])
			|| !isset($_POST["adapter_seq"])
		)
		{
			$this->EH->error_handle($pages,'Error! parameter error');
		}
		if($_POST["barcode"] == 0)
			$_POST["barcode_seq"] = array();
		if($_POST["adapter"] == 0)
			$_POST["adapter_seq"] = "";
		if(count($_POST["barcode_seq"]) == 0)
			$_POST["barcode"] = 0;
		
		foreach($_POST["barcode_seq"] as &$seq)
			$seq = strtoupper($seq);
		
		$_POST["adapter_seq"] = strtoupper($_POST["adapter_seq"]);
		
		for($j=0; $j<count($_POST["barcode_seq"]); $j++)
		{
			for($i=0; $i<strlen($_POST["barcode_seq"][$j]); $i++)
			{
				$char = $_POST["barcode_seq"][$j][$i];
				if($char == "A" || $char == "C" || $char == "G" || $char == "T" )
				{
					// ok
				}
				else
				{
					$this->EH->error_handle($pages,'barcode sequence only allow A, C, G, T');
				}
			}
		}
			
		for($i=0; $i<strlen($_POST["adapter_seq"]); $i++)
		{
			$char = $_POST["adapter_seq"][$i];
			if($char == "A" || $char == "C" || $char == "G" || $char == "T" )
			{
				// ok
			}
			else
			{
				$this->EH->error_handle($pages,'adapter sequence only allow A, C, G, T');
			}
		}
		return $_POST;
	}
	
	/**
	 * @brief peat_buy
	 * Csalt_baseSpace_peat::peat_buy()
	 */
	function srnap_buy()
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "srnap", "srnap_buy", "request buy");
		//********************************//
		
		$this->load->model('Csalt_baseSpace_api_v2', 'BS');
		
		//檢查參數
		$para = $this->exam_submited_parameter();
		
		// set_access_token
		// get_user_info
		// get_sample_info
		// get_files_from_sample
		$sample_href = $para["sample_select"];
		$this->BS->run_check_init();
		$this->BS->get_user_info();
		$this->BS->get_sample_info($sample_href);
		$this->BS->get_files_from_sample();
		
		//檢查檔案正確性
		// check $_POST['files_select']
		$files = $this->BS->run_check_selected_files( $para["sample_type"] );
		//取得所有檔案大小
		$AllFileSize = $this->BS->get_all_file_size($files, $para["sample_type"]);
		
		if($AllFileSize == 0)
			$this->error_handle($pages,'File size ERROR');
		if($AllFileSize > 100 *1024 *1024 *1024)
			$this->error_handle($pages,'Sorry, your files are too large, > 100G');
		
		//檢查資料庫，使用者資訊以及是否已經app session 跑過
		//1. is user in db
		//2. is app session already run
		$DB_user = $this->BS->run_check_user();
		
		$_SESSION['request_run_info'] = Array(
			'sample_name'					=> $this->BS->sample_data['Name']
			,'sample_href'					=> $this->BS->sample_data['Href']
			,'files'						=> $files
			,'AllFileSize'					=> $AllFileSize
			,'DB_user'						=> $DB_user
			,'parameter'					=> $para
		);
		
		if($AllFileSize <= 0.000 *1024 *1024 *1024)
		{
			// NO FREE
			//For Free
			//$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
			//$pages['main_page'] = $this->load->view('basespace/view_basespace_peat_run_ok', $data, true );
		}
		else if($AllFileSize <= 100 *1024 *1024 *1024)
		{
			// purchase
			$peat_product_name = "SmallRNApipeline";
			$product_id = $this->peat_product[$peat_product_name][2];
			$_SESSION['purchase_id'] = $this->BS->request_buy($product_id, $_SESSION['app_name']);
		}
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "peat", "peat_buy", "request buy success");
		//********************************//
		
		if(!isset($_SESSION['order_id']))
			$_SESSION['HrefPurchaseDialog'] = $this->BS->purchase_response['HrefPurchaseDialog'];
		
		//echo $_SESSION['HrefPurchaseDialog'];

		// save $_SESSION for order
		if(!isset($_SESSION['order_id']))
			$_SESSION['order_id'] = 0;
		$_SESSION['order_id'] = $this->BS->run_save_order( $_SESSION['order_id'], $_SESSION['BS_Id'], 0, 2);
		
		
		//For Free
		//$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		//$pages['main_page'] = $this->load->view('basespace/view_basespace_peat_run_ok', $data, true );
		
		$request_run = $_SESSION['request_run_info'];
		$request_sample_name = $request_run['sample_name'];
		$request_sample_href = $request_run['sample_href'];
		$request_paired_files = $request_run['files'];
		$request_AllFileSize = $request_run['AllFileSize'];
		$request_DB_user = $request_run['DB_user'];
		$request_parameter = $request_run["parameter"];
		
		$data = array(
			'DB_user'				=>	$request_DB_user,
			'Files'					=>	$request_paired_files,
			'sample_name'			=>	$request_sample_name,
			'AllFileSize'			=>	$request_AllFileSize,
			'parameter'				=>	$request_parameter,
			'purchase_url'          =>  $_SESSION['HrefPurchaseDialog']
		);
		
		$pages['sub_menu'] = $this->load->view("basespace/view_basespace_sub_menu",'', true );
		$pages['main_page'] = $this->load->view("basespace/view_basespace_{$_SESSION['app_name']}_check", $data, true );
				
		
		return $pages;
		//header("Location: $HrefPurchaseDialog");
		
		//return false;
	}
	
	function srnap_run()
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "srnap_run", "srnap_run", "srnap run init");
		//********************************//
		
		$this->load->model('Csalt_baseSpace_api_v2', 'BS');
		
		//########## app code, app session, init BS api
		// $_SESSION['BS_authorization_code']
		// $_SESSION['BS_appsessionuri']
		// $_SESSION['BS_access_token']
		// $_SESSION['app_name']
		$this->BS->session_check_init($_SESSION['app_name'], $_SESSION['BS_appsessionuri'], $_SESSION['BS_authorization_code'], $_SESSION['bs_type']);
		
		if(!isset($_SESSION['request_run_info']))
		{
			die("request undefine ,<br> you may have run this session, or have not trigger this session.");
		}
		
		$request_run = $_SESSION['request_run_info'];
		$request_sample_name = $request_run['sample_name'];
		$request_sample_href = $request_run['sample_href'];
		$request_paired_files = $request_run['files'];
		//$request_paired_files = $request_run['paired_files'];
		$request_AllFileSize = $request_run['AllFileSize'];
		$request_DB_user = $request_run['DB_user'];
		$request_parameter = $request_run["parameter"];
		
		$project_name = "SmallRNAPipeline-proj-".$request_sample_name;
		$sample_name = $request_sample_name;
		$appresult_name = "SmallRNAPipeline-appr-".$request_sample_name."-".date('Y-m-d-H-i-s', time());
		
		// 新建 project
		$this->BS->create_project($project_name);
		
		// 新建 appresult
		$this->BS->create_appresult($appresult_name, "Small RNA pipeline result", $request_sample_href);
		
		//開機器並且執行
		$this->load->model('csalt_aws_ec2_api','AWS');
		//$instance_ids = $this->AWS->instance_launch('q_run_instance_srnap');
		//$instance_id = $instance_ids[0];
		
		$instance_id = 0;
		
		//lose some para
		$this->BS->sample_data['Href'] = $request_sample_href;
		$this->BS->user_data['Href'] = $request_DB_user['Href'];
		$run_id = $this->BS->run_save($instance_id ,$project_name, $sample_name, $appresult_name, $request_AllFileSize);
		
		//update order
		$this->BS->run_save_order( $_SESSION['order_id'], $_SESSION['BS_Id'], $run_id, 4);
		
		//Save parameters
		$this->BS->run_save_parameter($run_id, $request_parameter);
		
		//HAVE BUGS
		$this->BS->run_save_files($run_id, $request_paired_files, $request_parameter["sample_type"]);
		
		
		//wait for instance
		//$this->bg_curl("https://localhost/basespace/query_task_peat/waiting_for_instance_running/".$_SESSION['BS_access_token']);
		//$this->bg_curl("https://hoth.muggle.tw/basespace/query_task_peat/waiting_for_instance_running/".$_SESSION['BS_access_token']);
		//system("echo 'https://hoth.muggle.tw/basespace/query_task_peat/waiting_for_instance_running/{$_SESSION['BS_access_token']}' >> test");
		//$this->bg_curl("http://localhost/~andy/csalt/index.php/basespace_v2/query_task_peat/waiting_for_instance_running/".$_SESSION['BS_access_token']);
		$this->bg_curl($this->config->item('full_url')."basespace_v2/query_task_peat/waiting_for_instance_running/".$_SESSION['BS_access_token']);
		
		
		//Email to user
		//$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
		//$this->BS_PEAT->send_mail($request_DB_user, $_SESSION['app_name'], "start_run", NULL);

		//Show Html
		$data = array(
			'DB_user'				=>	$request_DB_user,
			'Files'					=>	$request_paired_files,
			'sample_name'			=>	$sample_name,
			'project_name'			=>	$project_name,
			'appresult_name'		=>	$appresult_name,
			'AllFileSize'			=>	$request_AllFileSize,
			'parameter'				=>	$request_parameter
		);
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "srnap", "srnap_run", "srnap_run run success");
		//********************************//
		
		unset($_SESSION['request_run_info']);

		return $data;
	}
	function run_command($run, $instance_id, $access_token)
	{
		$query = $this->db->get_where( 'peat_instance', array("InstanceId"=>$instance_id ) );	
		$instance = $query->row_array();
		
		$query = $this->db->get_where( 'run_parameter', array("run_id"=>$run["uid"] ) );	
		$run_parameter = $query->row_array();
		$run_para_detail = json_decode($run_parameter['json'], true);
		system("echo '$access_token ok: run_command start  \n' >> test");
		
		$this->load->model('Csalt_ssh_model', 'SSH');
		$this->SSH->ssh_connect_by_key($instance['PublicDnsName'], "ubuntu", $this->SSH->pem_location);
		
		$cmd = "rm -rf /c-salt/pokemon_public";
		echo $this->SSH->ssh_command($cmd);
		
		// clone git for srna pipeline
		$cmd = "cd /c-salt;git clone http://andy:qsefth@gitlab.jhhlab.tw/srna_pipeline.git";
		echo $this->SSH->ssh_command($cmd);
		system("echo '$access_token ok: run_command git clone  \n' >> test");
		// make include file
		/// barcode
		$include_content = $this->make_include_file($run_para_detail);

		echo "<br /> $include_content <br />";
		
		$cmd = "echo \"$include_content\" > /c-salt/srna_pipeline/pipeline/config.hpp";
		echo $this->SSH->ssh_command($cmd);
		
		system("echo '$access_token ok: run_command copy config file  \n' >> test");
		
		$cmd = "sudo apt-get install --force-yes -y libpng++-dev";
		echo $this->SSH->ssh_command($cmd);
		
		system("echo '$access_token ok: run_command apt install libpng++-dev  \n' >> test");
		$cmd = "cd /c-salt/srna_pipeline;cmake .;make srnap_trial;";
		echo $this->SSH->ssh_command($cmd);
		echo "make done\n";
		
		system("echo '$access_token ok: run_command make \n' >> test");
		
		if(strstr($run['task'], 'hoth'))
			$platform = "hoth.muggle.tw";
		else
			$platform = "peat.jhhlab.tw";
		
		$genome = $run_para_detail["genome"];
		$cmd = "cd /c-salt/srna_pipeline;sudo nohup /c-salt/srna_pipeline/srnap_trial http://$platform/basespace/query_task_peat/file_list/$access_token $genome > log 2>&1 &";
		echo $this->SSH->ssh_command($cmd);
		echo "<br> $cmd";
		//********** Saving Logs *********//
		$this->csalt_log->add(2, "basespace", "query_task_peat", "request waiting_for_instance_running: running peat task ");
		//********************************//
		
		system("echo '$access_token ok: run_command RUNNING... \n' >> test");
		// return new status
		return 9;
	}
	private function make_include_file($para)
	{
		$include_file_content = "";
		$barcode = $para["barcode"];
		$adapter = $para["adapter"];
		
		if($barcode == 0)
		{
			$include_file_content .= "typedef std::tuple <boost::mpl::string<'null'> >BarcodeType;";
		}
		else
		{
			$include_file_content .= "typedef std::tuple <";
			foreach($para["barcode_seq"] as $b_seq)
			{
				$include_file_content .= "boost::mpl::string<'{$b_seq}'>,";
			}
			$include_file_content = substr($include_file_content, 0, -1);
			$include_file_content .= " > BarcodeType;";
		}
		
		if($barcode == 3)
		{
			$include_file_content .= "typedef BarcodeHandler <BarcodeHandleScheme::Three_Prime, BarcodeType> BarcodeProcessor;";
		}
		// no barcode or 5p barcode
		else
		{
			$include_file_content .= "typedef BarcodeHandler <BarcodeHandleScheme::Five_Prime, BarcodeType> BarcodeProcessor;";
		}
		
		if($adapter != 0)
		{
			$a_seq = $para["adapter_seq"];
			$include_file_content .= "std::string g_adapter_seq = \\\"$a_seq\\\";";
		}
		else
		{
			$include_file_content .= "std::string g_adapter_seq;";
		}
		
		$db = $para["genome"];
		
		// annotation
		$include_file_content .= "struct AnnoTrait_MGI{ const char* file_path = \\\"ENSEMBL.$db.all.bed\\\"; };";
		
		return $include_file_content;
	}
	private function bg_curl($url)
	{
		$pid = pcntl_fork();
		if(!$pid)
		{
			system("echo 'start curl -k \"$url\"' >> test");
			shell_exec("curl -k \"$url\" >/dev/null 2>&1");
			system("echo 'finish curl -k \"$url\"' >> test");
			exit;	
		}
		return;
		
		system("echo 'nohup curl -k \"$url\" &' >> test");
		shell_exec("nohup curl -k \"$url\" >/dev/null 2>&1 &");
		
		return;
		
		$ch1 = curl_init();
		curl_setopt($ch1, CURLOPT_URL, $url);
		curl_setopt($ch1, CURLOPT_HEADER, 0);
		curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, 0);
		$mh = curl_multi_init();
		curl_multi_add_handle($mh,$ch1);
		$active = null;
		$mrc = curl_multi_exec($mh, $active);
	}

}

?>
