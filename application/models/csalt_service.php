<?php
require 'vendor/autoload.php';

class Csalt_service extends CI_Model
{
	function __construct()
  {
      parent::__construct();
  }
  function bg_program()
  {
		//nothing
  }
  
	function monitoring_runs()
	{
		$this->load->database();
		$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
		
		$limit_time = time() - 60*60*24*30;
		$query = $this->db->query("SELECT * FROM `peat_runs` WHERE `status`>'1' AND `DateCreate` > '$limit_time'");
		
		foreach($query->result_array() as $row)
		{
			
			$t = time() - $row['DataRefresh'];
			if($t > 60*20 && $t < 60*30)
			{
				//********** Saving Logs *********//
				$this->csalt_log->add(2, "service", "monitoring_runs", "Error, DataRefresh time is too long, >20min, {$row['access_token']}");
				//********************************//
				
				$this->BS_PEAT->send_mail_manual("RUN ERROR!","DataRefresh time is too long, >20min. \nAccess_token: {$row['access_token']}","poi5305@gmail.com");
			}
			elseif($t > 60*50 && $t < 60*60)
			{
				//********** Saving Logs *********//
				$this->csalt_log->add(2, "service", "monitoring_runs", "Error, DataRefresh time is too long, >50min, {$row['access_token']}");
				//********************************//
				
				$this->BS_PEAT->send_mail_manual("RUN ERROR!","DataRefresh time is too long >60min. Terminating... \nAccess_token: {$row['access_token']}","poi5305@gmail.com");
				
				$this->bg_curl("http://localhost/basespace/query_task_peat/failure/{$row['access_token']}");
			}
			elseif($row['DataRefresh'] == 0 && (time()-$row['DateCreate']) > 60*60)
			{
				//********** Saving Logs *********//
				$this->csalt_log->add(2, "service", "monitoring_runs", "Error, DateCreate 60min, and DataRefresh is ZERO , {$row['access_token']}");
				//********************************//
				
				$this->BS_PEAT->send_mail_manual("RUN ERROR!","DataCreate 60min, and DataRefresh is ZERO. Terminating... \nAccess_token: {$row['access_token']}","poi5305@gmail.com");
				
				$this->bg_curl("http://localhost/basespace/query_task_peat/failure/{$row['access_token']}");
			}
		}
	}
	
	function monitoring_instance()
	{
		$this->load->model('csalt_aws_ec2_api','AWS');
		$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
		$this->load->database();
		$query = $this->db->query("SELECT * FROM `peat_instance` WHERE `State` != 'terminated' AND `State` != 'shutting-down' AND `State` !='unknown' ");
		//$query = $this->db->get_where("peat_instance", array("State"=>"shutting-down") );
		$array = Array();
		foreach($query->result_array() as $row)
		{
			
			$array[] = $row['InstanceId'];
			$launchTime = time() - $row['LaunchTime'];
			if( $launchTime > 60*60*24 && $launchTime < 60*60*24+10*60)
			{
				//********** Saving Logs *********//
				$this->csalt_log->add(2, "service", "monitoring_instance", "RUN TOO LONG!, {$row['InstanceId']}, monitoring_instance, run time is too long >1 day.");
				//********************************//
				
				$this->BS_PEAT->send_mail_manual("RUN TOO LONG!","monitoring_instance, run time is too long >1 day. \nInstanceId:{$row['InstanceId']}","poi5305@gmail.com");
			}
		}
		try
		{
			if(count($array)!=0);
				$this->AWS->instance_update_status($array);
		}
		catch(Exception $e)
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "service", "monitoring_instance", "Error!, Some instance id is not exist in aws.".json_encode($array) );
			//********************************//
			//把在AWS已經不存在的機器設為 unknown，才不會一直出現錯誤
			foreach($array as $instance_id)
			{
				$sql = $this->db->update_string('peat_instance', array('State'=>'unknown'), "InstanceId = '$instance_id'");
				$this->db->query($sql);
			}
		}
		
	}
	
	
	private function bg_curl($url)
	{
		system("echo 'nohup curl -k \"$url\" &' >> test");
		shell_exec("nohup curl -k \"$url\" >/dev/null 2>&1 &");
		return;
		$ch1 = curl_init();
		curl_setopt($ch1, CURLOPT_URL, $url);
		curl_setopt($ch1, CURLOPT_HEADER, 0);
		$mh = curl_multi_init();
		curl_multi_add_handle($mh,$ch1);
		$active = null;
		$mrc = curl_multi_exec($mh, $active);
	}
}
?>