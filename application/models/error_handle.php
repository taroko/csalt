<?php
require 'vendor/autoload.php';

class Error_handle extends CI_Model
{
	function __construct()
  {
      parent::__construct();
  }
  
  var $error_view_page = "basespace/view_basespace_error.php";
  
  
  public function error_handle($data=array(), $error_h2='Unknow error!', $error_p='Please check.')
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(1, "Error_handle", "$error_h2", "$error_p");
		//********************************//
		
		if(!isset($data['main_page']))
			$data['main_page']		= $this->load->view($this->error_view_page, array('error_h2'=>$error_h2, 'error_p'=>$error_p), true);
		$this->createHtmlView($data);
		die();
	}
  
	public function createHtmlView($data=array())
	{
		if(!isset($data['head']))				$data['head']					= $this->load->view($this->config->item('template')."/view_head.php", '', true);
		if(!isset($data['header']))			$data['header']				= $this->load->view($this->config->item('template')."/view_header.php", '', true);
		if(!isset($data['main_menu']))	$data['main_menu']		= $this->load->view($this->config->item('template')."/view_main_menu.php", '', true);
		if(!isset($data['sub_menu']))		$data['sub_menu']			= $this->load->view($this->config->item('template')."/view_sub_menu.php", '', true);
		if(!isset($data['main_page']))	$data['main_page']		= $this->load->view($this->config->item('template')."/view_main_page.php", '', true);
		if(!isset($data['footer']))			$data['footer']				= $this->load->view($this->config->item('template')."/view_footer.php", '', true);
		echo $this->load->view($this->config->item('template')."/view_index.php", $data, true);
	}


}
?>