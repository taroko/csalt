<?php
require 'vendor/autoload.php';

class Csalt_aws_ec2_api extends CI_Model
{
	function __construct()
	{
      parent::__construct();
      $this->init();
  }
	//AWS ID 496508083411
	var $aws_key       = "AKIAIVHCLYAZVAZPJHAQ";
	var $aws_secret    = "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";
	var $aws_region    = "us-east-1";
	var $client        = NULL;
	
	var $q_image_list_self				= array
	(
		'Owners'					=> array('self')
	);
	
	var $q_run_instance_peat			= array
	(
		'ImageId'				=> 'ami-5f775e1a',
		'MinCount'				=> 1,
		'MaxCount'				=> 1,
		'InstanceType'			=> 'c1.medium',
		'KeyName'				=> 'basic',
		'UserData'				=> 'PEAT instance',
		'Placement'				=> array('AvailabilityZone' => 'us-west-1c'),
		'SecurityGroups'		=> array('basic_test')
	);
	
	var $q_run_instance_srnap			= array
	(
		'ImageId'				=> 'ami-5f775e1a',
		'MinCount'				=> 1,
		'MaxCount'				=> 1,
		//'InstanceType'			=> 'm3.2xlarge',
		//'InstanceType'			=> 'm3.xlarge',
		'InstanceType'			=> 'm1.small',
		'KeyName'				=> 'basic',
		'UserData'				=> 'PEAT instance',
		'Placement'				=> array('AvailabilityZone' => 'us-west-1c'),
		'SecurityGroups'		=> array('basic_test')
	);
	
	var $q_run_spot_instance_peat		= array
	(
		'SpotPrice'       => '0.05',
		'InstanceCount'   => 1,
		'LaunchSpecification'			=> array(
			'ImageId'							=>	'ami-5f775e1a',
			'KeyName'							=>	'basic',
			'UserData'							=>	'PEAT instance',
			'InstanceType'						=>	'c1.medium',
			'Placement'							=>	array('AvailabilityZone' => 'us-west-1c'),
			'SecurityGroups'					=>	array('basic_test')
		)
	);
	
	var $q_run_instance_spviewer		= array
	(
		'ImageId'				=> 'ami-68a80e00',
		'MinCount'				=> 1,
		'MaxCount'				=> 1,
		'InstanceType'			=> 'm3.medium',
		'KeyName'				=> 'basespace',
		'UserData'				=> 'SPViewer instance',
		'BlockDeviceMappings' => array(
		  array(
		  	'DeviceName' => '/dev/sdc',
		  	'VirtualName' => 'ephemeral0'
		  ),
		  array(
		  	'DeviceName' => '/dev/sdb',
		  	'Ebs' => array(
		  	  'VolumeSize' => 40,
		  	  'DeleteOnTermination' => true,
		  	  'VolumeType' => 'gp2', //'gp2',
		  	  'Encrypted' => false
		  	)
		  )
		  
		),
		'Placement'				=> array('AvailabilityZone' => 'us-east-1d'),
		'SecurityGroups'		=> array('Viewer_security_group')
	);
	
	function init()
	{
		$this->client = Aws\Ec2\Ec2Client::factory(array(
			'key'	=> $this->aws_key,
			'secret' => $this->aws_secret,
			'region' => $this->aws_region
		));
	}
	//################ Request ################
	function request($cmd, $info="")
	{
		//********** Saving Logs *********//
		//$this->csalt_log->add(3, "AWS", "request", "request init, $cmd");
		//********************************//
		
		if( !is_array($info) )
		{
			if($info != "")
				$info = $this->$info;
			else
				$info = array();
		}
		$result = $this->client->$cmd($info);
		
		//********** Saving Logs *********//
		//$this->csalt_log->add(3, "AWS", "request", "request success, $cmd");
		//********************************//
		
		return $result->toArray();
	}
	
	
	
	
	//################ do something ################
	function instance_launch($info)
	{
		//********** Saving Logs *********//
		//$this->csalt_log->add(4, "AWS", "instance_launch", "instance_launch init");
		//********************************//
		$this->load->database();
		$result = $this->AWS->request('runInstances', $info);
		
		$dba = $this->AWS->dba_run_instance($result);
		
		$return = array();
		
		foreach($dba as $instance_id => $data)
		{
			$return[] = $instance_id;
			$sql = $this->db->insert_string('instances', $data);
			$this->db->query($sql);
		}
		//********** Saving Logs *********//
		//$this->csalt_log->add(3, "AWS", "instance_launch", "instance_launch success");
		//********************************//
		return $return;
	}
	function instance_update_status($array=array())
	{
		//********** Saving Logs *********//
		//$this->csalt_log->add(4, "AWS", "instance_update_status", "instance_update_status init");
		//********************************//
		$this->load->database();
		$result = $this->AWS->request('describeInstances', array('InstanceIds' => $array ) );
		$dba = $this->AWS->dba_update_status_instance($result);
//print_r($array);
//print_r($dba);
//print_r($result);		
		//if(count($dba) == 0)
		//	$this->csalt_log->add(2, "AWS", "instance_update_status", "instance_update_status, aws return no data");
		
		$return = array();
		
		foreach($dba as $instance_id => $data)
		{
			if(!isset($data['State']))
				continue;
			$return[$instance_id] = $data['State'];
			$sql = $this->db->update_string('instances', $data, "InstanceId = '$instance_id'");
			$this->db->query($sql);
		}
		//********** Saving Logs *********//
		//$this->csalt_log->add(4, "AWS", "instance_update_status", "instance_update_status success");
		//********************************//
		return $return;
	}
	function instance_terminate($array)
	{
		//********** Saving Logs *********//
		//$this->csalt_log->add(4, "AWS", "instance_terminate", "instance_terminate init");
		//********************************//
		$this->load->database();
		
		try
		{
			$result = $this->request('terminateInstances', array('InstanceIds' => $array ) );
		}
		catch(Exception $e)
		{
			$this->load->database();
			if($e->getExceptionCode() == "InvalidInstanceID.NotFound" || $e->getStatusCode() == 400)
			{
				// instance is already terminated and the record have not exist. Auto change to terminated
				$this->db->update("instances", array("State"=>"terminated"), array("InstanceId"=>$array[0]) );
			}
			return true;
		}
		
		
		$dba = $this->dba_terminate_instance($result);
		
		$error = false;
		
		foreach($dba as $instance_id => $data)
		{
			if($data['TerminateTime'] == 0)
				$error=true;
			$sql = $this->db->update_string('instances', $data, "InstanceId = '$instance_id'");
			$this->db->query($sql);
		}
		//********** Saving Logs *********//
		//$this->csalt_log->add(3, "AWS", "instance_terminate", "instance_terminate success");
		//********************************//
		
		return !$error;
	}
	
	// describeSpotInstanceRequests
	// runInstances
	// requestSpotInstances
	// describeImages
	// describeInstances
	// terminateInstances




	//################ create query db data array ################
	
	function dba_run_instance($result)
	{
		//********** Saving Logs *********//
		//$this->csalt_log->add(4, "AWS", "dba_run_instance", "dba_run_instance init");
		//********************************//
		
		foreach($result['Instances'] as $instance)
		{
			$data[ $instance['InstanceId'] ] = array(
				'ReservationId'		=> $result['ReservationId'], 
				'InstanceId'		=> $instance['InstanceId'],
				'ImageId'			=> $instance['ImageId'],
				'State'				=> $instance['State']['Name'],
				'InstanceType'		=> $instance['InstanceType'],
				'KeyName'			=> $instance['KeyName'],
				'LaunchTime'		=> time(),
				'TerminateTime'		=> 0
			);
		}
		return $data;
	}
	function dba_update_status_instance($result)
	{
		$data = Array();
		//********** Saving Logs *********//
		//$this->csalt_log->add(4, "AWS", "dba_update_status_instance", "dba_update_status_instance init");
		//********************************//
		if(count($result['Reservations']) ==0)
			return array();
		foreach($result['Reservations'] as $instances)
		{
			$instance = $instances['Instances'][0];
			if( isset($instance['State']['Name']) && @is_string($instance['State']['Name']) )
				$data[ $instance['InstanceId'] ]['State'] = $instance['State']['Name'];
				
			if( isset($instance['PrivateDnsName']) && @is_string($instance['PrivateDnsName']) )
				$data[ $instance['InstanceId'] ]['PrivateDnsName'] = $instance['PrivateDnsName'];
				
			if( isset($instance['PublicDnsName']) && @is_string($instance['PublicDnsName']) )
				$data[ $instance['InstanceId'] ]['PublicDnsName'] = $instance['PublicDnsName'];
				
			if( isset($instance['PrivateIpAddress']) && @is_string($instance['PrivateIpAddress']) )
				$data[ $instance['InstanceId'] ]['PrivateIpAddress'] = $instance['PrivateIpAddress'];
				
			if( isset($instance['PublicIpAddress']) && @is_string($instance['PublicIpAddress']) )
				$data[ $instance['InstanceId'] ]['PublicIpAddress'] = $instance['PublicIpAddress'];
		}
		//foreach($result['Reservations'][0]['Instances'] as $instance)
		//{
			/*
			$data[ $instance['InstanceId'] ] = array(
				'State'								=> $instance['State']['Name'],
				'PrivateDnsName'			=> $instance['PrivateDnsName'],
				'PublicDnsName'				=> $instance['PublicDnsName'],
				'PrivateIpAddress'		=> $instance['PrivateIpAddress'],
				'PublicIpAddress'			=> $instance['PublicIpAddress'],
			);
			*/
		//}
		//********** Saving Logs *********//
		//$this->csalt_log->add(3, "AWS", "dba_update_status_instance", "dba_update_status_instance success");
		//********************************//
		
		return $data;
	}
	function dba_terminate_instance($result)
	{
		//********** Saving Logs *********//
		//$this->csalt_log->add(4, "AWS", "dba_terminate_instance", "dba_terminate_instance init");
		//********************************//
		foreach($result['TerminatingInstances'] as $instance)
		{
			if($instance['CurrentState']['Name'] == 'shutting-down' || $instance['CurrentState']['Name'] == 'terminated')
			{
				$data[ $instance['InstanceId'] ] = array(
					'State'							=> $instance['CurrentState']['Name'],
					'TerminateTime'			=> time()
				);
			}
			else
			{
				$data[ $instance['InstanceId'] ] = array(
					'State'							=> "Error! terminated failure",
					'TerminateTime'			=> 0
				);
			}
		}
		//********** Saving Logs *********//
		//$this->csalt_log->add(3, "AWS", "dba_terminate_instance", "dba_terminate_instance success");
		//********************************//
		return $data;
	}
}
//$aws = new CsaltAWS_EC2_API();
//$result = $aws->request('requestSpotInstances', 'q_run_spot_instance_peat');
//var_dump($result);


?>
