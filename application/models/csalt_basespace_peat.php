<?php
/**
 *  @file csalt_basespace_peat.php
 *  @brief csalt_basespace_peat.php
 *  @author JHH Corp.
 */
 
/**
 * @struct Csalt_baseSpace_peat
 * 
 * @brief Csalt_baseSpace_peat Model
 *  
 * @tparam
 *  
 */
class Csalt_baseSpace_peat extends CI_Model
{
	function __construct()
  {
      parent::__construct();
  }

	/* file size, price, id */
	public $peat_product		= array(
		"peat"			=> array(100, 10, "2c92c0f93ff561dc01400ff71c5c720e"),
		"peat_large"	=> array(300, 20, "2c92c0f93ff561dc01400ff8cdf677d9")
	);
	
//	/**
//	 * @brief peat_trigger
//	 * @param $app_name 
//	 * @param $BS_appsessionuri 
//	 * @param $BS_authorization_code 
//	 * Csalt_baseSpace_peat::peat_trigger($app_name, $BS_appsessionuri,$BS_authorization_code)
//	 */
//	function peat_trigger($app_name, $BS_appsessionuri,$BS_authorization_code)
//	{
//		//********** Saving Logs *********//
//		$this->csalt_log->add(4, "peat", "peat_trigger", "trigger init");
//		//********************************//
//		
//		// include model
//		$this->load->model('Csalt_baseSpace_api', 'BS');
//		$this->load->database();
//		
//		//########## app code, app session, init BS api
//		// $_SESSION['BS_authorization_code']
//		// $_SESSION['BS_appsessionuri']
//		// $_SESSION['BS_access_token']
//		// $_SESSION['app_name']
//		$this->BS->session_check_init($app_name, $BS_appsessionuri, $BS_authorization_code);
//		
//		//########## app user
//		$this->BS->get_user_info();
//		$_SESSION['BS_Id'] = $BS_Id = $this->BS->user_data['Id'];
//		$_SESSION['BS_Access_key'] = $BS_Access_key = md5( $this->BS->user_data['Id'] . $this->BS->user_data['Name'] . $this->BS->user_data['Email'] );
//		
//		//########## save user info
//		$this->BS->save_user_info($BS_Id, $BS_Access_key);
//		
//		//output for html
//		$this->BS->get_projects_from_user();
//		//$this->BS->get_runs_from_user();
//
//		
//		$data = array('user_projects'=>$this->BS->user_projects, 'appsession'=>$_SESSION['BS_appsessionuri']);
//		
//		//********** Saving Logs *********//
//		$this->csalt_log->add(3, "peat", "peat_trigger", "trigger success");
//		//********************************//
//		
//		return $data;
//	}
	
	
	/**
	 * @brief peat_buy
	 * Csalt_baseSpace_peat::peat_buy()
	 */
	function peat_buy()
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "peat", "peat_buy", "request buy");
		//********************************//
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		$this->load->model('Csalt_baseSpace_api', 'BS');
		
		// set_access_token
		// get_user_info
		// get_sample_info
		// get_files_from_sample
		$this->BS->run_peat_check_init();
		
		//檢查檔案正確性
		// check $_POST['files_select']
		$paired_files = $this->BS->run_peat_check_files();

		//取得所有檔案大小
		$AllFileSize = 0;
		foreach($paired_files as $file)
		{
			$AllFileSize += $file['R1']['Size'];
			$AllFileSize += $file['R2']['Size'];
		}
		if($AllFileSize == 0)
			$this->error_handle($pages,'File size ERROR');
		if($AllFileSize > 300 *1024 *1024 *1024)
			$this->error_handle($pages,'Sorry, your files are too large, > 300G');

		//檢查資料庫，使用者資訊以及是否已經app session 跑過
		//1. is user in db
		//2. is app session already run
		$DB_user = $this->BS->run_peat_check_db();
		
		$_SESSION['request_run_info'] = Array(
			'sample_name'						=> $this->BS->sample_data['Name']
			,'sample_href'					=> $this->BS->sample_data['Href']
			,'paired_files'					=> $paired_files
			,'AllFileSize'					=> $AllFileSize
			,'DB_user'							=> $DB_user
		);
		//
		//
		//
		//$AllFileSize = 101 *1024 *1024 *1024;
		
		if($AllFileSize <= 0.001 *1024 *1024 *1024)
		{
			//For Free
			$data = $this->peat_run();
			return $data;
		}
		else if($AllFileSize <= 100 *1024 *1024 *1024)
		{
			// purchase
			$peat_product_name = "peat";
			$product_id = $this->peat_product[$peat_product_name][2];
			$_SESSION['purchase_id'] = $this->BS->request_buy($product_id, $_SESSION['app_name']);
		}
		else if($AllFileSize <= 300 *1024 *1024 *1024)
		{
			// purchase
			$peat_product_name = "peat_large";
			$product_id = $this->peat_product[$peat_product_name][2];
			$_SESSION['purchase_id'] = $this->BS->request_buy($product_id, $_SESSION['app_name']);
		}
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "peat", "peat_buy", "request buy success");
		//********************************//
		
		$HrefPurchaseDialog = $this->BS->purchase_response['HrefPurchaseDialog'];
		//echo $HrefPurchaseDialog;
		header("Location: $HrefPurchaseDialog");
		
		return false;
	}
	function peat_purchase_check($BS_purchaseid)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "peat", "peat_purchase_check", "check purchase");
		//********************************//
		
		if( !isset($_SESSION['request_run_info']) )
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "peat", "peat_purchase_check", "Error! No request_run, or timeout. <br> Or you may have run this session, or have not trigger this session.");
			//********************************//
			die("Error! No request_run, or timeout. <br> Or you may have run this session, or have not trigger this session.");
		}
			
		
		if($BS_purchaseid == 'Free')
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(3, "peat", "peat_purchase_check", "Free version no need check");
			//********************************//
			
			if($_SESSION['request_run_info']['AllFileSize'] > 5 *1024 *1024 *1024)
			{
				$this->csalt_log->add(1, "peat", "peat_purchase_check", "Free version, but file size more than free!");
				die("purchase id error, not Free!!!");
			}
				
			$data = $this->peat_run();
		}
		else
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(3, "peat", "peat_purchase_check", "Check non-free purchase");
			//********************************//
			
			$this->load->model('Csalt_baseSpace_api', 'BS');
			$this->BS->session_check_init($_SESSION['app_name'], NULL, NULL, $_SESSION['bs_type']);
			$check = $this->BS->request_buy_update($BS_purchaseid);
			
			
			if($check['Status'] == "COMPLETED")
			{
				$data = $this->peat_run();
			}
			else
			{
				die("Sorry! this product have not purchased");
			}
			
			//not free	
		}
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "peat", "peat_purchase_check", "check purchase success");
		//********************************//
		return $data;
	}
	function peat_run()
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "peat", "peat_run", "peat run init");
		//********************************//
		
		$this->load->model('Csalt_baseSpace_api', 'BS');
		
		//########## app code, app session, init BS api
		// $_SESSION['BS_authorization_code']
		// $_SESSION['BS_appsessionuri']
		// $_SESSION['BS_access_token']
		// $_SESSION['app_name']
		$this->BS->session_check_init($_SESSION['app_name'], NULL, NULL, $_SESSION['bs_type']);
		
		if(!isset($_SESSION['request_run_info']))
		{
			die("request undefine ,<br> you may have run this session, or have not trigger this session.");
		}
		$request_run = $_SESSION['request_run_info'];
		$request_sample_name = $request_run['sample_name'];
		$request_sample_href = $request_run['sample_href'];
		$request_paired_files = $request_run['paired_files'];
		$request_AllFileSize = $request_run['AllFileSize'];
		$request_DB_user = $request_run['DB_user'];
		
		
		$project_name = "PEAT-proj-".$request_sample_name;
		$sample_name = $request_sample_name;
		$appresult_name = "PEAT-appr-".$request_sample_name."-".date('Y-m-d-H-i-s', time());
		
		// 新建 project
		$this->BS->create_project($project_name);
		
		// 新建 appresult
		$this->BS->create_appresult($appresult_name, "PEAT result", $request_sample_href);
		
		//開機器並且執行
		$this->load->model('csalt_aws_ec2_api','AWS');
		$instance_ids = $this->AWS->instance_launch('q_run_instance_peat');
		$instance_id = $instance_ids[0];
		
		//$instance_id = 0;
		
		//lose some para
		$this->BS->sample_data['Href'] = $request_sample_href;
		$this->BS->user_data['Href'] = $request_DB_user['Href'];
		$run_id = $this->BS->run_peat_save_run($instance_id ,$project_name, $sample_name, $appresult_name, $request_AllFileSize);
		
		//HAVE BUGS
		$this->BS->run_peat_save_files($run_id, $request_paired_files);

		//wait for instance
		$this->bg_curl("http://localhost/basespace/query_task_peat/waiting_for_instance_running/".$_SESSION['BS_access_token']);
		
		//Email to user
		$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
		$this->BS_PEAT->send_mail($request_DB_user, $_SESSION['app_name'], "start_run", NULL);

		//Show Html
		$data = array(
			'DB_user'						=>	$request_DB_user,
			'Files'							=>	$request_paired_files,
			'sample_name'				=>	$sample_name,
			'project_name'			=>	$project_name,
			'appresult_name'		=>	$appresult_name,
			'AllFileSize'				=>	$request_AllFileSize
		);
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "peat", "peat_run", "peat run success");
		//********************************//
		
		unset($_SESSION['request_run_info']);

		return $data;
	}
	function run_command($run, $instance_id, $access_token)
	{
		$query = $this->db->get_where( 'peat_instance', array("InstanceId"=>$instance_id ) );	
					
		$instance = $query->row_array();
		system("echo 'ok $access_token \n' >> test");
		
		$this->load->model('Csalt_ssh_model', 'SSH');
		$this->SSH->ssh_connect_by_key($instance['PublicDnsName'],"ubuntu","/peat/www/csalt/application/models/basic-west.pem");
		
		$cmd = "rm -rf /c-salt/pokemon_public";
		echo $this->SSH->ssh_command($cmd);
		
		$cmd = "cd /c-salt;git clone http://gitlab.jhhlab.tw/pokemon_public.git";
		echo $this->SSH->ssh_command($cmd);
		
		$cmd = "g++ -std=c++11 /c-salt/pokemon_public/PeatCloud/PeatFree.cpp -lboost_thread -lpthread -lboost_system -Ofast -lz -lcurl -lboost_iostreams -o /c-salt/pokemon_public/PeatCloud/PeatCloud";
		echo $this->SSH->ssh_command($cmd);
		
		if(strstr($run['task'], 'hoth'))
			$platform = "hoth.muggle.tw";
		else
			$platform = "peat.jhhlab.tw";

		$cmd = "nohup /c-salt/pokemon_public/PeatCloud/PeatCloud http://$platform/basespace/query_task_peat/file_list/$access_token 0.3/0.6/0.4/30 > log 2>&1 &";
		echo $this->SSH->ssh_command($cmd);
		
		//********** Saving Logs *********//
		$this->csalt_log->add(2, "basespace", "query_task_peat", "request waiting_for_instance_running: running peat task ");
		//********************************//
		
		// return new status
		return 9;
	}
	
	
	
	function peat_status_table($status)
	{
		$table = array();
		$table[0]		=	array('Complete', 'Finish');
		$table[1]		=	array('Error', 'Unknow Error!');
		$table[2]		=	array('Running', '');
		$table[3]		=	array('Running', '');
		$table[4]		=	array('Running', '');
		$table[5]		=	array('Running', '');
		$table[6]		=	array('Running', '');
		$table[7]		=	array('Running', '');
		$table[8]		=	array('Running', '');
		$table[9]		=	array('Running', 'Running PEAT');
		$table[10]	=	array('Running', 'Pending instance');
		
		if($status > 10)
			return array('Error!','Error status nember.');
		
		return $table[$status];
	}
	// !Mail
	function send_mail($DB_user = NULL, $app_name = NULL, $action = NULL, $data = NULL)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "peat", "send_mail", "send_mail init");
		//********************************//
		
		
		//sender info
		$mail_sender = Array(
			"name"						=> "C-Salt service"
			,"email"					=> "andy@mail.jhhlab.tw"
		);
		//receive info example
		//$mail_receiver = $DB_user
		
		$peat_data = Array(
			"name"						=> "Andy"
			,"access_key"			=> "cc4b1b9cb4f3494b9ba1d8de16effbae"
		);
		
		// peat free require: name, access_key
		// peat require: name, access_key
		
		
		$mail_content = Array(
			"peat_free"					=> Array(
				"start_run"					=> Array(
						"title"							=> "Thank you for running PEAT !!"
						,"message"					=> "Hello {$DB_user['Name']} ~! \nThank you for running PEAT \nYou can browse your jobs from this link: http://peat.jhhlab.tw/basespace/browse?access_key={$DB_user['access_key']}\nIf you have any questions, please send a reply to obigbando@gmail.com."
				)
				,"finish_run"				=> Array(
						"title"							=> "Your Job have been finished !!"
						,"message"					=> "Hello {$DB_user['Name']} ~! \nThank you for running PEAT \nYour Job of PEAT running is finished, You can browse the result on basespace: https://basespace.illumina.com/project/{$data['project_id']}\nIf you have any questions, please send a reply to obigbando@gmail.com."
				)
				,"failure"					=> Array(
						"title"							=> "Sorry! Your PEAT run failed."
						,"message"					=> "Hello {$DB_user['Name']} ~! \nYour Job failed."
				)	
			)
			,"peat"					=> Array(
				"start_run"					=> Array(
						"title"							=> "Thank you for running PEAT !!"
						,"message"					=> "Hello {$DB_user['Name']} ~! \nThank you for running PEAT \nYou can browse your jobs from this link: http://peat.jhhlab.tw/basespace/browse?access_key={$DB_user['access_key']}\nIf you have any questions, please send a reply to obigbando@gmail.com."
				)
				,"finish_run"				=> Array(
						"title"							=> "Your Job have been finished !!"
						,"message"					=> "Hello {$DB_user['Name']} ~! \nThank you for running PEAT \nYour Job of PEAT running is finished, You can browse the result on basespace: https://basespace.illumina.com/project/{$data['project_id']}\nIf you have any questions, please send a reply to obigbando@gmail.com."
				)
				,"failure"					=> Array(
						"title"							=> "Sorry! Your PEAT run failed."
						,"message"					=> "Hello {$DB_user['Name']} ~! \nYour Job failed."
				)	
			)
			,"peat_hoth"					=> Array(
				"start_run"					=> Array(
						"title"							=> "Thank you for running PEAT !!"
						,"message"					=> "Hello {$DB_user['Name']} ~! \nThank you for running PEAT \nYou can browse your jobs from this link: http://peat.jhhlab.tw/basespace/browse?access_key={$DB_user['access_key']}\nIf you have any questions, please send a reply to obigbando@gmail.com."
				)
				,"finish_run"				=> Array(
						"title"							=> "Your Job have been finished !!"
						,"message"					=> "Hello {$DB_user['Name']} ~! \nThank you for running PEAT \nYour Job of PEAT running is finished, You can browse the result on basespace: https://basespace.illumina.com/project/{$data['project_id']}\nIf you have any questions, please send a reply to obigbando@gmail.com."
				)
				,"failure"					=> Array(
						"title"							=> "Sorry! Your PEAT run failed."
						,"message"					=> "Hello {$DB_user['Name']} ~! \nYour Job failed."
				)	
			)
		);
		
		
		//send
		$this->load->library('email');
		/*
		$config['protocol'] = 'smtp';
		$config['charset'] = 'iso-8859-1';
		$config['smtp_host'] = "ssl://smtp.googlemail.com";
		$config['smtp_port'] = 465;
		$config['smtp_crypto'] = 'ssl';
		$config['smtp_user'] = "poi5305@gmail.com";
		$config['smtp_pass'] = "tfxjzqkmdfnavefv";
		$config['wordwrap'] = TRUE;
		*/
		$config['protocol'] = 'smtp';
		$config['charset'] = 'utf-8';
		$config['smtp_host'] = "mail.jhhlab.tw";
		$config['smtp_port'] = 25;
		$config['smtp_user'] = "andy";
		$config['smtp_pass'] = "qsefth";
		$config['smtp_timeout'] = 10;
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);
		
		$this->email->from($mail_sender['email'], $mail_sender['name']);
		$this->email->to($DB_user['Email']);
		$this->email->cc("andy@mail.jhhlab.tw"); 
		
		$this->email->subject($mail_content[$app_name][$action]['title']);
		$this->email->message($mail_content[$app_name][$action]['message']); 
		
		$this->email->send();
		
		//echo $this->email->print_debugger();
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "peat", "send_mail", "send_mail success, {$DB_user['Email']}, {$mail_content[$app_name][$action]['title']}, {$mail_content[$app_name][$action]['message']}");
		//********************************//
		
	}
	
	function send_mail_manual($title, $message, $email="poi5305@gmail.com", $cc="obigbando@gmail.com")
	{
		$this->load->library('email');
		
		$config['protocol'] = 'smtp';
		$config['charset'] = 'utf-8';
		$config['smtp_host'] = "mail.jhhlab.tw";
		$config['smtp_port'] = 25;
		$config['smtp_user'] = "andy";
		$config['smtp_pass'] = "qsefth";
		$config['smtp_timeout'] = 10;
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);
		
		$this->email->from("andy@mail.jhhlab.tw", "C-Salt Admin");
		$this->email->to($email);
		$this->email->cc($cc); 
		
		$this->email->subject($title);
		$this->email->message($message); 
		
		$this->email->send();
	}
	

	private function bg_curl($url)
	{
		$ch1 = curl_init();
		curl_setopt($ch1, CURLOPT_URL, $url);
		curl_setopt($ch1, CURLOPT_HEADER, 0);
		$mh = curl_multi_init();
		curl_multi_add_handle($mh,$ch1);
		$active = null;
		$mrc = curl_multi_exec($mh, $active);
	}

}

?>