<?php

include_once 'aws-sdk-for-php/sdk.class.php';


class Csalt_eucalyptus_api extends CI_Model
{
	function __construct()
  {
      parent::__construct();
      $this->init();
  }
	//AWS ID 496508083411
	var $aws_key									=	"EQEHQN8OUYTVLQY1XJ96C";
	var $aws_secret								=	"wgAiEfLe7o1uPA5LEywHjTuqdI1FUS5IrbAjiApc";
	var $aws_region								=	"cluster01";
	var $response_addr = NULL;
	var $client										=	NULL;
	var $ec2									= NULL;
	var $q_image_list_self				= array
	(
		'Owners'					=> array('self')
	);
	
	var $q_run_instance_peat			= array
	(
		'ImageId'					=> 'emi-3313318A',
		'MinCount'				=> 1,
		'MaxCount'				=> 1,
		'InstanceType'		=> 'm1.small',
		'KeyName'					=> 'default',
		'UserData'				=> 'PEAT instance',
		'Placement'				=> array('AvailabilityZone' => 'cluster01'),
		'SecurityGroups'	=> array('default')
	);
	
	var $q_run_spot_instance_peat		= array
	(
		'SpotPrice'								=> '0.05',
		'InstanceCount'						=> 1,
		'LaunchSpecification'			=> array(
			'ImageId'									=>	'emi-3313318A',
			'KeyName'									=>	'default',
			'UserData'								=>	'PEAT instance',
			'InstanceType'						=>	'm1.small',
			'Placement'								=>	array('AvailabilityZone' => 'cluster01'),
			'SecurityGroups'					=>	array('default')
		)
	);
	
	function init()
	{
//		$this->client = Aws\Ec2\Ec2Client::factory(array(
//			'key'		=> $this->aws_key,
//			'secret' => $this->aws_secret,
//			'region' => $this->aws_region,
//		//	'scheme' => 'http',
//			'signature' => v2,
//			'base_url' => 'http://140.113.15.91:8773/services/Eucalyptus/'
//		));

		$this->ec2 = new AmazonEC2(array(
					'key' => 'EQEHQN8OUYTVLQY1XJ96C',
					'secret' => 'wgAiEfLe7o1uPA5LEywHjTuqdI1FUS5IrbAjiApc',
					'region' => $this->aws_region
					));
		$this->ec2->set_hostname("140.113.15.91", 8773);
		$this->ec2->set_resource_prefix("/services/Eucalyptus");
		$this->ec2->hostname .= $this->ec2->resource_prefix;
		@$this->ec2->disable_ssl();
		$response = $this->ec2->describe_images();

//		if ($response->isOK()) {
//			// `body` is a SimpleXMLElement
//			foreach ($response->body->imagesSet->item as $item) {
//				$image_id = (string) $item->imageId;
//				print_r($image_id . "\n");
//			}
//		} else {
//			// error--dump the whole request
//			print_r($response);
//		}
//		exit();
#		$this->client->set_hostname("140.113.15.91", 8773);
	}
	//################ Request ################
	function request($cmd, $info="")
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "AWS", "request", "request init, $cmd");
		//********************************//
		
		if( !is_array($info) )
		{
			if($info != "")
				$info = $this->$info;
			else
				$info = array();
		}
		$result = $this->client->$cmd($info);
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "AWS", "request", "request success, $cmd");
		//********************************//
		
		return $result->toArray();
	}
	
	
	//################ create query db data array ################
	
	function dba_run_instance($result)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "AWS", "dba_run_instance", "dba_run_instance init");
		//********************************//
		
		foreach($result['Instances'] as $instance)
		{
			$t = (array)$instance['instanceState'];
			$data[ $instance['instanceId'] ] = array(
				'ReservationId'		=> $result['ReservationId'], 
				'InstanceId'			=> $instance['instanceId'],
				'ImageId'					=> $instance['imageId'],
				'State'						=> $t['name'],
				'InstanceType'		=> $instance['instanceType'],
				'KeyName'					=> 'default',
				'LaunchTime'			=> time(),
				'TerminateTime'		=> 0
			);
		}
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "AWS", "dba_run_instance", "dba_run_instance success");
		//********************************//
		return $data;
	}
	function dba_update_status_instance($result)
	{
		$data = Array();
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "AWS", "dba_update_status_instance", "dba_update_status_instance init");
		//********************************//
		if(count($result['Reservations']) ==0)
			return array();
		foreach($result['Reservations'] as $instances)
		{
			$instance = $instances['Instances'][0];
			if( isset($instance['State']['Name']) && @is_string($instance['State']['Name']) )
				$data[ $instance['InstanceId'] ]['State'] = $instance['State']['Name'];
				
			if( isset($instance['PrivateDnsName']) && @is_string($instance['PrivateDnsName']) )
				$data[ $instance['InstanceId'] ]['PrivateDnsName'] = $instance['PrivateDnsName'];
				
			if( isset($instance['PublicDnsName']) && @is_string($instance['PublicDnsName']) )
				$data[ $instance['InstanceId'] ]['PublicDnsName'] = $instance['PublicDnsName'];
				
			if( isset($instance['PrivateIpAddress']) && @is_string($instance['PrivateIpAddress']) )
				$data[ $instance['InstanceId'] ]['PrivateIpAddress'] = $instance['PrivateIpAddress'];
				
			if( isset($instance['PublicIpAddress']) && @is_string($instance['PublicIpAddress']) )
				$data[ $instance['InstanceId'] ]['PublicIpAddress'] = $instance['PublicIpAddress'];
		}
		//foreach($result['Reservations'][0]['Instances'] as $instance)
		//{
			/*
			$data[ $instance['InstanceId'] ] = array(
				'State'								=> $instance['State']['Name'],
				'PrivateDnsName'			=> $instance['PrivateDnsName'],
				'PublicDnsName'				=> $instance['PublicDnsName'],
				'PrivateIpAddress'		=> $instance['PrivateIpAddress'],
				'PublicIpAddress'			=> $instance['PublicIpAddress'],
			);
			*/
		//}
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "AWS", "dba_update_status_instance", "dba_update_status_instance success");
		//********************************//
		
		return $data;
	}
	function dba_terminate_instance($result)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "AWS", "dba_terminate_instance", "dba_terminate_instance init");
		//********************************//
		foreach($result['TerminatingInstances'] as $instance)
		{
			if($instance['CurrentState']['Name'] == 'shutting-down' || $instance['CurrentState']['Name'] == 'terminated')
			{
				$data[ $instance['InstanceId'] ] = array(
					'State'							=> $instance['CurrentState']['Name'],
					'TerminateTime'			=> time()
				);
			}
			else
			{
				$data[ $instance['InstanceId'] ] = array(
					'State'							=> "Error! terminated failure",
					'TerminateTime'			=> 0
				);
			}
		}
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "AWS", "dba_terminate_instance", "dba_terminate_instance success");
		//********************************//
		return $data;
	}
	
	//################ do something ################
	function instance_launch($info)
	{
		if( !is_array($info) )
                {
                        if($info != "")
                                $info = $this->$info;
                        else
                                $info = array();
                }
//		print_r($info);
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "AWS", "instance_launch", "instance_launch init");
		//********************************//
		$this->load->database();
		//$result = $this->AWS->request('runInstances', $info);
		
		$img_id = $info["ImageId"];
		$inst_type = $info["InstanceType"];
		$KeyName = $info["KeyName"];
		$ZONE = $info['Placement']['AvailabilityZone'];
//		print_r( $this->ec2);
		$ALL_Address = array(	'103'=>'140.113.15.103', 
					'104'=>'140.113.15.104')
//					'120'=>'140.113.15.120')
;
		//var $response_addr = NULL;

//		foreach( $ALL_Address as $name => $IP)
		{
//			print ("testing $IP...");
//			$response_addr = $this->ec2->release_address(array('PublicIp' => $IP));
//			var_dump($response_addr->isOK());
//			if ( !$response_addr->isOK() )
//			{
//				continue;
//			}
//			$response_addr = $this->ec2->allocate_address();
			//if ( $response_addr->isOK() )
			//{
			//	break;
			//}
		}
//		$response_addr = $this->ec2->allocate_address();
//		if (!$response_addr->isOK())
//		{
//			print ("Getting IP failed, no avaialbe address resource! please try again later!!");
//			var_dump($response_addr->isOK());
//			exit;
//		}
//		var_dump($response_addr->isOK());
		$result = (array)$this->ec2->run_instances($img_id, 1, 1, array('InstanceType' => $inst_type,
										'Placement' => array('AvailabilityZone'=>$ZONE)
										));
		$result = (array)$result["body"];

//		print_r($result);
		if ( @$result["Errors"] )
		{
			print ("<B>Getting IP failed, no avaialbe address or resource! please try again later!!<p></B>");
			print_r($result);
			echo '<meta http-equiv="refresh" content="3;url=https://peat.jhhlab.tw/index.php/basespace_test">';
			exit;

		}

		$result["ReservationId"] = $result["reservationId"];
		$result["Instances"] = (array)$result["instancesSet"];

		$result["Instances"]["item"] = (array)$result["Instances"]["item"];
		$dba = $this->AWS->dba_run_instance( $result );
		$return = array();
		
		foreach($dba as $instance_id => $data)
		{
			$return[] = $instance_id;
			$sql = $this->db->insert_string('peat_instance', $data);
			$this->db->query($sql);
		}
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "AWS", "instance_launch", "instance_launch success");
		//********************************//
		return $return;
	}
	function instance_update_status($array=array())
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "AWS", "instance_update_status", "instance_update_status init");
		//********************************//
		$this->load->database();
		//$result = $this->AWS->request('describeInstances', array('InstanceIds' => $array ) );
		$result = $this->ec2->describe_instances(array('InstanceIds' => $array ) );
		$dba = $this->AWS->dba_update_status_instance($result);
print_r($array);
print_r($dba);
print_r($result);		
		if(count($dba) == 0)
			$this->csalt_log->add(2, "AWS", "instance_update_status", "instance_update_status, aws return no data");
		
		$return = array();
		
		foreach($dba as $instance_id => $data)
		{
			if(!isset($data['State']))
				continue;
			$return[$instance_id] = $data['State'];
			$sql = $this->db->update_string('peat_instance', $data, "InstanceId = '$instance_id'");
			$this->db->query($sql);
		}
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "AWS", "instance_update_status", "instance_update_status success");
		//********************************//
		return $return;
	}
	function instance_terminate($array)
	{
		return;
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "AWS", "instance_terminate", "instance_terminate init");
		//********************************//
		$this->load->database();
		
		try
		{
//			$result = $this->request('terminateInstances', array('InstanceIds' => $array ) );
			$result = $this->ec2->terminate_instances(array('InstanceIds' => $array ));
		}
		catch(Exception $e)
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(4, "AWS", "instance_terminate", "terminate failure, No such instance id".json_encode($array));
			//********************************//
			return true;
		}
		
		
		$dba = $this->dba_terminate_instance($result);
		
		$error = false;
		
		foreach($dba as $instance_id => $data)
		{
			if($data['TerminateTime'] == 0)
				$error=true;
			$sql = $this->db->update_string('peat_instance', $data, "InstanceId = '$instance_id'");
			$this->db->query($sql);
		}
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "AWS", "instance_terminate", "instance_terminate success");
		//********************************//
		
		return !$error;
	}
	
	// describeSpotInstanceRequests
	// runInstances
	// requestSpotInstances
	// describeImages
	// describeInstances
	// terminateInstances
}

//$aws = new CsaltAWS_EC2_API();
//$result = $aws->request('requestSpotInstances', 'q_run_spot_instance_peat');
//var_dump($result);


?>
