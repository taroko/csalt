<?php
require 'vendor/autoload.php';

class Csalt_log extends CI_Model
{
	function __construct()
  {
      parent::__construct();
  }

	var $log_status_filter = 3;
	var $session_time = NULL;

  function add($status = 4, $task="unknow", $name="unknow", $log_mini="unknow", $log="unknow")
  {
  	if(! $this->status_filter($status) )
  		return false;
		
		if(session_id() == '')
  		session_start();
		
	  $this->load->database();
	  
	  $data = Array(
	  	"session_id"		=> session_id(),
	  	"task"					=> $task,
	  	"status"				=> $status,
	  	"name"					=> $name,
	  	"log_mini"			=> $log_mini,
	  	//"log"					=> $log,
	  	"time"					=> time()
	  );
	  $sql = $this->db->insert_string('logs', $data);
		$this->db->query($sql);
	  return true;
  }
  
  function status_filter($status)
  {
  	// 0 嚴重錯誤
  	// 1 普通錯誤
  	// 2 重要訊息
  	// 3 訊息記錄
  	// 4 垃圾訊息
	  
	  if($status > $this->log_status_filter)
	  {
		  return false;
	  }
	  return true;
	  
  }

}
?>