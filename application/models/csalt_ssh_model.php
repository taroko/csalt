<?php
require 'vendor/autoload.php';

set_include_path(dirname(__FILE__).'/phpseclib');
require_once ('phpseclib/Net/SSH2.php');
require_once('phpseclib/Crypt/RSA.php');



class Csalt_ssh_model extends CI_Model
{
	
	//need model apt-get install libssh2-1-dev libssh2-php
	var $host = NULL;
	var $port = NULL;
	var $user = NULL;
	var $passwd = NULL;
	//var $public_key = NULL;
	var $private_key = NULL;
	var $connection = NULL;
	var $current_result = NULL;
	var $retry = 10;
	var $pem_location  = "/Users/andy/Dropbox/program/csalt/application/models/basic-west.pem";
	
	function __construct()
	{
		parent::__construct();
	}
	
	function ssh_connect_by_passwd($host, $user, $passwd)
	{
		$this->host = $host;
		$this->user = $user;
		$this->passwd = $passwd;
		
		$this->connection = new Net_SSH2($host);
		
		$retry = 0;
		while(!$this->connection->login($user, $passwd))
		{
			$retry++;
			$this->utility->log("msg", __CLASS__.".".__FUNCTION__, "", "SSH connect Failed", "Retry...$retry times");
			if($retry == $this->retry)
			{
				$this->utility->log("die", __CLASS__.".".__FUNCTION__, "", "SSH connect Failed", "Login Failed");
				return false;
			}
			sleep(10);
		}
		return true;
		//if (!$this->connection->login($user, $passwd))
		//{
		//	$this->utility->log("die", __CLASS__.".".__FUNCTION__, "", "SSH connect", "SSH connect Failed");
		//	exit('Login Failed');
		//}
		
	}
	function ssh_connect_by_key($host, $user, $private_key)
	{
		$this->user = $user;
		$this->private_key = $private_key;
		
		$key = new Crypt_RSA();
		$key->loadKey(file_get_contents($private_key));
		$this->connection = new Net_SSH2($host);
		
		$retry = 0;
		while(!$this->connection->login($user, $key))
		{
			$retry++;
			$this->utility->log("msg", __CLASS__.".".__FUNCTION__, "", "SSH connect Failed", "Retry...$retry times");
			if($retry == $this->retry)
			{
				$this->utility->log("die", __CLASS__.".".__FUNCTION__, "", "SSH connect Failed", "Login Failed");
				return false;
			}
			sleep(10);
		}
		return true;
		//if (!$this->connection->login($user, $key))
		//{
		//  	exit('Login Failed');
		//}
	}
	
	function ssh_command($cmd="")
	{
		$this->utility->log("msg", __CLASS__.".".__FUNCTION__, "", "SSH command", "{$this->host}: $cmd");
		return $this->connection->exec($cmd);
	}
	
	function ssh_task()
	{
		
	}
	
}


?>