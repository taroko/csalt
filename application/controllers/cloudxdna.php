<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *  @file basespace.php
 *  @brief controllers basespace
 */

ini_set('display_errors', 0);

/**
 * @struct 
 * 
 * @brief 
 *  
 * @tparam
 *  
 */
class Cloudxdna extends CI_Controller {

	public function index()
	{
		//$pages['sub_menu'] = $this->load->view("basespace/view_basespace_sub_menu",'', true );
		//$pages['main_page'] = $this->load->view("basespace/view_bs_{$app_name}_trigger", $view, true );
		$this->utility->createHtmlView(array());
	}
}

/* End of file basespace.php */
