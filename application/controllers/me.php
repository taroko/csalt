<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Me extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $html_template = 'template_lazdays';
	
	

	public function css($page)
	{
		$this->load->view("{$this->html_template}/css/$page");
	}
	public function images($page)
	{
		$page = $this->uri->assoc_to_uri($this->uri->uri_to_assoc());
		if(getimagesize("application/views/{$this->html_template}/images/$page"))
		{
			echo file_get_contents("application/views/{$this->html_template}/images/$page");
		}
		//$this->load->view("{$this->html_template}/images/$page");
	}
	
	public function js($page, $version)
	{
		$this->load->view("js/$page-$version.js");
	} 
	 
	
	 
	 
	public function index()
	{
$r=rand(10000,99999);
echo '{
   "id": "1000006531'.$r.'",
   "name": "\u80d6\u80d6\u7cd6",
   "first_name": "\u80d6\u7cd6",
   "last_name": "\u80d6",
   "link": "https://www.facebook.com/profile.php?id=100000653131720",
   "gender": "male",
   "email": "poi5305\u0040hotmail.com",
   "timezone": 8,
   "locale": "zh_TW",
   "updated_time": "2011-03-18T00:26:19+0000"
}';		
//$this->load->view('me');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
