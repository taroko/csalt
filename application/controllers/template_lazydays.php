<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template_lazydays extends CI_Controller {

	var $html_template = 'template_lazydays';
	
	public function css($page)
	{
		$this->load->view("{$this->html_template}/css/$page");
	}
	public function images($page)
	{
		$page = $this->uri->assoc_to_uri($this->uri->uri_to_assoc());
		if(getimagesize(APPPATH."/views/{$this->html_template}/images/$page"))
		{
			echo file_get_contents(APPPATH."/views/{$this->html_template}/images/$page");
		}
	}
	
	public function js($page, $version)
	{
		$this->load->view("js/$page-$version.js");
	}
	public function index($page)
	{
		$this->load->view("{$this->html_template}/css/$page");
	}
}
