<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cli extends CI_Controller {

	var $default_BS_model = "Csalt_bs_api";
	
	public function index()
	{
		
	}
	public function monitor()
	{
		$this->monitor_instance();
	}
	public function monitor_5min(){}
	public function monitor_10min(){}
	public function monitor_20min()
	{
	}
	public function monitor_30min()
	{
		$this->monitor();
	}
	public function monitor_instance()
	{
		if(!$this->input->is_cli_request())
			return;	
		$this->load->database();
		$sql = "SELECT * FROM `instances` WHERE `State` != 'terminated'";
		$query = $this->db->query($sql);
		if($query->num_rows() == 0)
		{
			$this->utility->log("msg", __CLASS__.".".__FUNCTION__, "Monitor", "No UNterminated instances", "No any not terminated instances, break.");
			return;
		}
		foreach( $query->result_array() as $instance )
		{
			$this->handle_instance($instance);
		}
	}
	
	private function handle_instance(&$instance)
	{
		// update instance status
		$new_state = $this->update_instance($instance);
		switch($instance['State'])
		{
			case "pending":
			case "running": 
				// check instance no to disappear
				$this->check_instance_is_disappear($instance, $new_state);
				// check time out and time up
				$this->check_app_time($instance);
				break;
			case "shutting-down":
				// change to terminate
				// update_instance already done
				break;
			case "terminated":
				// do not thing
				break;
		}
	}
	private function update_instance(&$instance)
	{
		$this->load->database();
		$this->load->model('csalt_aws_ec2_api','AWS');
		try
		{
			$new_state = $this->AWS->instance_update_status( array($instance['InstanceId']) );
			// api 找不到 instance，不一定會丟 exception，所以如果沒有結果，也要設為終止
			if(count($new_state) == 0)
				$new_state[$instance['InstanceId']] = "terminated";
		}
		catch(Exception $e)
		{
			//var_dump($e->getExceptionCode()); //InvalidInstanceID.NotFound
			//var_dump($e->getStatusCode()); //400
			$this->utility->log( "msg", __CLASS__.".".__FUNCTION__, "Monitor", "AWS API Exception: {$instance['InstanceId']}", $e->getExceptionCode() );
			if($e->getExceptionCode() == "InvalidInstanceID.NotFound" || $e->getStatusCode() == 400)
			{
				// instance is already terminated and the record have not exist. Auto change to terminated
			}
			else
			{
				$msg = "Unknown AWS API exception\nInstance id:{$instance['InstanceId']}\n";
				$this->utility->log( "mail", __CLASS__.".".__FUNCTION__, "Monitor", "AWS API Unknown Exception: {$instance['InstanceId']}", $msg );
			}
			$new_state[$instance['InstanceId']] = "terminated";
		}
		if($new_state[$instance['InstanceId']] == "terminated")
			$this->db->update("instances", array("State"=>"terminated"), array("InstanceId"=>$instance['InstanceId']) );
		return $new_state;
	}
	
	private function check_instance_is_disappear(&$instance, &$new_state)
	{
		if($new_state[ $instance['InstanceId'] ] == "terminated" && ($instance['State'] == "pending" || $instance['State'] == "running" ))
		{
			$this->load->database();
			$query = $this->db->get_where('runs', array("InstanceId"=>$instance['InstanceId']));
			if($query->num_rows() == 0)
			{
				$this->utility->log( "msg", __CLASS__.".".__FUNCTION__, "Monitor", "Instance Terminate", "Instance Terminate, But No Run: {$instance['InstanceId']}" );
				return ;
			}
			$run = $query->row_array();
			$this->load->model($this->default_BS_model, 'BS');
			$status = $this->BS->status_table($run['app_name'], $run['status']);
			
			if(	$status[2] != "terminated" )
			{
				$msg = "Instance id:{$instance['InstanceId']}, Old run status: {$run['status']}, Old instance Status:{$instance['State']} ";
				$this->utility->log( "mail", __CLASS__.".".__FUNCTION__, "Monitor", "Instance Unknown Error. Disappear: {$instance['InstanceId']}", $msg );
				
				$sql = $this->db->update_string('runs', array("status"=>"0"), "InstanceId = '{$instance['InstanceId']}'");
				$this->db->query($sql);
			}
		}
	}
	private function check_app_time(&$instance)
	{
		$this->load->database();
		$sql = "SELECT * FROM `runs`, `purchase_product` WHERE `runs`.`InstanceId` = '{$instance['InstanceId']}' AND `runs`.`purchase_id` = `purchase_product`.`purchase_id`";
		$query = $this->db->query($sql);
		if($query->num_rows() == 0)
		{
			$this->utility->log("msg", __CLASS__.".".__FUNCTION__, "Monitor", "No run and product", "Unknown error");
			return;
		}
		
		$run = $query->row_array();
		
		$this->load->model($this->default_BS_model, 'BS');
		$this->load->model('csalt_aws_ec2_api','AWS');
		$this->load->model($this->BS->get_model_name_from_app_name($run['app_name']), 'APP');
		
		$status = $this->BS->status_table($run['app_name'], $run['status']);
		$product = $this->APP->get_product_by_key($run['Id']);
		
		$pending_time = 50*60;
		$running_time = 25*60*60;
		$product_time = $product[2]*24*60*60 + 1*60*60;
		$timeup = false; $timeout = false;
		
		// TimeUp...
		if(time() > $run['DateCreate'] + $product_time)
		{
			$timeup = true;
			$mail_type = "TimeUp";
			$run_status = 4;
			$this->utility->log("msg", __CLASS__.".".__FUNCTION__, "Monitor", "Run Time Up", "{$run['app_name']}, {$run['access_token']}, {$run['InstanceId']}");
		}
		// TimeOut
		if(	(time() > $run['DateCreate'] + $pending_time && $status[0] == 'Pending') ||
			(time() > $run['DateCreate'] + $running_time && $status[0] == 'Running')
		  )
		{
			$timeout = true;
			$mail_type = "TimeOut";
			$run_status = 0;
			$this->utility->log("msg", __CLASS__.".".__FUNCTION__, "Monitor", "Pending(Running) Time Out", "{$run['app_name']}, {$run['access_token']}, {$run['InstanceId']}");
		}
		if($timeup || $timeout)
		{
			// stop instance
			$this->AWS->instance_terminate( array($run['InstanceId']) );
			// email to user
			$query2 = $this->db->get_where('peat_user', array("Href"=>$run['HrefUser']));
			$user = $query2->row_array();
			$browse_url = "http://www.cloudxdna.com/bs/browse?access_key={$user['Access_key']}";
			$mail_msg = $this->APP->get_email_message($mail_type, $user['Name'], $browse_url);
			$this->utility->send_mail($user['Email'], $mail_msg['title'], $mail_msg['content']);
			// update run status
			$this->BS->update_progress($run['access_token'], $run_status); // 寫死，未來要改
			
			if($timeout)
				$this->utility->log("mail", __CLASS__.".".__FUNCTION__, "Monitor", "Pending(Running) Time Out", "{$run['app_name']}, {$run['access_token']}, {$run['InstanceId']}");
		}
	}
	
}

/* End of file Cli.php */
/* Location: ./application/controllers/cli.php */