<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *  @file basespace.php
 *  @brief controllers basespace
 *  @author JHH Corp.
 */

ini_set('display_errors', 0);

/**
 * @struct Basespace
 * 
 * @brief Basesapce 網頁控制入口 \n
 *        URL: http://www.jhhlab.tw/basespace
 *  
 * @tparam
 *  
 */
class Bs extends CI_Controller {

	var $default_BS_model = "Csalt_bs_api";
	var $allow_test = true;
	
	private function trigger($app_name, $bs_type)
	{
		//echo $app_name. " ";
		session_start();
		$this->utility->log("info", __CLASS__.".".__FUNCTION__, $app_name, "New Trigger!", "There is new one want to use this App.");
		
		// include model
		$this->load->model($this->default_BS_model, 'BS');
		$this->load->model($this->BS->get_model_name_from_app_name($app_name), 'APP');
		
		//########## app code, app session, init BS api
		// $_SESSION['config']
		$this->BS->init($app_name, $bs_type, $_GET['appsessionuri'], $_GET['authorization_code']);
		
		//########## get and save user info
		$this->BS->init_user();
		
		$appsession = $this->BS->get_appsessions();
		if(count($appsession["References"]) == 0 )
			$this->utility->log("error", __CLASS__.".".__FUNCTION__, $app_name, "No App Session!", "Please Trigger this app from basespace");
		
		$view = $this->APP->check_appsession($appsession);
		
		// 給 Buy 加速用的 session，就不需要重新呼叫 get session
		$_SESSION["config"]["product"] = $view["product"];
		
		$app_name = str_replace("_hoth", "", $app_name);

		$pages['sub_menu'] = $this->load->view("basespace/view_basespace_sub_menu",'', true );
		$pages['main_page'] = $this->load->view("basespace/view_bs_{$app_name}_trigger", $view, true );
		$this->utility->createHtmlView($pages);

	}
	private function buy()
	{
		session_start();
		$this->utility->log("info", __CLASS__.".".__FUNCTION__, $_SESSION['config']['app_name'], "Going to Buy!", "There is new one want to buy this App.");
			
		// include model
		$this->load->model($this->default_BS_model, 'BS');
		
		//########## app code, app session, init BS api
		// $_SESSION['config']
		$this->BS->init();
		
		$is_run_exist = $this->BS->is_run_exist($_SESSION["user"]["Href"], $_SESSION["config"]["appsessionuri"]);
		if($is_run_exist)
			$this->utility->log("error", __CLASS__.".".__FUNCTION__, $_SESSION['config']['app_name'], "Buy for two times.", "This appsession has been run.");
		
		$product_id = $_SESSION["config"]["product"][4];
		$_SESSION['config']['purchase_info'] = $this->BS->request_purchase($product_id);
		$HrefPurchaseDialog = $this->BS->purchase_response['HrefPurchaseDialog'];
		
		//https://hoth-store.basespace.illumina.com/applications/a793efe6f6204a29acd2f4619fdc549b/purchases
		//echo $HrefPurchaseDialog;
		//print_r($_SESSION);
		header("Location: $HrefPurchaseDialog");
	}
	private function no_buy()
	{
		session_start();
		$this->utility->log("info", __CLASS__.".".__FUNCTION__, $_SESSION['config']['app_name'], "Going to Buy!", "There is new one want to buy this App.");
			
		// include model
		$this->load->model($this->default_BS_model, 'BS');
		
		//########## app code, app session, init BS api
		// $_SESSION['config']
		$this->BS->init();
		
		$is_run_exist = $this->BS->is_run_exist($_SESSION["user"]["Href"], $_SESSION["config"]["appsessionuri"]);
		if($is_run_exist)
			$this->utility->log("error", __CLASS__.".".__FUNCTION__, $_SESSION['config']['app_name'], "Buy for two times.", "This appsession has been run.");
		
		$this->load->database();
		
		// insert to purchase
		$this->purchase_response = $data = Array(
			"Id"							=> "0"
			,"Status"						=> "COMPLETED"
			,"RefundStatus"					=> "NOTREFUNDED"
			,"InvoiceNumber"				=> ""
			,"Amount"						=> 0
			,"AmountOfTax"					=> 0
			,"AmountTotal"					=> 0
			,"RefundSecret"					=> "0"
			,"HrefPurchaseDialog"			=> "0"
		);
		$sql = $this->db->insert_string('purchase', $data);
		$this->db->query($sql);
		$purchase_id = $this->db->insert_id();
		
		// insert to purchase_product
		$data = Array(
			"purchase_id"						=> $purchase_id
			,"task"								=> $_SESSION['config']['app_name']
			,"Id"								=> "aaaaaaaaaaaa"
			,"Name"								=> "FREE TEST"
			,"Quantity"							=> 1
			,"PersistenceStatus"				=> "NOPERSISTENCE"
			,"Price"							=> 0
			,"Tags"								=> "[\"spviewer\"]"
		);
		$sql = $this->db->insert_string('purchase_product', $data);
		$this->db->query($sql);		
		
		$this->BS->config['purchase_info']['db_purchase_id'] = $purchase_id;
		
		$this->run();
		$appsession = $this->BS->get_appsessions();
		//check app session
		
		$view = $this->APP->check_appsession($appsession);
		//create view
		$app_name = str_replace("_hoth", "", $this->BS->config['app_name']);
		$view['user'] = $_SESSION['user'];
		$pages['sub_menu'] = $this->load->view("basespace/view_basespace_sub_menu",'', true );
		$pages['main_page'] = $this->load->view("basespace/view_bs_{$app_name}_purchased", $view, true );
		$this->utility->createHtmlView($pages);
		
		$this->utility->log("msg", __CLASS__.".".__FUNCTION__, $_SESSION['config']['app_name'], "Good News!!", "New One App Run. {$this->BS->config['access_token']}");
	}
	
	private function purchased()
	{
		session_start();
		if(!isset($_SESSION['config']))
			$this->utility->log("die", __CLASS__.".".__FUNCTION__, "Unknow", "Session Not Found", "Session May Expire");
		
		if(!isset($_GET["purchaseid"]))
			$this->utility->log("die", __CLASS__.".".__FUNCTION__, $_SESSION['config']['app_name'], "purchase_id Not Found", "No purchase_id parameter");
		
		$this->utility->log("info", __CLASS__.".".__FUNCTION__, $_SESSION['config']['app_name'], "Good News!", "New One App Purchased");
		
		$purchaseid = $_GET["purchaseid"];
		
		// include model
		$this->load->model($this->default_BS_model, 'BS');
		$this->BS->init();
		$this->load->model($this->BS->get_model_name_from_app_name($this->BS->config['app_name']), 'APP');
		
		if($_SESSION['config']['purchase_info']['bs_purchase_id'] != $purchaseid)
			$this->utility->log("error", __CLASS__.".".__FUNCTION__, $_SESSION['config']['app_name'], "Parameter problem", "Current purchase id is not equal in the past");
		
		$is_run_exist = $this->BS->is_run_exist($_SESSION["user"]["Href"], $_SESSION["config"]["appsessionuri"]);
		if($is_run_exist)
			$this->utility->log("error", __CLASS__.".".__FUNCTION__, $_SESSION['config']['app_name'], "Buy for two times.", "This appsession has been run.");
		
		$purchase_statue = $this->BS->purchase_check($purchaseid);
		if(!$purchase_statue)
			$this->utility->log("error", __CLASS__.".".__FUNCTION__, $_SESSION['config']['app_name'], "Not Purchased!", "This product is not purchased");
		
		// Run
		$this->run();
		
		$appsession = $this->BS->get_appsessions();
		//check app session
		
		$view = $this->APP->check_appsession($appsession);
		//create view
		$app_name = str_replace("_hoth", "", $this->BS->config['app_name']);
		$view['user'] = $_SESSION['user'];
		$pages['sub_menu'] = $this->load->view("basespace/view_basespace_sub_menu",'', true );
		$pages['main_page'] = $this->load->view("basespace/view_bs_{$app_name}_purchased", $view, true );
		$this->utility->createHtmlView($pages);
		
		$this->utility->log("msg", __CLASS__.".".__FUNCTION__, $_SESSION['config']['app_name'], "Good News!!", "New One App Run. {$this->BS->config['access_token']}");
		
	}
	private function run()
	{
		$model_name = $this->BS->get_model_name_from_app_name($this->BS->config['app_name']);
		$this->load->model($model_name, 'APP');
		$this->load->model('csalt_aws_ec2_api','AWS');
		
		$instance_id = 0;
		$instance_ids = $this->AWS->instance_launch('q_run_instance_spviewer');
		$instance_id = $instance_ids[0];
		
		$run_id = $this->BS->save_run($instance_id);
		
		//wait for instance
		$base_url = $this->BS->base_url[$this->BS->config['bs_type']];
		$this->utility->bg_curl("$base_url/bs/wait_instance/".$this->BS->config['access_token']);
		
		//Email to user // spviewer@muggle.tw
		$browse_url = $this->BS->base_url[$this->BS->config['bs_type']]."/bs/browse?access_key={$_SESSION['user']['Access_key']}";
		$userpass = $this->APP->get_user_pass($_SESSION['user']['Email'], $instance_id);
		$mail_msg = $this->APP->get_email_message("RunStart", $_SESSION['user']['Name'], $browse_url, $userpass[0], $userpass[1]);
		$this->utility->send_mail($_SESSION['user']['Email'], $mail_msg['title'], $mail_msg['content']);
		
		//unset($_SESSION['config']);
		return true;
	}
	
	private function complete(&$run, $mail_type = "RunFinish")
	{
		$this->load->model($this->default_BS_model, 'BS');
		$this->load->model($this->BS->get_model_name_from_app_name($run['app_name']), 'APP');
		
		if( count($this->BS->config) == 0)
			$this->BS->init_bg($run['access_token']);
		
		$query = $this->db->get_where('peat_user', array("Href"=>$run['HrefUser'] ) );
		$user = $query->row_array();
		
		$browse_url = $this->BS->base_url[$this->BS->config['bs_type']]."/bs/browse?access_key={$user['Access_key']}";
		$userpass = $this->APP->get_user_pass($user['Email'], $run['InstanceId']);
		$mail_msg = $this->APP->get_email_message($mail_type, $user['Name'], $browse_url, $userpass[0], $userpass[1]);
		$this->utility->send_mail($user['Email'], $mail_msg['title'], $mail_msg['content']);
	}
	
	public function refund()
	{
		// set and check purchase id
		if( !isset($_GET['purchase_id']))
			$this->utility->log("error", __CLASS__.".".__FUNCTION__, "Refund", "Purchase Id Not Found.", "Please check purchase id.");
		$purchase_id = $_GET['purchase_id'];
		
		// set and check access key
		session_start();
		if(!isset($_SESSION['user']['Access_key']))
			$this->utility->log("error", __CLASS__.".".__FUNCTION__, "Refund", "Access key is wrong!.", "Please Login from browse page.");
		$BS_Access_key = $_SESSION['user']['Access_key'];
		
		$this->load->database();
		$this->load->model($this->default_BS_model, 'BS');
		
		// check and get user info
		$query = $this->db->get_where('peat_user', array("access_key"=>$BS_Access_key ) );
		if($query->num_rows() == 0)
			$this->utility->log("error", __CLASS__.".".__FUNCTION__, "Refund", "Access key is wrong!.", "Please check your access key.");
		$DB_user = $query->row_array();
		
		// get run and purchase information
		$sql = "SELECT * FROM `runs`, `purchase_product`, `purchase` WHERE `runs`.`HrefUser` = '{$DB_user['Href']}' AND `runs`.`purchase_id` = {$purchase_id} AND `purchase_product`.`purchase_id` = `runs`.`purchase_id` AND `purchase`.`uid` = `runs`.`purchase_id`";
		$query = $this->db->query($sql);
		if($query->num_rows() == 0)
			$this->utility->log("error", __CLASS__.".".__FUNCTION__, "Refund", "Refund error!", "Can not find purchase information.");
		$run = $query->row_array();

		$model_name = $this->BS->get_model_name_from_app_name($run['app_name']);
		$this->load->model($model_name, 'APP');
		
		// check refund is allowed
		$is_refund = $this->APP->check_is_refund($run);
		if(!$is_refund)
			$this->utility->log("error", __CLASS__.".".__FUNCTION__, "Refund", "Refund is Not Allowed!.", "Expire 1 day, or have been refund.");
		
		$this->BS->init_bg($run['access_token']);
		// Do refund
		$is_success = $this->BS->refund($run['Id'], $run['RefundSecret']);
		
		// update purchase information
		$this->BS->request_purchase_update($run['Id']);
		
		// Maybe Error
		if($is_success === false) // not clear error
			$this->utility->log("error", __CLASS__.".".__FUNCTION__, "Refund", "Refund Error!.", "Please contact us about this issue.");
		else if (is_array($is_success)) // basespace error, may be expire 1 hour?
			$this->utility->log("error", __CLASS__.".".__FUNCTION__, "Refund", "{$is_success['ErrorCode']}", "{$is_success['Message']}");
		
		// update status and close instance
		$this->update_progress($run['access_token'], $run['status']==1 ? 2 : 3, 1);
		
		// create view
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		$pages['main_page'] = $this->load->view('basespace/view_bs_refund', array(), true );
		$this->utility->createHtmlView($pages);
	}
	
	public function spviewer()
	{
		if( !isset($_GET['action']) )
			$this->utility->log("die", __CLASS__.".".__FUNCTION__, "spviewer", "No action!", "No 'action' parameter");
			
		switch($_GET['action'])
		{
			case "trigger":
				$this->trigger("spviewer", "basic");
				break;
			case "buy":
				$this->no_buy();
				break;
			case "purchase":
				$this->purchased();
				break;
			default:
				die("Error! Action is not exist");
		}
		
	}
	public function spviewer_hoth()
	{
		if( !isset($_GET['action']) )
			$this->utility->log("die", __CLASS__.".".__FUNCTION__, "spviewer", "No action!", "No 'action' parameter");
			
		switch($_GET['action'])
		{
			case "trigger":
				$this->trigger("spviewer_hoth", "hoth");
				break;
			case "buy":
				$this->buy();
				break;
			case "purchase":
				$this->purchased();
				break;
			default:
				die("Error! Action is not exist");
		}
		
	}
	
	public function browse()
	{
		session_start();
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		$BS_Access_key = NULL;
		
		if( isset($_GET['access_key']))
		{
			$BS_Access_key = $_GET['access_key'];
			$_SESSION['user']['Access_key'] = $BS_Access_key;
		}
		
		if($BS_Access_key == NULL )//&& !isset($_SESSION['BS_Access_key']) )
		{
			if(!isset($_SESSION['user']['Access_key']))
				$this->utility->log("error", __CLASS__.".".__FUNCTION__, "browse", "Access key is wrong!.", "Please check your access key.");
			$BS_Access_key = $_SESSION['user']['Access_key'];
		}
		//query db
		$this->load->database();
		$this->load->model($this->default_BS_model, 'BS');
		
		$query = $this->db->get_where('peat_user', array("access_key"=>$BS_Access_key ) );
		if($query->num_rows() == 0)
			$this->utility->log("error", __CLASS__.".".__FUNCTION__, "browse", "Access key is wrong!.", "Please check your access key.");
		$DB_user = $query->row_array();
		
		$sql = "SELECT * FROM `runs`, `purchase_product`, `purchase` WHERE `runs`.`HrefUser` = '{$DB_user['Href']}' AND `purchase_product`.`purchase_id` = `runs`.`purchase_id` AND `purchase`.`uid` = `runs`.`purchase_id` ORDER BY `runs`.`uid` DESC";
		$runs = $this->db->query($sql);
		
		$pages['main_page'] = $this->load->view('basespace/view_bs_browse', array('DB_user'=>$DB_user, 'DB_run'=>$runs, 'BS'=>$this->BS), true );
			
		$this->utility->createHtmlView($pages);
	}
	
	public function index()
	{
		$this->browse();
	}
	
	public function update_progress($access_token="", $status=1, $close_instance=0)
	{
		if($access_token == NULL)
			return;
		$this->load->model($this->default_BS_model, 'BS');
		$run = $this->BS->init_bg($access_token);
		$status_info = $this->BS->status_table($run["app_name"], $status);
		
		$Status = $status_info[0];
		$Summary = $status_info[1];

		$this->BS->set_appsession_status($run, $Status, $Summary);
					
		$data = Array("status" => $status, "DataRefresh" => time());
		$sql = $this->db->update_string('runs', $data, "access_token = '$access_token'");
		$this->db->query($sql);
		
		// $status == 1, Success Running, Do something
		if($status == 1)
			$this->complete($run, "RunFinish");
		
		// $close_instance == 1, auto stop instance...
		if($close_instance == 1 && $run['InstanceId'] != "0")
		{
			$this->load->model('csalt_aws_ec2_api','AWS');
			$this->AWS->instance_terminate(array($run['InstanceId']));
			if($status == 0) $this->complete($run, "TimeOut");
		}
	} 
	
	public function get_appsession($access_token="")
	{
		if(!$access_token)
			return;
		$this->load->model($this->default_BS_model, 'BS');
		$run = $this->BS->init_bg($access_token);
		$appsession = $this->BS->get_appsessions();
		echo json_encode($appsession);
	}
	
	public function wait_instance($access_token="")
	{
		if($access_token == NULL)
			return;
		$this->load->model($this->default_BS_model, 'BS');
		$run = $this->BS->init_bg($access_token);
		
		$instance_id = $run['InstanceId'];
		sleep(20);
		
		$query = $this->db->get_where( 'instances', array("InstanceId"=>$instance_id ) );
		if($query->num_rows() == 0)
		{
			$this->utility->log("die", __CLASS__.".".__FUNCTION__, $run['app_name'], "No instance id.", "Access token: $access_token");
		}
		$instance = $query->row_array();
		
		if($instance['State'] == 'terminated')
		{
			$this->utility->log("die", __CLASS__.".".__FUNCTION__, $run['app_name'], "Instance state wrong", "This instance already terminated. $access_token");
		}
		$this->utility->log("msg", __CLASS__.".".__FUNCTION__, $run['app_name'], "Waiting Instance", "Waiting For instance Running.");

		sleep(30);
		
		$this->load->model('csalt_aws_ec2_api','AWS');
		
		$run_times=0;
		while(1)
		{
			$states = $this->AWS->instance_update_status(array($instance_id));
			if($states[$instance_id] == 'running' )
				break;
			if($run_time++ == 20)
				$this->utility->log("die", __CLASS__.".".__FUNCTION__, $run['app_name'], "Waiting Too Long", "Waiting Too Long > 600s. $access_token");
			sleep(15);
		}
		$this->utility->log("msg", __CLASS__.".".__FUNCTION__, $run['app_name'], "Instance Running", "Instance Running Success");
		sleep(20);
		
		$model_name = $this->BS->get_model_name_from_app_name($run['app_name']);
		$this->load->model($model_name, 'APP');
		
		$this->update_progress($access_token, 9); //Pending images
		$is_success = $this->APP->run_command($run);
		
		if(!$is_success)
		{
			$this->update_progress($access_token, 0, 1);
			// maybe email
		}
		else
			$this->utility->log("msg", __CLASS__.".".__FUNCTION__, $run['app_name'], "Running Command Send", "Running Command Send Success");
	}
	
	
	// ############ for basespace gereral use ###############

	
	// #### for Test ####
	function test_mail()
	{
		if(!$this->allow_test) return;
		$this->utility->send_mail("TestUser", "spviewer@muggle.tw", "poi5305@gmail.com", "TestTitle", "TestContent");
		
	}
	function test_get_simple_url_retry()
	{
		//https://hoth.muggle.tw/bs/test_get_simple_url_retry
		if(!$this->allow_test) return;
		$this->load->model($this->default_BS_model, 'BS');
		$this->BS->config['access_token'] = "";
		echo $this->BS->get_simple_url("http://140.113.15.84/asjjjjjjjdfsaf");
	}
	function test_update_progress($access_token="", $status=1)
	{
		if(!$this->allow_test) return;
		//hoth.muggle.tw/bs/test_update_progress/d80977b2f65947718ce482922a0f1243/9
		$this->update_progress($access_token, $status);
		echo "Update progress done<br />";
	}
	function test_run_command($access_token="")
	{
		//return;
		if(!$this->allow_test) return;
		
		if($access_token == NULL)
			return;
		$this->load->model($this->default_BS_model, 'BS');
		$run = $this->BS->init_bg($access_token);
		
		$instance_id = $run['InstanceId'];
		
		if($run['app_name'] == "spviewer" || $run['app_name'] == "spviewer_hoth")
		{
			$model_name = "Csalt_bs_spviewer";	
		}
		else
		{
			die("No such app");
		}
		$this->load->model($model_name, 'APP');
		$new_status = $this->APP->run_command($run);
		
		system("echo '".date( "Y-m-d H:i:s", time())." : finish, success2 $access_token  \n' >> test");
	}
	function test_run_instance()
	{
		return;
		if(!$this->allow_test) return;
		
		$this->load->model('csalt_aws_ec2_api','AWS');
		
		$instance_ids = $this->AWS->instance_launch('q_run_instance_spviewer');
		$instance_id = $instance_ids[0];
		sleep(3);
		$states = $this->AWS->instance_update_status(array($instance_id));
		
		echo $instance_id . " id<br />";
		print_r($states);
		
	}
	function test_stop_instance($instance_id)
	{
		if(!$this->allow_test) return;
		$this->load->model('csalt_aws_ec2_api','AWS');
		$this->AWS->instance_terminate(array($instance_id));
	}
	function test_bg()
	{
		if(!$this->allow_test) return;
		file_put_contents("tmp_qsef/ttt", "asdfasdfs");
	}
	function test_bg_curl()
	{
		if(!$this->allow_test) return;
		$this->utility->bg_curl("http://hoth.muggle.tw/bs/test_bg/123");
		echo "http://hoth.muggle.tw/bs/test_bg/123";
	}
}

/* End of file basespace.php */
