<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *  @file basespace.php
 *  @brief controllers basespace
 *  @author JHH Corp.
 */

ini_set('display_errors', 1);
date_default_timezone_set('Asia/Taipei');

/**
 * @struct Basespace
 * 
 * @brief Basesapce 網頁控制入口 \n
 *        URL: http://www.jhhlab.tw/basespace
 *  
 * @tparam
 *  
 */
class Basespace_v2 extends CI_Controller {

	var $html_template = 'template_lazydays';
	
	/**
	 * @brief 讓網頁可以利用網址，像是 basespace/js/jquery/1.2 讀取到javascript內容
	 * @param $page is the name of javascript
	 * @param $version is the version of javescript
	 */
	public function js($page, $version)
	{
		$this->load->view("js/$page-$version.js");
	}
	public function css($page)
	{
		$this->load->view("{$this->html_template}/css/$page");
	}
	public function images($page)
	{
		$page = $this->uri->assoc_to_uri($this->uri->uri_to_assoc());
		if(getimagesize(APPPATH."/views/{$this->html_template}/images/$page"))
		{
			echo file_get_contents(APPPATH."/views/{$this->html_template}/images/$page");
		}
	}


	public function index()
	{
		$pages['main_page'] = $this->load->view('basespace/view_basespace_main_page','', true );
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		$this->createHtmlView($pages);
	}
	public function about_basespace()
	{}
	public function about_basespace_app()
	{}
	public function about_basespace_pipeline()
	{}
	public function about_basespace_workflow()
	{}
	public function about_peat()
	{
		//$data = Array();
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		$pages['main_page'] = $this->load->view('basespace/view_basespace_about_peat', '', true );
		$this->createHtmlView($pages);
	}
	function test($access_token)
	{
		//echo dirname(__FILE__);
		//return;
		$this->load->database();
		$query = $this->db->get_where( 'peat_runs', array("access_token"=>$access_token ) );
		$run = $query->row_array();
		$instance_id = $run['InstanceId'];
		
		$this->load->model('Csalt_baseSpace_srnap', 'BS_SRNAP');
		$new_status = $this->BS_SRNAP->run_command($run, $instance_id, $access_token);
	}
	
	public function browse()
	{
		//session_start();
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		$BS_Access_key = NULL;
		
		
		//echo $_SESSION['BS_Id'];
		
		if( isset($_GET['access_key']))
		{
			$BS_Access_key = $_GET['access_key'];
		}
		
		if($BS_Access_key == NULL )//&& !isset($_SESSION['BS_Access_key']) )
		{
			session_start();
			if(isset($_SESSION['BS_Id']))
			{
				$this->load->database();
				$this->load->model('Error_handle', 'EH');
				
				$query = $this->db->get_where('peat_user', array("Id"=>$_SESSION['BS_Id'] ) );
				if($query->num_rows() == 0)
					$this->EH->error_handle($pages,'Access key is wrong!.', 'Please check your access key.');
				
				$DB_user = $query->row_array();
				$runs = $this->db->get_where('peat_runs', array("HrefUser"=>$DB_user['Href'] ) );
				
				$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
				$pages['main_page'] = $this->load->view('basespace/view_basespace_browse', array('DB_user'=>$DB_user, 'DB_run'=>$runs, 'BS_PEAT'=>$this->BS_PEAT), true );
			}
			else
			{
				$pages['main_page'] = $this->load->view('basespace/view_basespace_browse_key', '', true );
			}
			
		}
		else
		{	
			//query db
			$this->load->database();
			$this->load->model('Error_handle', 'EH');
			
			$query = $this->db->get_where('peat_user', array("access_key"=>$BS_Access_key ) );
			if($query->num_rows() == 0)
				$this->EH->error_handle($pages,'Access key is wrong!.', 'Please check your access key.');
			
			$DB_user = $query->row_array();
			$runs = $this->db->get_where('peat_runs', array("HrefUser"=>$DB_user['Href'] ) );
			
			$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
			$pages['main_page'] = $this->load->view('basespace/view_basespace_browse', array('DB_user'=>$DB_user, 'DB_run'=>$runs, 'BS_PEAT'=>$this->BS_PEAT), true );
			
		}
		$this->createHtmlView($pages);
	}
	
	
	
	public function create_spot_instance()
	{
		$this->load->model('Csalt_aws_ec2_api', 'AWS');
		//$kk = $this->AWS->request('requestSpotInstances', 'q_run_spot_instance_peat');
		//$kk = $this->AWS->request('describeImages', 'q_image_list_self');
		var_dump($kk);
		echo "ok";
	}
	
	
	
	
	
	public function srnap_hoth()
	{	
		if( !isset($_GET['action']) )
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "basespace", "srnap_hoth", "No action");
			//********************************//
			die("Error! No action");
		}
		//start session
		session_start();

		$this->load->model('Csalt_basespace_api_v2', 'BS');
		$this->load->model('Csalt_basespace_srnap', 'BS_SRNAP');
		$BS_action = $_GET['action'];

		if($_GET['action'] == 'trigger')
		{
			$pages = $this->BS->trigger("srnap_hoth", "hoth");
			$this->createHtmlView($pages);
		}
		else if($_GET['action'] == 'buy')
		{
			//echo "srnap_hoth buy";
			$pages = $this->BS_SRNAP->srnap_buy();
			
			
			$this->createHtmlView($pages);
			//Free version
			//if($is_run)
			//{
				// this version have no free 
				//$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
				//$pages['main_page'] = $this->load->view('basespace/view_basespace_peat_run_ok', $data, true );
				//$this->createHtmlView($pages);
			//}
		}
		else if($_GET['action'] == 'purchase')
		{
			$this->load->model('Csalt_baseSpace_api', 'BS');
			
			$BS_purchaseid				 	= $_GET['purchaseid'];
			$is_run = $this->BS->run_purchase_check($BS_purchaseid);

			if($is_run)
			{
				$data = $this->BS_SRNAP->srnap_run();
				$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
				$pages['main_page'] = $this->load->view('basespace/view_basespace_srnap_run_ok', $data, true );
				$this->createHtmlView($pages);
			}
			else
			{
				die("You have not purchased");
			}
		}
		else
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "basespace", "peat", "unknow GET action");
			//********************************//
		}
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "basespace", "peat", "page success");
		//********************************//
	}
		
	public function peat_free()
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "basespace", "peat_free", "trigger init");
		//********************************//
		
		if( isset($_GET['action']) )
		{
			if($_GET['action'] == 'trigger')
			{
				//$BS_state								 = $_GET['state'];
				$BS_action							 = $_GET['action'];
				$BS_appsessionuri				 = $_GET['appsessionuri'];
				$BS_authorization_code	 = $_GET['authorization_code'];
			}
		}
		else
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "basespace", "peat_free", "No trigger");
			//********************************//
			die("Error! No trigger");
		}
		
		//start session
		session_start();
		// include model
		$this->load->model('Csalt_baseSpace_api', 'BS');
		$this->load->database();
		
		//########## app code, app session, init BS api
		// $_SESSION['BS_authorization_code']
		// $_SESSION['BS_appsessionuri']
		// $_SESSION['BS_access_token']
		// $_SESSION['app_name']
		$this->BS->session_check_init("peat_free", $BS_appsessionuri, $BS_authorization_code);
		
		//########## app user
		$this->BS->get_user_info();
		$_SESSION['BS_Id'] = $BS_Id = $this->BS->user_data['Id'];
		$_SESSION['BS_Access_key'] = $BS_Access_key = md5( $this->BS->user_data['Id'] . $this->BS->user_data['Name'] . $this->BS->user_data['Email'] );
		
		//########## save user info
		$this->BS->save_user_info($BS_Id, $BS_Access_key);
		
		//output for html
		$this->BS->get_projects_from_user();
		//$this->BS->get_runs_from_user();

		
		$data = array('user_projects'=>$this->BS->user_projects, 'appsession'=>$_SESSION['BS_appsessionuri']);
		
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		$pages['main_page'] = $this->load->view('basespace/view_basespace_peat_free', $data, true );
		$this->createHtmlView($pages);
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "basespace", "peat_free", "trigger success, $BS_appsessionuri, $BS_authorization_code");
		//********************************//
	}
	
	public function peat_hoth()
	{
		if( !isset($_GET['action']) )
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "basespace", "peat", "No action");
			//********************************//
			die("Error! No action");
		}
		
		//start session
		session_start();
		
		//load model BS_PEAT
		$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
		$BS_action								= $_GET['action'];
		
		if($_GET['action'] == 'trigger')
		{
			$this->trigger("peat_hoth", "hoth");
		}
		else if($_GET['action'] == 'buy')
		{
			$data = $this->BS_PEAT->srnap_buy();
			if($data)
			{
				$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
				$pages['main_page'] = $this->load->view('basespace/view_basespace_peat_run_ok', $data, true );
				$this->createHtmlView($pages);
			}
			
		}
		else if($_GET['action'] == 'purchase')
		{
			$BS_purchaseid				 	= $_GET['purchaseid'];
			$data = $this->BS_PEAT->srnap_purchase_check($BS_purchaseid);
			$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
			$pages['main_page'] = $this->load->view('basespace/view_basespace_peat_run_ok', $data, true );
			$this->createHtmlView($pages);
		}
		else
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "basespace", "peat", "unknow GET action");
			//********************************//
		}
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "basespace", "peat", "page success");
		//********************************//
	}
	
	public function request_run_peat_free()
	{
		session_start();
		
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		
		$this->load->model('Csalt_baseSpace_api', 'BS');
		
		//變數 session 確認
		$this->BS->run_peat_check_init();
		//檢查檔案正確性
		$paired_files = $this->BS->run_peat_check_files();
		//取得所有檔案大小，並且檢查
		$AllFileSize = 0;
		foreach($paired_files as $file)
		{
			$AllFileSize += $file['R1']['Size'];
			$AllFileSize += $file['R2']['Size'];
		}
		
		if($AllFileSize == 0)
			$this->error_handle($pages,'File size ERROR');
		if($AllFileSize > 50 *1024 *1024 *1024)
			$this->error_handle($pages,'Sorry, your files are too large');

		//檢查資料庫，使用者資訊以及是否已經app session 跑過
		$DB_user = $this->BS->run_peat_check_db();

		//檢查通過		
		$project_name = "PEAT-FREE-proj-".$this->BS->sample_data['Name'];
		$sample_name = $this->BS->sample_data['Name'];
		$appresult_name = "PEAT-FREE-appr-".$this->BS->sample_data['Name']."-".date('Y-m-d-H-i-s', time());
		
		// 新建 project
		$this->BS->create_project($project_name);
		
		// 新建 appresult
		$this->BS->create_appresult($appresult_name, "PEAT result", $this->BS->sample_data['Href']);
		
		//開機器並且執行
		$this->load->model('csalt_aws_ec2_api','AWS');
		$instance_ids = $this->AWS->instance_launch('q_run_instance_peat');
		$instance_id = $instance_ids[0];
		
		//$instance_id = 0;
		
		$run_id = $this->BS->run_peat_save_run($instance_id ,$project_name, $sample_name, $appresult_name, $AllFileSize);

		$this->BS->run_peat_save_files($run_id, $paired_files);
		
		//wait for instance
		$this->bg_curl("http://localhost/basespace/query_task_peat/waiting_for_instance_running/".$_SESSION['BS_access_token']);
		
		//Email to user
		
		$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
		$this->BS_PEAT->send_mail($DB_user, $_SESSION['app_name'], "start_run", NULL);
		
		//Show Html
		$data = array(
			'DB_user'						=>	$DB_user,
			'Files'							=>	$paired_files,
			'sample_name'				=>	$sample_name,
			'project_name'			=>	$project_name,
			'appresult_name'		=>	$appresult_name,
			'AllFileSize'				=>	$AllFileSize
		);
		$pages['main_page'] = $this->load->view('basespace/view_basespace_peat_run_ok', $data, true );
		$this->createHtmlView($pages);
		
		//session_unset();
	}
	
	public function query_task_peat($request=NULL, $access_token=NULL)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(4, "basespace", "query_task_peat", "init $request, $access_token");
		//********************************//
		
		if($request==NULL || $access_token==NULL)
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "basespace", "query_task_peat", "No query or access_token");
			//********************************//
			die("No query or access_token");	
		}
			
			
		$this->load->database();
		
		$query = $this->db->get_where( 'peat_runs', array("access_token"=>$access_token ) );
		if($query->num_rows() == 0)
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(2, "basespace", "query_task_peat", "No access token error.");
			//********************************//
			die("No access token error.");
		}
			
			
		$run = $query->row_array();
		
		$this->load->model('Csalt_baseSpace_api', 'BS');
		$this->BS->init($run['task'], NULL, NULL);
		$this->BS->set_access_token($access_token);
		
		//********** Saving Logs *********//
		$this->csalt_log->add(3, "basespace", "query_task_peat", "run_id: {$run['uid']}, $request, $access_token");
		//********************************//
		
		switch($request)
		{
			// !status
			case "status":
				if(!isset($_GET['status']) )//|| !isset($_GET['appsession']))
					die("GET not set.");
				
				//********** Saving Logs *********//
				$this->csalt_log->add(4, "basespace", "query_task_peat", "request status init");
				//********************************//
				
				$status = $_GET['status'];
				$appsession = $run['AppSession'];
				
				if($run['status'] == 0)
					die("This app session already finish.");
				
				$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
				$status_info = $this->BS_PEAT->peat_status_table($status);
				$Status = $status_info[0];
				$Summary = $status_info[1];
	
				$this->BS->set_appsession_status($appsession, $Status, $Summary);
							
				$data = Array(
					"status" 					=>	$status,
					"DataRefresh"			=> time()
				);
				
				$sql = $this->db->update_string('peat_runs', $data, "AppSession = '$appsession'");
				$this->db->query($sql);
				
				//********** Saving Logs *********//
				$this->csalt_log->add(3, "basespace", "query_task_peat", "request status, update peat runs success: $status");
				//********************************//
				
				echo "OK";
				
				break;
				
			// !status_finish
			case "status_finish":
			
				$appsession = $run['AppSession'];
				
				if($run['status'] == 0)
					die("This app session already finish.");
					
				//********** Saving Logs *********//
				$this->csalt_log->add(4, "basespace", "query_task_peat", "request status_finish init");
				//********************************//
				
				$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
				$status_info = $this->BS_PEAT->peat_status_table(0);
				
				$Status = $status_info[0];
				$Summary = $status_info[1];
				$this->BS->set_appsession_status($appsession, $Status, $Summary);
				
				$data = Array(
					"status" 			=>	0,
					"DataRefresh"			=> time(),
					"DateFinish"	=>	time()
				);
				
				$sql = $this->db->update_string('peat_runs', $data, "AppSession = '$appsession'");
				$this->db->query($sql);
				
				$instance_id = $run['InstanceId'];
				
				$this->load->model('csalt_aws_ec2_api','AWS');
				
				
				$is_terminated = $this->AWS->instance_terminate( array($instance_id) );
				echo "close instance $instance_id !!!!!\n ";
				
				//********** Saving Logs *********//
				$this->csalt_log->add(2, "basespace", "query_task_peat", "request status_finish, terminate instance: $instance_id success");
				//********************************//
				
				//如果機器關機失敗...
				
				//email to user
				$query = $this->db->get_where( 'peat_user', array("Href"=>$run['HrefUser'] ) );
				$DB_user = $query->row_array();
				
				$project_id = explode("/",$run['HrefProjects']);
				$project_id = $project_id[2];
				
				$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
				$this->BS_PEAT->send_mail($DB_user, $run['task'], "finish_run", Array('project_id'=>$project_id));
				
				//********** Saving Logs *********//
				$this->csalt_log->add(3, "basespace", "query_task_peat", "request status_finish success");
				//********************************//
				
				break;
			// !waiting_for_instance_running
			case "waiting_for_instance_running":
				
				$instance_id = $run['InstanceId'];
				//********** Saving Logs *********//
				$this->csalt_log->add(2, "basespace", "query_task_srnap", "request waiting_for_instance_running: $instance_id, $access_token init");
				//********************************//
				sleep(20);
				
				$query = $this->db->get_where( 'peat_instance', array("InstanceId"=>$instance_id ) );	
				if($query->num_rows() == 0)
				{
					//********** Saving Logs *********//
					$this->csalt_log->add(1, "basespace", "query_task_peat", "request waiting_for_instance_running: $instance_id, Error No such instance in database...");
					//********************************//
					//$this->bg_curl("http://localhost/~andy/csalt/index.php/basespace_v2/query_task_peat/failure/$access_token");
					$this->bg_curl($this->config->item('full_url')."basespace_v2/query_task_peat/failure/$access_token");
					
					$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
					$this->BS_PEAT->send_mail_manual("PEDDING ERROR!","No instance in DB. Terminating... \nAccess_token: $access_token","poi5305@gmail.com");
					
					die("No instance id");
				}
					
				
				$instance = $query->row_array();
				
				if($instance['State'] == 'terminated')
				{
					//********** Saving Logs *********//
					$this->csalt_log->add(2, "basespace", "query_task_peat", "request waiting_for_instance_running: This instance already terminate");
					//********************************//
					die('This instance already terminated.');
				}
					
				system("echo '".date( "Y-m-d H:i:s", time())." : init $access_token  \n' >> test");
				
				$this->load->model('csalt_aws_ec2_api','AWS');
	
				sleep(50);
				
				$run_times=0;
				while(1)
				{
					$states = $this->AWS->instance_update_status(array($instance_id));

					
					if($states[$instance_id] == 'running' )
						break;
					
					$run_times++;
					
					if($run_time == 100)
						die('Instance status error!');
					
					sleep(15);
				}
				sleep(10);
				
				//********** Saving Logs *********//
				$this->csalt_log->add(3, "basespace", "query_task_peat", "request waiting_for_instance_running: instance status: running and compiling");
				//********************************//
				
				// run command
				if($run['task'] == "peat" || $run['task'] == "peat_free" || $run['task'] == "peat_hoth" || $run['task'] == "PEAT")
				{
					$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
					$new_status = $this->BS_PEAT->run_command($run, $instance_id, $access_token);
				}
				elseif($run['task'] == "srnap" || $run['task']=="srnap_hoth")
				{
					$this->load->model('Csalt_baseSpace_srnap', 'BS_SRNAP');
					$new_status = $this->BS_SRNAP->run_command($run, $instance_id, $access_token);
				}
				// 設定狀態為 Running PEAT
				$_GET['status'] = $new_status;
				$this->query_task_peat('status', $access_token);
				
				break;
			// !update_progress
			case "update_progress":
			
				if($run['status'] == 0)
				{
					//********** Saving Logs *********//
					$this->csalt_log->add(2, "basespace", "query_task_peat", "request update_progress. This app session already finish.");
					//********************************//
					die("This app session already finish.");
				}
					
				if( ! isset($_GET['files_uid']) )
				{
					//********** Saving Logs *********//
					$this->csalt_log->add(2, "basespace", "query_task_peat", "request update_progress. No files uid.");
					//********************************//
					die("No files uid.");
				}
					
				if( ! isset($_GET['f1_Status']) )
				{
					//********** Saving Logs *********//
					$this->csalt_log->add(2, "basespace", "query_task_peat", "request update_progress. No file_size1.");
					//********************************//
					die("No file_size1.");
				}
					
				
				//********** Saving Logs *********//
				$this->csalt_log->add(3, "basespace", "query_task_peat", "request update_progress init, {$_GET['files_uid']}, {$_GET['f1_Status']}");
				//********************************//
								
				//更新 peat_files
				$files_uid = $_GET['files_uid'];
				$f1_Status = $f2_Status = $_GET['f1_Status'];
				
				
				$data = Array(
					"f1_Status" 			=>	$f1_Status,
					"f2_Status"				=>	$f2_Status
				);
				$sql = $this->db->update_string('peat_files', $data, "uid = '$files_uid'");
				$this->db->query($sql);
				
				//更新 peat_runs
				$run_id = $run['uid'];
				$query = $this->db->get_where( 'peat_files', array("run_id"=>$run_id ) );
				$DoneFilesSize = 0;
				
				foreach ($query->result_array() as $row )
				{
					$DoneFilesSize += ($row['f1_Size'] * ($row['f1_Status']/10000) + $row['f2_Size'] * ($row['f2_Status']/10000) );
				}
				//echo $DoneFilesSize;
				$data = Array(
					"DoneFilesSize"		=> $DoneFilesSize,
					"DataRefresh"			=> time()
				);
				
				$sql = $this->db->update_string('peat_runs', $data, "uid = '$run_id'");
				$this->db->query($sql);
				
				//********** Saving Logs *********//
				$this->csalt_log->add(4, "basespace", "query_task_peat", "request update_progress success, $files_uid, $f1_Status");
				//********************************//
				
				break;
			// !file_list
			case "file_list":
				//********** Saving Logs *********//
				$this->csalt_log->add(4, "basespace", "query_task_peat", "request file_list init");
				//********************************//
				
				$query = $this->db->get_where( 'peat_files', array('run_id'=>$run['uid']) );
				
				if($query->num_rows() == 0)
					die("No file list.");
				
				if(strstr($run['task'], 'hoth'))
					$platform = "https://api.cloud-hoth.illumina.com";
				else
					$platform = "https://api.basespace.illumina.com";
					
				$result = array('files'=>array(), 'access_token'=>$access_token, 'HrefAppresult'=>$run['HrefAppresult'], 'platform'		=> "$platform");
				
				foreach ($query->result_array() as $row)
				{
					$result['files'][] = array(
						'R1_name'		=> $row['f1_Name'],
						'R2_name'		=> $row['f2_Name'],
						'R1_content'	=> $row['f1_Href'],
						'R2_content'	=> $row['f2_Href'],
						'R1_size'		=> $row['f1_Size'],
						'R2_size'		=> $row['f2_Size'],
						'R1_osize'		=> $row['f1_OSize'],
						'R2_osize'		=> $row['f2_OSize'],
						'uid'			=> $row['uid']
					);
				}
				$msg = json_encode($result);
				echo $msg;
				//********** Saving Logs *********//
				$this->csalt_log->add(3, "basespace", "query_task_peat", "request file_list success, $msg");
				//********************************//
				break;
			// !failure
			case "failure":
				$instance_id = $run['InstanceId'];
				
				$this->load->model('csalt_aws_ec2_api','AWS');
				
				
				//$is_terminated = $this->AWS->instance_terminate( array($instance_id) );
				echo "close instance $instance_id !!!!!\n ";
				
				//********** Saving Logs *********//
				$this->csalt_log->add(0, "basespace", "query_task_peat", "request failure, terminate instance: $instance_id success");
				//********************************//
				
				//update database, status 1 => unknow error
				$sql = $this->db->update_string('peat_runs', array("status"=>1), "access_token='$access_token'");
				$this->db->query($sql);
				
				//email to user
				$query = $this->db->get_where( 'peat_user', array("Href"=>$run['HrefUser'] ) );
				$DB_user = $query->row_array();
				
				$project_id = explode("/",$run['HrefProjects']);
				$project_id = $project_id[2];
				
				//email to admin
				$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
				$this->BS_PEAT->send_mail($DB_user, $run['task'], "failure", Array('project_id'=>$project_id));
				$this->BS_PEAT->send_mail_manual("RUN ERROR!","Unknow error. Access_token: $access_token","poi5305@gmail.com");
				
				
				break;
			
			
			default:
				die("? error!");
			
		}
		
	}
	
	
	
	
	
	// for bg control, these function will be move to other controller in the future
	public function bg_control($service, $status, $password)
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(2, "basespace", "bg_control", "Do bg_program, $service, $status, $password");
		//********************************//
		
		$pages['sub_menu'] = $this->load->view('basespace/view_basespace_sub_menu','', true );
		
		$this->load->database();
		$query = $this->db->get_where("bg_service", array("service"=>$service) );
		
		if($query->num_rows() == 0)
			$this->EH->error_handle($pages,'No such service', 'Please check your service name');
		
		$sv = $query->row_array();
		
		if($sv['password'] != $password )
			$this->EH->error_handle($pages,'Password Error!', 'Please check your service password');
		
		if($status == $sv['status'])
			$this->EH->error_handle($pages,'This service is still $status', 'Please check your service status');
		
		if($status == "start")
			$status = 1;
		else if($status == "stop")
			$status = 0;
		else
			$this->EH->error_handle($pages,'Status Error!', 'No such status, $status');
		
		$sql = $this->db->update_string('bg_service', Array("status"=>$status), "uid = '{$sv['uid']}'");
		$this->db->query($sql);
		
		//$this->load->model("csalt_service", "SV");
		//$this->SV->$service($status);
		
	}
	public function bg_program($password="")
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(2, "basespace", "bg_program", "Do bg_program, $password");
		//********************************//
		
		if($password != "qsefth")
			exit;
		$this->bg_curl("http://localhost/basespace/bg_program_impl/agjr12asdfasd34jk");
		//********** Saving Logs *********//
		$this->csalt_log->add(2, "basespace", "bg_program", "Do bg_program Successed, $password");
		//********************************//

		echo "Success";
	}
	
	public function bg_program_impl($password="")
	{
		//********** Saving Logs *********//
		$this->csalt_log->add(2, "basespace", "bg_program_impl", "Do bg_program_impl, $password");
		//********************************//
		if($password != "agjr12asdfasd34jk")
			exit;
			
		$this->load->model("csalt_service", "SV");
		$this->load->database();
		while(true)
		{
			//********** Saving Logs *********//
			$this->csalt_log->add(4, "basespace", "bg_program_impl", "Loop control service");
			//********************************//
			
			$query = $this->db->get_where("bg_service", array("service" => "bg_program"));
			
			if($query->num_rows() == 0)
			{
				//********** Saving Logs *********//
				$this->csalt_log->add(2, "basespace", "bg_program_impl", "ERROR!!,no bg_program Exit bg_program_impl");
				//********************************//
			}
				
			
			$bg = $query->row_array();
			
			if($bg['status'] == 0)
			{
				//********** Saving Logs *********//
				$this->csalt_log->add(2, "basespace", "bg_program_impl", "Exit bg_program_impl");
				//********************************//
			}
			$query = $this->db->get_where("bg_service", array("status" => 1) );
			foreach($query->result_array() as $row)
			{
				$uid = $row['uid'];
				$service = $row['service'];
				$status = $row['status'];
				try
				{
					$this->SV->$service();
				}
				catch (Exception $e)
				{
					echo "Error!, $service error.";
					//********** Saving Logs *********//
					$this->csalt_log->add(2, "basespace", "bg_program_impl", "Error!, $service error.");
					//********************************//
				}
				$sql = $this->db->update_string('bg_service', Array("time"=>time()), "uid = '$uid'");
				$this->db->query($sql);
			}
			break;
			sleep(240);
		}
	}
	
	// ############ for basespace gereral use ###############
	
	public function jsquery($href=NULL)
	{
		session_start();

		if( !isset($_SESSION['BS_access_token']) )
			die("Error! No access_token!");
		
		if( !isset($_SESSION['app_name']) )
			die("Error! No app name.");
		
		if( $href == NULL)
			die("Error! No href.");
		
		$href = $this->uri->assoc_to_uri($this->uri->uri_to_assoc());
		
		//BS model init	
		$this->load->model('Csalt_baseSpace_api', 'BS');
		$this->BS->init($_SESSION['app_name'], NULL, NULL, $_SESSION['bs_type']);
		$this->BS->set_access_token($_SESSION['BS_access_token']);
		
		echo json_encode( $this->BS->get_require_from_js( $href ) );
	}
	
	// private deal with html
	private function error_handle($data=array(), $error_h2='Unknow error!', $error_p='Please check.')
	{
		
		if(!isset($data['main_page']))	$data['main_page']		= $this->load->view("basespace/view_basespace_error.php", array('error_h2'=>$error_h2, 'error_p'=>$error_p), true);
		$this->createHtmlView($data);
		die();
	}
	
	private function createHtmlView($data=array())
	{
		
		if(!isset($data['head']))				$data['head']					= $this->load->view($this->config->item('template')."/view_head.php", '', true);
		if(!isset($data['header']))			$data['header']				= $this->load->view($this->config->item('template')."/view_header.php", '', true);
		if(!isset($data['main_menu']))	$data['main_menu']		= $this->load->view($this->config->item('template')."/view_main_menu.php", '', true);
		if(!isset($data['sub_menu']))		$data['sub_menu']			= $this->load->view($this->config->item('template')."/view_sub_menu.php", '', true);
		if(!isset($data['main_page']))	$data['main_page']		= $this->load->view($this->config->item('template')."/view_main_page.php", '', true);
		if(!isset($data['footer']))			$data['footer']				= $this->load->view($this->config->item('template')."/view_footer.php", '', true);
		echo $this->load->view($this->config->item('template')."/view_index.php", $data, true);
	}

	private function bg_curl($url)
	{
		$pid = pcntl_fork();
		if(!$pid)
		{
			system("echo 'start curl -k \"$url\"' >> test");
			shell_exec("curl -k \"$url\" >/dev/null 2>&1");
			system("echo 'finish curl -k \"$url\"' >> test");
			exit;	
		}
		return;
		
		system("echo 'nohup curl -k \"$url\" &' >> test");
		shell_exec("nohup curl -k \"$url\" >/dev/null 2>&1 &");
		
		return;
		
		$ch1 = curl_init();
		curl_setopt($ch1, CURLOPT_URL, $url);
		curl_setopt($ch1, CURLOPT_HEADER, 0);
		curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, 0);
		$mh = curl_multi_init();
		curl_multi_add_handle($mh,$ch1);
		$active = null;
		$mrc = curl_multi_exec($mh, $active);
	}
}

/* End of file basespace.php */
