<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $html_template = 'template_lazdays';
	
	

	public function css($page)
	{
		$this->load->view("{$this->html_template}/css/$page");
	}
	public function images($page)
	{
		$page = $this->uri->assoc_to_uri($this->uri->uri_to_assoc());
		if(getimagesize("application/views/{$this->html_template}/images/$page"))
		{
			echo file_get_contents("application/views/{$this->html_template}/images/$page");
		}
		//$this->load->view("{$this->html_template}/images/$page");
	}
	
	public function js($page, $version)
	{
		$this->load->view("js/$page-$version.js");
	} 
	 
	
	 
	 
	public function index()
	{
		$this->load->view('welcome_message');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */