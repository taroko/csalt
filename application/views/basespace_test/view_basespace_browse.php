		<style>
			table
			{
				width:100%;
			}
			td
			{
				padding-top:10px; 
				cursor: pointer;
				border-bottom: #888 1px solid;
			}
			.task_detail_table
			{
				background-color: #444;
				padding: 5px;
			}
			.task_files_detail_table
			{
				background-color: #444;
				padding: 5px;
			}

		</style>
		<script src="js/jquery/2.0"></script>
		<script src="js/c-salt/1.0"></script>
		<script>
		  $(function(){
		  	$(".task_tr").bind('click',function(){
		  		$(this).next().toggle(200);
		  	});
		
		
		  });
		</script>
		<div class="width75 floatRight">
			
			

      <!-- Gives the gradient block -->
      <div class="gradient">
      
      	<h1>All Tasks</h1>
      	
      	<p>
      	
		    	<table>
					<?php

					foreach ($DB_run->list_fields() as $field)
					{
						echo "<tr>";
						echo "<td>Task</td>";
						echo "<td>Project</td>";
						echo "<td>Status</td>";
						echo "<td>Progress</td>";
						echo "<td>Start Date</td>";
						echo "</tr>";
						break;
					}
					foreach($DB_run->result_array() as $task)
					{
						$this->load->model('Csalt_baseSpace_peat', 'BS_PEAT');
						$status = $BS_PEAT->peat_status_table($task['status']);
						
						echo "<tr class='task_tr'>";
						echo "<td>{$task['task']}</td>";
						echo "<td>{$task['ProjectName']}</td>";
						echo "<td>".$status[1]."</td>";
						echo "<td>".Round($task['DoneFilesSize']*100/$task['AllFilesSize'])." %</td>";
						echo "<td>".date("Y-m-d H:i:s",$task['DateCreate'])."</td>";
						//echo "<td><a href='".base_url('basespace')."/query_task_peat/status/{$task['access_token']}?status=0&appsession={$task['AppSession']}'>{$task['status']}</a></td>";
						echo "</tr>";
						
						echo "<tr style='display:none;'>";
						echo "<td colspan=5>";
						
						$files = $this->db->get_where('peat_files', array("run_id"=>$task['uid'] ) );
					?>
						<table class="task_detail_table table w100">
							<tr>
								<td>Sample name: </td>
								<td><?php echo $task['SampleName']; ?></div>
							</tr>
							<tr>
								<td>App result name: </td>
								<td><?php echo $task['AppresultName']; ?></div>
							</tr>
							<tr>
								<td>Process: </td>
								<td><?php echo $task['DoneFilesSize']; ?> / <?php echo $task['AllFilesSize']; ?> B</div>
							</tr>
							<tr>
								<td>Running Time: </td>
								<td>
								<?php
									if($task['status'] == 1)
										echo ($task['DataRefresh'] - $task['DateCreate'] );
									elseif($task['DateFinish']==0) 
										echo (time() - $task['DateCreate'] );
									else
										echo ($task['DateFinish'] - $task['DateCreate'] );
								?>
								 s</td>
							</tr>
						</table>
						
						
						<table class="task_files_detail_table">
							<tr>
								<td>File name</td>
								<td>Progress</td>
							</tr>
							<?php foreach($files->result_array() as $file) 
							{	
							?>
							<tr>
								<td><?php echo $file['f1_Name']; ?></td>
								<td><?php echo $file['f1_Status']/100; ?> %</td>
							</tr>
							<tr>
								<td><?php echo $file['f2_Name']; ?></td>
								<td><?php echo $file['f2_Status']/100; ?> %</td>
							</tr>
							<?php
							}
							?>
						</table>
					<?php
						echo "</td>";
						echo "</tr>";
					}
					
					?>
					</table>

      	</p>
      
      </div>

    </div>
