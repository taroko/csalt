		<!-- 25 percent width column, aligned to the left -->
		<div class="width25 floatLeft leftColumn">

			<h1>BaseSpace</h1>

			<ul class="sideMenu">
				<!--
				<li class="here">
					About
					<ul>
						<li><a href="about_basespace" title="Jump to section">Basespace</a></li> 
						<li><a href="about_basespace_app" title="Jump to section">App In Basespace</a></li>
						<li><a href="about_basespace_pipeline" title="Jump to section">Pipeline In Basespace</a></li>
						<li><a href="about_basespace_workflow" title="Jump to section">Work flow</a></li>
					</ul>
				</li>
				-->
				<li class="here">
					Browse
					<ul>
						<li><a href="<?php echo base_url('basespace/browse'); ?>" title="Jump to section">Browse tasks</a></li>
					</ul>
				</li>
				<li class="here">
					PEAT
					<ul>
						<li><a href="about_peat" title="Jump to section">What is PEAT ?</a></li>
						<li><a href="https://basespace.illumina.com/apps/245245/PEAT-FREE" title="Jump to section">Run it for free!</a></li>
					</ul>
				</li>
				<li class="here">
					Links
					<ul>
						<li><a href="https://basespace.illumina.com/dashboard" title="Basespace">Go Basespace</a></li>
					</ul>
				</li>
			</ul>

			


		</div>