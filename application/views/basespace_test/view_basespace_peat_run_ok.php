	<script src="js/jquery/2.0"></script>
	<script src="js/c-salt/1.0"></script>
		<div class="width75 floatRight">


      <!-- Gives the gradient block -->
      <div class="gradient">
      
      	<h1>Success!!</h1>
				<blockquote class="go">
					<h2>Hello <?php echo $DB_user['Name']; ?> ~! Your job is now running!!!</h2>
					<p>
						Input sample: <?php echo $sample_name; ?>
						<br />
						New project name: <?php echo $project_name; ?>
						<br />
						Result folder name: <?php echo $appresult_name; ?>
						<br />
						Running files list (Total Size : <?php echo (int)($AllFileSize/1000000); ?> MB): 
						<br />
						<?php 
						foreach($Files as $fItem)
						{
							echo "{$fItem['R1']['Name']} ({$fItem['R1']['Size']} B) <br />";
							echo "{$fItem['R2']['Name']} ({$fItem['R2']['Size']} B) <br />";
							/*
							echo "<tr>";
							echo "<td>".$fItem['R1']['Name']."</td>";
							echo "<td>".$fItem['R2']['Name']."</td>";
							echo "</tr>";
							*/
						}
						?>
					</p>
					
				</blockquote>
				
				<blockquote class="go">
					<h3>
						We have sent an email containing a link to view your jobs.
					</h3>
					<p>
						Or you can <a href="<?php echo base_url('basespace_test/browse')."?access_key={$DB_user['access_key']}" ;?>">View jobs</a> Now!
					</p>
				</blockquote>
      </div>

    </div>
