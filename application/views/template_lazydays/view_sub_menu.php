<!-- 25 percent width column, aligned to the left -->
<div class="width25 floatLeft leftColumn">
	<h1>Menu</h1>
	<ul class="sideMenu">
		
		<li class="here">
			products
			<ul>
				<li><a href="#smallrnapipeline" title="Jump to section">sRNA Pipeline (Small RNA Pipeline)</a></li>
				<li><a href="#smallrnapipelineviewer" title="Jump to section">sRNA Viewer (Small RNA Visualizer)</a></li>
			</ul>
		</li>
		<li class="here">
			About Us
			<ul>
				<li><a href="#contactus" title="Jump to section">Contact Us</a></li>
			</ul>
		</li>
		<li class="here">
			Links
			<ul>
				<li><a href="https://basespace.illumina.com/dashboard/index" title="Basesapce">Basespace</a></li>
			</ul>
		</li>
	</ul>
</div>