<head>
	<title>CloudxDNA - a Small RNA Pipeline provider</title>
	<link href="images/favicon.ico" rel="SHORTCUT ICON">
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
	<meta name="author" content="cloudxdna" />
	<meta name="keywords" content="cloudxdna, Small RNA pipeline, Small RNA Visualizer, Viewer, Bioinformatic, NGS, sequencing, RNA-seq, sRNA, small rna, cloud, dna" />
	<meta name="description" content="Provided to handle resequencing data processing of Small RNA sequencing. Small RNA Pipeline and Small RNA Viewer." />
	<meta name="robots" content="index, follow, noarchive" />
	<meta name="googlebot" content="noarchive" />
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-55534856-1', 'auto');
	  ga('send', 'pageview');
	
	</script>

	<link rel="stylesheet" type="text/css" href="css/html.css" media="screen, projection, tv " />
	<link rel="stylesheet" type="text/css" href="css/layout.css" media="screen, projection, tv" />
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />

</head>