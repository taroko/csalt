<div class="width75 floatRight">


<!-- Gives the gradient block -->
<div class="gradient">
	
	<a name="fluidity"></a>
	
	<h1>Products</h1>
	<a name="smallrnapipeline"></a>
	<h2>sRNA Pipeline (Small RNA Pipeline)</h2>
	<h3>General:</h3>
	<p>
		Provided to handle resequencing data processing of Small RNA-Seq, where input data are applied with barcode de-multiplexing, adapter trimming, reference alignment, database-oriented annotation/classification, and statistical calculation for the annotated classifications. In addition, reads of the annotated classifications are exported as bam and bigwig files, the statistics results are exported as tens of thousands of csv tables wrapped in a tar file. We provide a web-based APP - sRNA Viewer that can render the analysis csv tables in a user-interactive manner.
	</p>
	
	<h3>Technical features:</h3>
	<p>
		<ul>
			<li>Input file format: Samples of FastQ format</li>
			<li>Reference aligner: Tailor aligner, supporting references of: human (UCSC hg19), mouse (UCSC mm10), zebrafish (UCSC danRer6), C. elegans (UCSC ce10), and D. melanogaster (UCSC dm3)</li>
			<li>Reads with length, estimated after adapter and barcode trimming, smaller than 15 nt and greater than 72 nt will be discarded</li>
			<li>Reads multiply aligned more than 1000 spots discarded</li>
			<li>Annotation: <a target="_blank" href="http://www.ensembl.org/" title="Ensembl">Ensembl</a> biotype and pre-miRNA annotation release 75, wherein pre-miRNA database is exclusively for miRNA heterogeneity analysis</li>
			<li>
				Statistical analysis:
				<ul>
					<li>Annotation/length/biotype/subtype oriented length distributions</li>
					<li>Sequence features, e.g. first/last 1nt, first/last 2nt, seed regions and tailing sequence, oriented length distributions</li>
					<li>microRNA heterogeneity</li>
					<li>microRNA sequence logo</li>
				</ul>
			</li>
			<li>Reads with tail sequence longer than 64 nt presented as 64 straight 'A' sequence</li>
		</ul>	
	</p>
	
	<h3>Output Naming Rule:</h3>
	<p>
		Output bam/bigwig files are named in a numeric encoded manner, wherein 4 digits are used to represent what kind of aligned reads are presented in the corresponding bam/bigwig files, e.g. BWG.2.1.0.0.miRNA.bwg. In detail, the 4 digits, from left to right, respectively serve for system config, filter config, database index config, and database hierarchy config purpose
	</p>
	
	<p>
		<ul>
			<li>database hierarchy config is not used currently and remains to be 0 for all of the output files</li>
			<li>database index config is used to indicate with which database the aligned reads are intersected</li>
			
			<ul>
				<li>database index config of 0 indicates the Ensembl biotype database, while database index config of 1 indicates the Ensembl pre-miRNA database</li>
				<li>For example, aligned reads intersected with Ensembl biotype database are recorded in BWG.0.1.0.0._.bwg file, while that intersected with Ensembl pre-miRNA database are recorded in BWG.0.1.1.0._.bwg file</li>				
			</ul>
			
			
			<li>system config is used to indicate what kind of clustering scheme is applied on the aigned reads</li>
			
			<ul>
				<li>system config of 2 record only those aligned reads that are intersected with Ensembl biotype database in a biotype specific manner, e.g. BWG.2.1.0.0.miRNA.bwg and BWG.2.1.0.0.proteincoding.bwg respectively include aligned reads intersected with biotype of miRNA and protein_coding gene of the Ensembl biotype database</li>
				<li>system config of 0 record aligned reads that are intersected with either the Ensembl biotype database, e.g. BWG.0.1.0.0._.bwg, or the Ensembl pre-miRNA database, e.g. BWG.0.1.1.0._.bwg</li>				
			</ul>
			
			
			<li>filter config used to indicate whether certain reads filter scheme of unwanted annotated reads is going to apply or not</li>
			
			<ul>
				<li>Aligned reads that are annotated with any of the following biotypes: miscRNA, rRNA, snRNA will be marked as to be filtered beforehand, and only reads not with filter mark will be recorded in files with filter config of 1, e.g. BWG.0.1.0.0._.bwg</li>
				<li>All aligned reads, either marked as to be filtered or not, will all be recorded in files with filter config of -1, e.g. BWG.0.-1.0.0._.bwg</li>				
			</ul>
			
		</ul>	
	</p>
	
	<blockquote class="go">
		<p>
		Go And Run (<a href="mailto:obigbando@gmail.com">Contact Us</a>)
		</p>
	</blockquote>
	
	
	<a name="smallrnapipelineviewer"></a>
	<h2>sRNA Viewer (Small RNA Visualizer)</h2>
	<h3>General:</h3>
	<p>
		sRNA Viewer takes AppResults obtained from running sRNA Pipeline APP as input, and provides an interactive visualization interface to browse the processed result. sRNA Viewer includes three main elements: <a target="_blank" href="http://jbrowse.org/" title="Jbrowse">Jbrowse</a>, <a target="_blank" href="https://www.socialtext.net/open/socialcalc" title="socialcalc">Socialcalc</a>, and <a target="_blank" href="http://d3js.org/" title="D3 plotter">D3 plotter</a>, which are integrated into a single web browser interface. Jbrowse is used to show genomes, annotation databases, bam, and bigwig tracks established according to the AppResults. Socialcalc and D3 plotter are respectively used to load statistical analysis csv tables and used for data visualization.
	</p>
	
	<h3>Demo:</h3>
	<p>
		Here is a <a target="_blank" href="http://cloudxdna.com/visualizer/">Demo</a> of sRNA Pipeline and sRNA Viewer. 
Public dataset <a target="_blank" href="http://www.ncbi.nlm.nih.gov/sra/?term=SRR038863" title="NCBI SRA">SRR038863</a> is processed by sRNA Pipeline, and the corresponding processing result is rendered by sRNA Viewer.
	</p>
	<h3>Manual:</h3>
	<p>
		<a target="_blank" href="http://cloudxdna.com/Manual-windows.pdf">Download Manual</a>
	</p>
	
	<h3>Snapshots:</h3>
	<!-- <img src="images/smallRNAViewer1.jpg" width="100%"/> -->
	<a target="_blank" href="images/smallRNA_viwer.png"><img src="images/smallRNA_viwer.png" width="100%" style="border:none !important; "/></a>
	
	<blockquote class="go">
		<p>
		Go And Run (<a href="mailto:obigbando@gmail.com">Contact Us</a>)
		</p>
	</blockquote>
</div>



<div class="gradient">
	<h1>About Us</h1>
	
	<a name="contactus"></a>
	<h2>Contact Us</h2>
	<blockquote class="go">
		<p>
			<a href="mailto:obigbando@gmail.com">Contact Us</a>
		</p>
	</blockquote>
</div>

</div>