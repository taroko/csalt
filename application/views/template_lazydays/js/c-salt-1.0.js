var Projects = new Object();
var Samples = new Object();
var Files = new Object();

function Filesystem(items)
{
//private
	var parent = this;
	
//public
	this.paired_files = new Array();
	this.select_files_num = 0;
//private
	get_paired_files = function()
	{
		tmp_files = new Object();
		for (var key in items)
		{
			var filename = items[key]['Name'];
			if(filename.search('fastq') != -1 && ( filename.search('R1') != -1 || filename.search('R2') != -1 ))
				tmp_files[filename] = items[key];
		}
		for (var key in tmp_files)
		{
			var filename1 = tmp_files[key]['Name'];
			var filename2 = filename1.replace('R1','R2');
			if( filename1.search('R1') == -1 )
				continue;
			if( tmp_files[filename2] == undefined )
				continue;
			parent.paired_files.push({'R1':tmp_files[filename1],'R2':tmp_files[filename2], 'selected':1});
		}
	};
	get_paired_files_idx = function(filename)
	{
		var paired_files = parent.paired_files;
		filename = filename.replace('R2','R1');
		var idx = 0;
		for(var key in paired_files)
		{
			if(paired_files[key]['R1']['Name'] == filename)
				return idx;
			++idx;
		}
		return -1;
	}
//public
/*
	this.select_file_by_filename = function(filename, is_select)
	{
		var paired_files = parent.paired_files;
		var select_idx = get_paired_files_idx(filename);
		if(select_idx == -1)
		{
			console.log('Error! Filename not match');
			return 0;
		}
		if(is_select == undefined)
		{
			if (paired_files[select_idx]['selected'] == 0)
				is_select = 1;
			else
				is_select = 0;
		}
		paired_files[select_idx]['selected'] = is_select;
		return is_select;
	}
	*/
	this.select_file_by_idx = function(idx, is_select)
	{
		var paired_files = parent.paired_files;
		var select_idx = idx;
		
		if(is_select == undefined)
		{
			if (paired_files[select_idx]['selected'] == 0)
				is_select = 1;
			else
				is_select = 0;
		}
		if(is_select)
			is_select = 1;
		else
			is_select = 0;
			
		paired_files[select_idx]['selected'] = is_select;
		return is_select;
	}
	
	this.get_submit_files = function()
	{
		var rv = new Array();
		parent.select_files_num = 0;
		for( var key in parent.paired_files)
		{
			if(parent.paired_files[key].selected == 0)
				continue;
			rv.push(parent.paired_files[key].R1.Href);
			rv.push(parent.paired_files[key].R2.Href);
			parent.select_files_num ++;
		}
		return JSON.stringify(rv);
	}
	
	this.html_select_file = function(is_select)
	{
		var paired_files = parent.paired_files;
		if(is_select == undefined)
			is_select = 1;
		
		var html = "<table>";
		html += "<tr><td></td><td>Name</td><td>Size</td></tr>";
		for(var key in paired_files)
		{
			if(paired_files[key]['selected'] == 1)
				is_check = "checked='checked'";
			else
				is_check = "";

			html += "<tr>";
			html += "<td><input type='checkbox' class='select_file' id='file_"+key+"' "+is_check+" /></td>"
			html += "<td>" + paired_files[key]['R1']['Name'] + "</td>";
			html += "<td>" + parseInt(paired_files[key]['R1']['Size']/(1024*1024)) + "MB</td>";
			html += "</tr>";
			html += "<tr>";
			html += "<td></td>"
			html += "<td>" + paired_files[key]['R2']['Name'] + "</td>";
			html += "<td>" + parseInt(paired_files[key]['R2']['Size']/(1024*1024)) + "MB</td>";
			html += "</tr>";
		}
		html += "</table>";
		//parent.html_select_file_action()
		return html;
	}
	this.html_select_file_action = function()
	{
		$(".select_file").unbind('click').bind('click',function(){
			var idx = get_split(this.id);
			parent.select_file_by_idx(idx, $(this).is(":checked"));
			//console.log(parent.paired_files[idx]['selected']);
		});
	}
	
	get_paired_files();
}




function project_change()
{
	if($("#project_select").val() == "")
		return 0;
	
	var href = $("#project_select").val() +"/samples";
	console.log("jsquery/"+href);
	$("#sample_select").html("<option>---Loading...---</option>");
	$.getJSON("jsquery/"+href, function(data){
		console.log(data);
		Projects = data;
		
		var html = "<option>---select samples---</option>\n";
		for( var key in data['Response']['Items'])
		{
			html += "<option id='sample_"+data['Response']['Items'][key]['Id']+"' value='"+data['Response']['Items'][key]['Href']+"'>"+data['Response']['Items'][key]['Name']+"</option>\n";
			
		}
		$("#sample_select").html(html);
		
		if(data['Response']['Items'].length == 1)
		{
			$("#sample_select").val(data['Response']['Items'][0]['Href']);
			sample_change();
		}
	});
}
function sample_change()
{
	if($("#sample_select").val() == "")
		return 0;
	var href = $("#sample_select").val();
	$("#sample_read_num").val("Reading...");
	$.getJSON("jsquery/"+href, function(data){
		Samples = data;
		$("#sample_read_num").val(data['Response']['NumReadsRaw']);
		$("#file_list").html("Reading");
		$.getJSON( "jsquery/"+data['Response']['HrefFiles'], function(data2){
			console.log(data2);
			
			Files = new Filesystem(data2['Response']['Items']);
			
			var html = Files.html_select_file();
			$("#file_list").html(html);
			Files.html_select_file_action();
			/*
			var html = "";
			for(var key in data2['Response']['Items'])
			{
				html += "<div>"+data2['Response']['Items'][key]['Name']+"</div>";
			}
			*/
			
		});
				
	});
		
	
}

function init_set()
{
	$("#project_select").bind("change",function(){
		project_change();
	
	});
	$("#sample_select").bind("change", function(){
		sample_change();
	});
}

function test_appsession()
{
	$.getJSON("jsquery/"+appsession, function(data){
		if(data['Response']['References'][0] != undefined)
		{
			var project_href = data['Response']['References'][0]['Href'];
			$("#project_select").val(project_href);
			project_change();
		}
	});
}
function submit_set()
{
	$("form").submit(function(){
		
		var errors = 0;
		if($("#sample_select").val() == "---select samples---")
		{
			console.log("No select samples");
			errors++;
		}
		if(! (Files instanceof Filesystem) )
		{
			console.log("Filesystem Error!");
			errors++;
		}
		else
		{
			$("#files_select").val(Files.get_submit_files());
		}
		if(Files.select_files_num == 0)
		{
			console.log("No select files");
			errors++;
		}
		//console.log(Files.select_files_num);
		if(errors > 0)
			return false;
		return true;
	});
}


function get_split(str)
{
	return str.split("_")[1];
}