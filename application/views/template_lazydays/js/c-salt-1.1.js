var Projects = new Object();
var Samples = new Object();
var Files = new Object();

var default_sample_type = 1;
var sample_source = "samples";
//var sample_source = "appresults";

function Filesystem(items)
{
//private
	var parent = this;
	
//public
	this.single_files = new Array();
	this.paired_files = new Array();
	this.select_files_num = 0;

	get_single_files = function()
	{
		for (var key in items)
		{
			var filename = items[key]['Name'];
			if(filename.search('fastq') != -1)
			{
				parent.single_files.push({'R1':items[key], 'selected':1});
			}
		}
	};

	get_paired_files = function()
	{
		tmp_files = new Object();
		for (var key in items)
		{
			var filename = items[key]['Name'];
			if(filename.search('fastq') != -1 && ( filename.search('R1') != -1 || filename.search('R2') != -1 ))
				tmp_files[filename] = items[key];
		}
		for (var key in tmp_files)
		{
			var filename1 = tmp_files[key]['Name'];
			var filename2 = filename1.replace('R1','R2');
			if( filename1.search('R1') == -1 )
				continue;
			if( tmp_files[filename2] == undefined )
				continue;
			parent.paired_files.push({'R1':tmp_files[filename1],'R2':tmp_files[filename2], 'selected':1});
		}
	};
	get_paired_files_idx = function(filename)
	{
		var paired_files = parent.paired_files;
		filename = filename.replace('R2','R1');
		var idx = 0;
		for(var key in paired_files)
		{
			if(paired_files[key]['R1']['Name'] == filename)
				return idx;
			++idx;
		}
		return -1;
	};
//public
	this.select_single_file_by_idx = function(idx, is_select)
	{
		var single_files = parent.single_files;
		var select_idx = idx;
		
		if(is_select == undefined)
		{
			if (single_files[select_idx]['selected'] == 0)
				is_select = 1;
			else
				is_select = 0;
		}
		if(is_select)
			is_select = 1;
		else
			is_select = 0;
			
		single_files[select_idx]['selected'] = is_select;
		return is_select;
	}
	this.select_file_by_idx = function(idx, is_select)
	{
		var paired_files = parent.paired_files;
		var select_idx = idx;
		
		if(is_select == undefined)
		{
			if (paired_files[select_idx]['selected'] == 0)
				is_select = 1;
			else
				is_select = 0;
		}
		if(is_select)
			is_select = 1;
		else
			is_select = 0;
			
		paired_files[select_idx]['selected'] = is_select;
		return is_select;
	}
	
	this.get_submit_files = function()
	{
		var rv = new Array();
		parent.select_files_num = 0;
		for( var key in parent.paired_files)
		{
			if(parent.paired_files[key].selected == 0)
				continue;
			rv.push(parent.paired_files[key].R1.Href);
			rv.push(parent.paired_files[key].R2.Href);
			parent.select_files_num ++;
		}
		return JSON.stringify(rv);
	}
	this.get_single_submit_files = function()
	{
		var rv = new Array();
		parent.select_files_num = 0;
		for( var key in parent.single_files)
		{
			if(parent.single_files[key].selected == 0)
				continue;
			rv.push(parent.single_files[key].R1.Href);
			parent.select_files_num ++;
		}
		return JSON.stringify(rv);
	}
	
	this.html_select_file = function(is_select)
	{
		var paired_files = parent.paired_files;
		if(is_select == undefined)
			is_select = 1;
		
		var html = "<table>";
		html += "<tr><td></td><td>Name</td><td>Size</td></tr>";
		for(var key in paired_files)
		{
			if(paired_files[key]['selected'] == 1)
				is_check = "checked='checked'";
			else
				is_check = "";

			html += "<tr>";
			html += "<td><input type='checkbox' class='select_file' id='file_"+key+"' "+is_check+" /></td>"
			html += "<td>" + paired_files[key]['R1']['Name'] + "</td>";
			html += "<td>" + parseInt(paired_files[key]['R1']['Size']/(1024*1024)) + "MB</td>";
			html += "</tr>";
			html += "<tr>";
			html += "<td></td>"
			html += "<td>" + paired_files[key]['R2']['Name'] + "</td>";
			html += "<td>" + parseInt(paired_files[key]['R2']['Size']/(1024*1024)) + "MB</td>";
			html += "</tr>";
		}
		html += "</table>";
		//parent.html_select_file_action()
		return html;
	}
	this.html_single_select_file = function(is_select)
	{
		var single_files = parent.single_files;
		if(is_select == undefined)
			is_select = 1;
		
		var html = "<table>";
		html += "<tr><td></td><td>Name</td><td>Size</td></tr>";
		for(var key in single_files)
		{
			if(single_files[key]['selected'] == 1)
				is_check = "checked='checked'";
			else
				is_check = "";

			html += "<tr>";
			html += "<td><input type='checkbox' class='select_file' id='file_"+key+"' "+is_check+" /></td>"
			html += "<td>" + single_files[key]['R1']['Name'] + "</td>";
			html += "<td>" + parseInt(single_files[key]['R1']['Size']/(1024*1024)) + "MB</td>";
			html += "</tr>";
		}
		html += "</table>";
		//parent.html_select_file_action()
		return html;
	}
	this.html_select_file_action = function()
	{
		$(".select_file").unbind('click').bind('click',function(){
			var idx = get_split(this.id);
			parent.select_file_by_idx(idx, $(this).is(":checked"));
			//console.log(parent.paired_files[idx]['selected']);
		});
	}
	this.html_single_select_file_action = function()
	{
		$(".select_file").unbind('click').bind('click',function(){
			var idx = get_split(this.id);
			parent.select_single_file_by_idx(idx, $(this).is(":checked"));
			//console.log(parent.paired_files[idx]['selected']);
		});
	}
	get_single_files();
	get_paired_files();
}


function project_change()
{
	if($("#project_select").val() == "")
		return 0;
	
	var href = $("#project_select").val() + "/" + sample_source;
	console.log("jsquery/"+href);
	$("#sample_select").html("<option>---Loading...---</option>");
	$.getJSON("jsquery/"+href, function(data){
		console.log(data);
		Projects = data;
		
		var html = "<option>---select "+sample_source+"---</option>\n";
		for( var key in data['Response']['Items'])
		{
			html += "<option id='sample_"+data['Response']['Items'][key]['Id']+"' value='"+data['Response']['Items'][key]['Href']+"'>"+data['Response']['Items'][key]['Name']+"</option>\n";
			
		}
		$("#sample_select").html(html);
		
		if(data['Response']['Items'].length == 1)
		{
			$("#sample_select").val(data['Response']['Items'][0]['Href']);
			sample_change();
		}
	});
}
function sample_change()
{
	if($("#sample_select").val() == "")
		return 0;
	var href = $("#sample_select").val();
	$("#sample_read_num").val("Reading...");
	$.getJSON("jsquery/"+href, function(data){
		Samples = data;
		if(data['Response']['NumReadsRaw'])
			$("#sample_read_num").val("done");
		else
			$("#sample_read_num").val("Unknown (only App result)");
		$("#file_list").html("Reading");
		$.getJSON( "jsquery/"+data['Response']['HrefFiles'], function(data2){
			//console.log(data2);
			Files = new Filesystem(data2['Response']['Items']);
			if(default_sample_type == 1)
			{
				var html = Files.html_single_select_file();
				$("#file_list").html(html);
				Files.html_single_select_file_action();
			}
			else
			{
				var html = Files.html_select_file();
				$("#file_list").html(html);
				Files.html_select_file_action();
			}
		});
				
	});
}

function init_set()
{
	$("#project_select").bind("change",function(){
		project_change();
	});
	$("#sample_select").bind("change", function(){
		sample_change();
	});
	$(".sample_type").bind("change", function(){
		$("#file_list").html("");
		if($("#sample_type_single_end").is(":checked"))
			default_sample_type=1;
		else
			default_sample_type=2;
		sample_change();
		console.log("default_sample_type", default_sample_type);
	});
	$(".sa_select").bind("change", function(){
		if($("#sa_select_s").is(":checked"))
			sample_source = "samples";
		else
			sample_source = "appresults";
		project_change();
		console.log("sample_source", sample_source);
	});
	//#adapter-3p
	$("#barcode-5p, #barcode-3p").bind("click", function(){
		$("#barcode_seq").attr("readonly",false);
	});
	$("#barcode-no").bind("click", function(){
		$("#barcode_seq").attr("readonly",true).val("");
	});
	$("#adapter-3p").bind("click", function(){
		$("#adapter_seq").attr("readonly",false);
	});
	$("#adapter-no").bind("click", function(){
		$("#adapter_seq").attr("readonly",true).val("");
	});
}

function test_appsession()
{
	$.getJSON("jsquery/"+appsession, function(data){
		if(data['Response']['References'][0] != undefined)
		{
			var project_href = data['Response']['References'][0]['Href'];
			$("#project_select").val(project_href);
			project_change();
		}
	});
}
function submit_set()
{
	$("form").submit(function(){
		
		var errors = 0;
		if($("#sample_select").val() == "---select samples---")
		{
			console.log("No select samples");
			errors++;
		}
		if(! (Files instanceof Filesystem) )
		{
			console.log("Filesystem Error!");
			errors++;
		}
		else
		{
			if(default_sample_type == 1)
				$("#files_select").val(Files.get_single_submit_files());
			else
				$("#files_select").val(Files.get_submit_files());
		}
		if(Files.select_files_num == 0)
		{
			console.log("No select files");
			errors++;
		}
		if(errors > 0)
			return false;
		return true;
	});
}


function get_split(str)
{
	return str.split("_")[1];
}