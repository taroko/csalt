	<script src="js/jquery/2.0"></script>
	<script src="js/c-salt/1.0"></script>
		<div class="width75 floatRight">


      <!-- Gives the gradient block -->
      <div class="gradient">
      
      	<h1>About PEAT (Paired-end Adapter Trimmer) </h1>

      	<h2>Description</h2>
      	<p> PEAT is a dedicated sequencing adapter removing tool for paired-end sequencing reads. PEAT takes two gz compressed FastQ files, containing paired-end sequenced reads from your BaseSpace project/run, as input, and returns two gz compressed FastQ files, containing adapter contamination free paired-end reads, to your APP session destination.</p>

      	<h2>Algorithm</h2>
	<p> The algorithm behind PEAT makes use of the mutually reverse-complement trait of the intrinsic DNA/cDNA portions of the adapter contaminated paired-end sequenced reads, to identify the adapter-contaminated reads and its intrinsic DNA/cDNA and adapter contaminated portions. The implementation detail for PEAT algorithm can be found in  paper titled "A fast and accurate paired-end adapter-trimming algorithm and its implementation in the cloud". </p>
	<p>
	<a href="http://140.113.15.108/PEAT/wp-content/uploads/2013/05/method1.png">
	<img class="aligncenter size-full wp-image-227" title="GG" src="http://www.jhhlab.tw/PEAT/wp-content/uploads/2013/05/GG.jpg" alt="" width="700" height="625">
	</a>
	</p>
	<p>
	<a href="http://140.113.15.108/PEAT/wp-content/uploads/2013/05/method1.png">
	<img class="aligncenter size-full wp-image-230" title="figure1" src="http://www.jhhlab.tw/PEAT/wp-content/uploads/2013/05/figure11.jpg" alt="" width="700" height="653">
	</a>
	</p>

	<h2>Using note</h2>
	<p> In general, we suggest that PEAT can make significant impact in NGS related analyses dealing with short DNA/cDNA fragments, such as shotgun sequencing, microRNA sequencing, bisulfite sequencing, and so on, where reads are sequenced from relatively short DNA fragments and are more likely to suffer from adapter contamination and incapability of genome mapping issues. </p>

      	<h2>Performance</h2>
	<p> In our real test, PEAT free version can handle adapter trimming task in a rate about 10.5 Gb/Hr. In other words, it takes about 30 minutes for a free run of PEAT handling adapter trimming operation for two gz compressed FastQ files having a total maximum file size of 5Gb. </p>
	<blockquote class="go">
          <p>
		A paid version of PEAT that can handle adapter trimming task for bigger files is coming soon. </p>
          </p>
        </blockquote>
      </div>
    </div>
