<div>
	Purchase Information:
	<p>
		Price: <?php echo $run['AmountTotal'];?> <br />
		RefundStatus: <?php echo $run['RefundStatus'];?> <br />
		Refund: <?php if($is_refund) echo "<a href='refund?purchase_id={$run['purchase_id']}'>Refund</a>"; else echo "Not Allowed."; ?>
	</p>
</div>

<div>
	Small RNA Pipeline Viewer Information:
	<?php
		if($run['Name'] == "weekly-pass")
			$t = 7*24*60*60 + 1*60*60;
		else if($run['Name'] == "monthly-pass")
			$t = 30*24*60*60 + 1*60*60;
		else if($run['Name'] == "season-pass")
			$t = 90*24*60*60 + 1*60*60;
		else if($run['Name'] == "FREE TEST")
			$t = 1*24*60*60 + 1*60*60;
		$remaining = $run['DateCreate'] + $t - time();
		$is_closed = true;
		$is_start = false;
		
		if($remaining > 0 && $run['status']==1 && $instance && $instance['State'] == "running")
			$is_closed = false;
		if($remaining > 0 && $run['status']!=0 && $instance && $instance['State'] == "running")
			$is_start = true;
	?>
	<?php if($is_closed) { ?>
	<p>
		<?php if($is_start) { ?>
		Service Running Time: <?php  echo date("z \D H \H i \M", time() - $run['DateCreate']); ?>
		<br />
		Service Remaining Time: <?php echo date("z \D H \H i \M", $remaining); ?>  
		<br />
		<?php } ?>
		Service Not Allowed
	</p>
	<?php }else{ ?>
	<p>
		Service Running Time: <?php  echo date("z \D H \H i \M", time() - $run['DateCreate']); ?>
		<br />
		Service Remaining Time: <?php echo date("z \D H \H i \M", $remaining); ?>  
		<br />
		Viewer URL: <a href='<?php echo "http://{$instance['PublicIpAddress']}/jb/visualizer/?&user=$user"; ?>'>Run SPViewer!</a>
	</p>
	<?php } ?>
	
</div>