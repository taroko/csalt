		<div class="width75 floatRight">


      <!-- Gives the gradient block -->
      <div class="gradient">

        <a name="BaseSpace"></a>

        <h1>BaseSpace</h1>
	<h2>Introduction</h2>
        <p>
	<a href="https://basespace.illumina.com/home/sequence">BaseSpace</a>
	 is a genomics cloud computing platform well acknowledged as a data manage center for cloud-based NGS data uploading, archiving, and sharing. The platform provides application programming interfaces for developers to build applications that can be launched by end-users to manipulate the already uploaded NGS data, achieve certain data analysis purpose, and obtain the data analysis result accordingly. 
        </p>

	<h2>Operation scheme</h2>
        <p>
        <a href="http://140.113.15.108/PEAT/wp-content/uploads/2013/05/method1.png">
        <img class="aligncenter size-full wp-image-227" title="GG" src=
"http://www.jhhlab.tw/PEAT/wp-content/uploads/2013/08/f6.jpg" alt="" width="568" height="391">
        </a>
        </p>
	<p>
	 In detail, a BaseSpace server, provided by the Illumina BaseSpace platform, serves as a web portal, so that users can grant access authorization to their BaseSpace archived data and launch PEAT cloud application. Users will be redirected to a PEAT cloud server after the authorization and launch operations. The PEAT cloud server then interacts with users to select the files that are designated by the user. The PEAT cloud server next launches a PEAT instance, i.e., an amazon EC2 instance, and has it configured to download the user-designated files from the BaseSpace data archive, i.e. Amazon S3, run PEAT application on the downloaded data, and upload the PEAT processed file. The PEAT cloud server further monitors the running status of the PEAT instance, terminates the instance and notifies the BaseSpace data archive system upon the completion of the trimming and uploading. As such, with the provided PEAT cloud server together with the PEAT instance launched in an on-demand manner, the PEAT cloud application on Illumina BaseSpace is freely available to all users for small scale use. 
	</p>
      </div>

    </div>
