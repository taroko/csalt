<?php
function query_curl($opts)
{
	$ch = curl_init();
	curl_setopt_array($ch, $opts);
	$response = curl_exec($ch); 
	curl_close($ch);
	return $response;
}

function step1()
{
	$url = "https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=t1.gzip&multipart=true";
	
	$options = Array(
		CURLOPT_URL => $url,
		CURLOPT_HTTPHEADER => Array("x-access-token: 4b2bf2c94ea34721ac01fc177111707f", "Content-Type: application/gzip"),
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_VERBOSE => true,
		CURLOPT_POST => true,
		CURLOPT_POSTFIELDS => ""
	);
	$response = query_curl($options);
	echo $response;
}
function makefile()
{
	$text = "";
	for($i=0; $i<100000000;$i++)
	{
		$text .= chr($i);
	}
	file_put_contents("test.txt", $text);
}
function step2()
{
	
	//file_put_contents("ttttt",$text);
	//$md5 = md5($text);
	$text = file_get_contents("aa.tar.gz");
	
	$url = "https://api.basespace.illumina.com/v1pre3/files/117418992/parts/3";
	$options = Array(
		CURLOPT_URL => $url,
		CURLOPT_HTTPHEADER => Array("x-access-token: 4b2bf2c94ea34721ac01fc177111707f", "Content-Type: multipart/form-data"),
		CURLOPT_CUSTOMREQUEST => 'PUT',
		CURLOPT_BINARYTRANSFER => true,
		CURLOPT_VERBOSE => true,
		CURLOPT_POSTFIELDS => $text
		//CURLOPT_POSTFIELDS => array('file' => '@'. '../wordpress/wp-includes/js/tinymce/wp-tinymce.js.gz')// OK!
	);
	$response = query_curl($options);
	echo $response;
}
function step3()
{
	$url = "https://api.basespace.illumina.com/v1pre3/files/117418992?uploadstatus=complete";
	
	$options = Array(
		CURLOPT_URL => $url,
		CURLOPT_HTTPHEADER => Array("x-access-token: 4b2bf2c94ea34721ac01fc177111707f"),
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_VERBOSE => true,
		CURLOPT_POST => true,
		CURLOPT_POSTFIELDS => ""
	);
	$response = query_curl($options);
	echo $response;
}

//makefile();
//step1();
step2();
//step3();



/*
{"Response":{"UploadStatus":"pending","HrefContent":"v1pre3/files/112891203/content","HrefParts":"v1pre3/files/112891203/parts","Id":"112891203","Href":"v1pre3/files/112891203","Name":"text.txt","ContentType":"application/text","Size":0,"Path":"text.txt","DateCreated":"2013-06-04T12:17:32.5876725Z"},"ResponseStatus":{},"Notifications":[]}



curl -v -H "x-access-token: 4b2bf2c94ea34721ac01fc177111707f" \
-H "Content-Type: application/zip" \
-X POST https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=reportarchive.zip\&multipart=true

{"Response":{"UploadStatus":"pending","HrefContent":"v1pre3/files/112886355/content","HrefParts":"v1pre3/files/112886355/parts","Id":"112886355","Href":"v1pre3/files/112886355","Name":"reportarchive.zip","ContentType":"application/zip","Size":0,"Path":"reportarchive.zip","DateCreated":"2013-06-04T12:12:59.6614020Z"},"ResponseStatus":{},"Notifications":[]}



{"Response":{"UploadStatus":"pending","HrefContent":"v1pre3/files/112893857/content","HrefParts":"v1pre3/files/112893857/parts","Id":"112893857","Href":"v1pre3/files/112893857","Name":"text.zip","ContentType":"application/zip","Size":0,"Path":"text.zip","DateCreated":"2013-06-04T12:16:17.2869081Z"},"ResponseStatus":{},"Notifications":[]}


curl -v -H "x-access-token: 4b2bf2c94ea34721ac01fc177111707f" \
-H "Content-MD5: 9mvo6qaA+FL1sbsIn1tnTg==" \
-T text.txt \
-X PUT https://api.basespace.illumina.com/v1pre3/files/112891203/parts/1
*/
?>

